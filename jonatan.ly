muziko = {
  \time 4/4
  <g\4>4 <e\2>4 <e\2>2
  <f\2>4 <d\3>4 <d\3>2
  <c\3>4 <d\3>4 <e\2>4 <f\2>4
  <g\4>4 <g\4>4 <g\4>2

}

<<
  \new Staff \with {\omit StringNumber}
  \transpose c c' {\muziko}
  \new TabStaff \with {stringTunings = \stringTuning <g' c' e' a'>}
  \transpose c c' {\muziko}
>>
