all:
	#$(MAKE) -C muzikaĵoj
	./enhavo.py > enhavo.tex
	./muzikaĵoj.py > muzikaĵoj.tex
	latexmk -xelatex -file-line-error ukulele

pvc:
	latexmk -silent -pvc -file-line-error ukulele

clean:
	-rm *.aux *.bbl *.blg *.log *.toc *.url *.cut *.bib *.run.xml *.bst *.bcf *.fls *.fdb_latexmk *.out *.dvi *.idx *.ilg *.ind

distclean: clean
	-rm ukulele.pdf
