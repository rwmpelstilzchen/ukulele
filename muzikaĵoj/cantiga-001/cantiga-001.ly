\include "../ĉiea.ly"

\header {
  titolo-xx     = "Des oge mais quér’ éu trobar"
  titolo-he     = "קנטיגה 1"
  titolo-eo     = "Kantigo 1"
  komponisto-xx = ""
  komponisto-he = "עממי / אלפונזו העשירי"
  komponisto-eo = "popola / Alfonso la 10-a"
  ikono         = "♍"
}

\include "../titolo.ly"

% http://www.gaita.co.uk/cantiga_vol1_sample.pdf
melodio = {
  \key d \minor
  \time 4/4
  a8 a4 a8 a4. g8\4 |
  a4. bes8 a4. g8\4 |
  f4 f8 g8\4 a8 bes8 c'8 bes8 |
  a8 bes8 a8 g8\4 a4. a8 |
  \break
  c'4 d'4 d'8 c'4 a8 |
  a4 bes16 a16 g8\4 a4. g8\4 |
  f4 f4 g8\4 a8 f8 e8 |
  d4. c8 d4. d8 |
  \break
  f8 e8 d8 c8 f4 f8 g8\4 |
  a8 bes8 a8 g8\4 a4. a8 |
  c'4 d'4 d'8 c'4 a8 |
  a4 bes16 a16 g8\4 a4. a16 g16\4 |
  \break
  f4 f4 g8\4 a8 f8 e8 |
  d4. c8 d4. g8\4 |
  g4.\4 e8 f16 e16 f8 g8\4 e8|
  e4. a8
  \break
  a4 a8 g8\4 f4 g8\4 a8
  f8 e8 d8 e8 c4. e8
  g4.\4 f16 e16 d8 e8 f8 e8
  e8 d8 d8 c8 d2
  \bar "|."
}

\include "../muziko.ly"
