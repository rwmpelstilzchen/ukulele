\include "../ĉiea.ly"

\header {
  titolo-xx     = ""
  titolo-he     = "השיר של טוטקה"
  titolo-eo     = "La kanto de Totaka"
  komponisto-xx = "戸高一生"
  komponisto-he = "קזומי טוטקה"
  komponisto-eo = "Kazumi Totaka"
  ikono         = "🐣"
}

\include "../titolo.ly"

melodio = {
  \key c \major
  \time 6/8
  c c16 d e4 d8 |
  c4. g |
  e c' |
  g r |
  \break
  g4 g16 aes g4 fis8 |
  ees4. r |
  d4. g |
  c4. r |
}

\include "../muziko.ly"
