\include "../ĉiea.ly"

\header {
  titolo-xx     = ""
  titolo-he     = "פלא: איך אוכל להמשיך לישון עכשיו"
  titolo-eo     = "Mirindaĵo: Kiel mi povus dormi nun"
  komponisto-xx = ""
  komponisto-he = "שי בן־צור"
  komponisto-eo = "Ŝaj Ben-Cur"
  ikono         = "💤"
}

\include "../titolo.ly"

melodio = {
  \key f \major
  \time 6/8
  %r8. r32 a4 f8 g8\4 ~ g32\4 |
  %a4 f8 g4.\4 | %←
  r8 a4 f8 g4\4 |
  %r8. a4 f8 g8.\4 |
  c8 d f g\4 a4 |
  f8 f8 ~ f2 |
  a4 f8 g4\4 c8 |
  d f c2 |
  \break
  %a4 f8 g4.\4 | %←
  r8 a4 f8 g4\4 |
  %r8. a4 f8 g8.\4 |
  c8 d f g\4 a4 |
  f8 f8 ~ f2 |
  a4 f8 g4\4 g8\4 |
  g\4 g\4 e8. d16 c4 ~ |
  \interim
  c8 a8 a a a8. g16\4 |
  f2. |
  \deinterim
  \bar "||"
  \break

  c'8 c' a c' ~ c'4 |
  c'8 d'4 c' a8 |
  c'4 g2\4 ~ |
  g2.\4 |
  \break
  c'4 a8 c'4. ~ |
  c'2 ~ c'8 c'8 |
  a8 c' d'4 c' |
  a8 c'4 g4.\4 |
  g8\4 a g\4 a g\4 a g\4 a g\4 f4. ~|
  f2.
  \break
  g8\4 g\4 e8. d16 c4 ~ |
  c2. |
  a8 a g8\4 a8. g16\4 f8 ~ |
  f2.
  \bar "|."
}

tmpmelodio = {
  \time 4/4
  \tempo 4 = 80
  a8. f8 g8. c8 d f g |
  a8. f8 f2 r4 |

  a8. f8 g8. c8 d f c4. |

  a8. f8 g8. c8 d f g |
	a4 f8 f4.
	a8. f8 g8. |
	g8 g g e8. |
	d16 c2
	a8 a a a8. g16 f4
}

\include "../muziko.ly"
