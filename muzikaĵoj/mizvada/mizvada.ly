\include "../ĉiea.ly"

\header {
  titolo-xx     = ""
  titolo-he     = "ענן על מקל: קיפודה עם מזוודה"
  titolo-eo     = "Nubo sur paliso: Histriko kun teko"
  komponisto-xx = ""
  komponisto-he = "דידי שחר"
  komponisto-eo = "Didi Ŝaĥar"
  ikono         = "💼"
}

\include "../titolo.ly"

melodio = {
  \tempo 4 = 180
  \key f \major

  %\time 3/4
  %\repeat volta 2 {
  %  f2 c8 f |
  %  g2. |
  %  f8 g a c' ~ c'4 |
  %  c'8 c' c' d ~ d4 |
  %  \break
  %  c'4 c'8 c' bes4 |
  %  a2.  |
  %  a4 bes8 a8 g4 |
  %  f2. |
  %}

  \time 4/4
  %\partial 4 f4 ~
  \interim
  c4 e g a |
  c'1 |
  \deinterim
  \repeat volta 2 {
    f1 |
	c8 f g2. |
    f8 g a c' ~ c'2 |
    c'8 c' c' d ~ d2 |
    \break
    c'4 c'8 c' bes4 a4 ~ |
    a4 a4 bes8 a8 g4 |
    f1 |
  }
  \break
  f4 f4 d2 |
  f8 f8 f4 d2 |
  f4 g f a |
  g f d2 |
  \break
  a8 bes4. a8 bes4. ~ |
  bes1 |
  c'4 c'4 bes4 a |
  c'1 |
  \break
  f4 f4 d2 |
  f8 f8 f4 d2 |
  f4 g f2 |
  a4 g f2  |
  \break
  a8 bes4. a8 bes4. ~ |
  bes1 |
  c'4 c'4 bes4 a |
  f1 |
  \bar "|."
}

\include "../muziko.ly"
