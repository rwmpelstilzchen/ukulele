\include "../ĉiea.ly"

\header {
  titolo-xx     = ""
  titolo-he     = "הפרפר והפרח"
  titolo-eo     = "La floro kaj la papilio"
  komponisto-xx = ""
  komponisto-he = "נחום נרדי"
  komponisto-eo = "Naĥum Nardi"
  ikono         = "💬"
}

\include "../titolo.ly"

melodio = {
  \time 3/4
  \tempo "Amabile" 4 = 80
  \mark "🌼→🦋"
  \key f \major
  \partial 8 c8 |
  \repeat volta 2 {
    f4 g8\4 a4. |
    c'4 d'8 a4 d'8 |
    c'4 a8 f4. |
  }
  \alternative {
	{ f8 e f g4\4 c8 | }
	{ g\4 a f c'4\fermata c8 | }
  }
  \break
  \repeat volta 2 {
    d4 e8 f4 g8\4 |
    a4. ~ a8 r c' |
    a4 f8 g4\4 a8 |
  }
  \alternative {
	{ f e f d r c | }
	{ f4. ~ f8 r4 | }
  }
  \bar "||"
  \pageBreak

  \mark "🦋→🌼"
  \key a \minor
  \partial 4 e |
  c' b c' |
  b a e |
  a8 g\4 a4 g8\4 f |
  e4 r c |
  \break
  c d e |
  f8 e d4 a |
  e8 d c4 d |
  e r e |
  \break
  b b d' |
  c'8 b a4 c' |
  b8 a g4\4 a |
  b e e |
  \break
  f g\4 a |
  d' b b |
  c' a b |
  e2 ~ e8 r |
  \break
  c'4 b c' |
  d'8 e' c'2 |
  b8 c' d' d' c' b |
  c'2 ~ c'8 r |
  \break
  a b c'4 b8 c' |
  a4 e'2 ~ |
  e4 r d'8\mp e' |
  d' c' b4 a |
  e r f8 b |
  e4 r r |
  \bar "|."
}

\include "../muziko.ly"
