\include "../ĉiea.ly"

\header {
  titolo-xx     = ""
  titolo-he     = "עוגה עוגה"
  titolo-eo     = "Ronde, ronde"
  komponisto-xx = ""
  komponisto-he = ""
  komponisto-eo = ""
  ikono         = "🎂"
}

\include "../titolo.ly"

melodio = {
  \key f \major
  \time 4/4
  a4 a g\4 g\4 |
  f2 c |
  a4 a g\4 g\4 |
  f2 c |
  \break
  %d4 d e c |
  %f4 a g2\4 |
  %g4\4 g\4 a g\4 |
  %e4 g\4 c2 |
  f4 f g\4 g\4 |
  a c' g2\4
  f4 f g\4 g\4 |
  a c' g2\4
  \break
  a4 f2 c4 |
  a f2. |
  a4 f2 c4 |
  a f2. |
  \break
  a4 f2 c4 |
  a f2. |
  c'4 c' bes a |
  g\4 f2. |
  \bar "|."
}

\include "../muziko.ly"
