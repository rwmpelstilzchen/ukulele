\include "../ĉiea.ly"

\header {
  titolo-xx     = ""
  titolo-he     = "לגדול איתך ביחד: ליל מנוחה"
  titolo-eo     = "Grandiĝi kun vi: Trankvilan nokton"
  komponisto-xx = ""
  komponisto-he = "יעל תלם"
  komponisto-eo = "Jael Telem"
  ikono         = "👀"
}

\include "../titolo.ly"

melodio = {
  \time 3/4
  \key c \major
  \partial 4 c8 d |
  e4. g8\4 g\4 e |
  a4 g\4 a8 g\4 |
  e4 c c8 e |
  e4 d2 |
  \break
  \time 4/4
  g16\2 g\4 g\2 g\4 e8 g\4 e2 |
  g8\4 e r4 c8 c c c |
  d2. c8 d |
  \break
  \time 3/4
  e4. g8\4 g\4 e |
  a4 g\4 a8 g\4 |
  e4. c8 c e |
  e4 d e8 d |
  \break
  c4. c8 c d |
  e4. a8 g\4 a |
  e4 c c8 d8 |
  c2. |
  \bar "|."
}

\include "../muziko.ly"
