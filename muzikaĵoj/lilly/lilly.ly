\include "../ĉiea.ly"

\header {
  titolo-xx     = ""
  titolo-he     = "לילי"
  titolo-eo     = "Lilly"
  komponisto-xx = ""
  komponisto-he = "צ׳יינה פֿורבס ותומס לונדרדייל"
  komponisto-eo = "China Forbes kaj Thomas Lauderdale"
  ikono         = "🐶"
}

\include "../titolo.ly"



A = {
  \mark "A"
  c''4\2 c''8\2 c''\2 des''4\2 c''8\2 bes'\2 ~ |
  bes'\2 g'4.\3 r4\3 g'8\3 g'\3 |
  bes'4\2 bes'8\2 bes'\2 bes'\2 des''4\2 c''8\2 ~ |
  c''2\2 r4 f''8 f''|
  \break
  c''4\2 c''8\2 c''\2 des''4\2 c''8\2 e'' ~ |
  e'' e''4. r4 e''8 e''|
  e'' e'' e'' e'' e'' e''4 f''8~ |
  %f''2 r2|
}

Aa = {
  \mark "A¹"
  f'8\2 c'\3 f'\2 aes' c''4 r |
  \repeat unfold 2 {r8 c'\3 e'\2 g' bes'4 r |}
  \repeat unfold 2 {r8 c'\3 d'\2 f'\2 aes'4 r |}
  \repeat unfold 2 {r8 c'\3 e'\2 g' bes'4 r |}
  r8 c'\3 f'\2 aes' c''4 r |
}

Ab = {
  \mark "A²"
  r4 aes'8 f'\2 des'4\3 f'8\2 c'\3 ~ |
  c'8\3 c'4\3 c'8\3 des'\3 c'\3 des'\3 c'\3 |
  c'4\3 aes'8 f'\2 des'4.\3 c'8\3 ~ |
  c'8\3 aes'4 f'8\2 c'\3 aes' c'' aes' |
  \break
  c''4 aes'8 f'\2 des'\3 aes' f'\2 c'' ~ |
  c''8 g'4\2 e'8\2 des'\3 c'\3 des'\3 c'\3 |
  %c'4 g'8 e' e'' bes' r4 |
  c'4\3 g'8\2 e'\2 e'\2 bes\3 r4 |
  r8 f\3 aes\3 c'\2 f'4\1 r |
}

B = {
  \mark "B"
  bes'8 bes'8 bes'8 bes'8 bes'2 |
  aes'8 aes' aes' aes' aes'2 |
  bes'8 bes' bes' bes' bes'4 des''8 des''8~ |
  des''8 c''2. r8 |
  bes'8 bes' bes' bes' bes'2 |
  aes'8 aes' aes' aes' aes'2 |
  aes'8 aes' aes' aes' aes'4 aes'8 aes'8~ |
  aes'8 g'2. r8 |
}

Ba = {
  \mark "B¹"
  r8 c'\3 g'\4 c'\3 c''2 |
  r8 c'\3 f'\4 c'\3 c''4 aes' |
  r4. c'8\3 bes'2 |
  r8 c'\3 f'\4 aes' c''4 aes'8 f'\4 |
  des'\3 bes\3 des'\3 bes' des''4 bes' |
  r8 c'\3 f'\4 aes' c''2 |
  r8 b' g' d'\2 b\3 g\3 f4\3 |
  r8 c4 d e g8 |
}

brass = {
  \mark "Inter¹"
  \repeat unfold 2 {
	%f''8\2 aes'' f''\2 ees''\3 ~ ees''\3 g''\2 ees''\3 des''\3 ~ |
	f''8 aes'' f'' ees''\2 ~ ees''\2 g'' ees''\2 des''\2 ~ |
	des''8\2 f'' des''\2 c''\2 ~ c''4\2 r4 |}
}

trumpetsolo = {
  \mark "Inter²"
  r c'8\2 c'\2 f'8. c'16\2 ~ c'\2 g'8 g'16 ~ |
  g'4 r r2 |
  r8 aes' g' f' e'\2 c'\2 bes\3 aes\3 |
  c'4\2 bes8\3 aes\3 ~ aes4\3 r |
  \tuplet 3/2 {r8. f g8\4 aes bes} \tuplet 3/2 { c'8. bes aes8 g\4 f } |
  \tuplet 3/2 {e4 f g8\4 aes} \tuplet 3/2 {bes2 aes8 f} |
  e8 c des c e4 g8\4 f ~ |
  f2 r4. c'8\2 ~ |
  c'8\2 f' aes\3 c'\2 f' f' c'4\2 |
  e'4\4 f'8 g' ~ g'4 r8 c'\2 ~ |
  c'8\2 g' g\3 c'\2 g' g' c'4\2 |
  f'4 g'8 aes' ~ aes'2 |
  r8 g'4 f'8 aes'16 g' f'8 c'8\2 des'\2 |
  r8 c' bes aes g f c des ~ |
  des8 e f g bes4. aes16 g |
  f2. r4 |
}

codaa = {
  \mark "Coda¹"
  g'8\4 aes'\4 e'\2 g'\2 c'' c'\3 g'4\2 |
  c'8\3 f'\2 aes'\4 f'\2 c'' f'\2 aes'4\2 |
  c'8\3 e'\2 g'\2 c'' bes' g'4.\2 |
  r4. f'8 e' ees' d' des' |
  %ges, bes, des ges bes des' ges' des'' |
  ges bes des ges bes des' ges' des'' |
  c''8 c des f aes c' aes f |
  %g4 b,8 d f g g' d'' |
  g4 b8 d f g g' d'' |
  g'2 r |
  %e''8 c'' bes' g' e' aes'4. |
  e'8\2 c'\2 bes\2 g\4 e\3 aes4.\2 |
}

codab = {
  \mark "Coda²"
  \repeat volta 2 {
	r1 |
	r8 c'4 c'8 des' c' des' c' |
	bes1 |
	r8 c'4\2 c'8\2 f' f' c'4\2 ~ |
	c'2\2 r |
	r8 e g bes des' c' des' c' |
	bes1 |
  }
  \alternative {
	{
	  r8 f\3 aes\3 c'\2 f'2 |
	}
	{
	  f'8 aes' f' ees'\4 ~ ees' g' ees'\4 des'\2 ~ |
	  des'8\2 f' des'\2 c'\2 ~ c'4\2 r |
	}
  }
  c'8\2 c'4.\2 r8 c'8\2 c'\2 f' ~ |
  f'4 r r2 |
}

melodio = {
  \tempo 4=145
  \key f \minor
  \time 4/4
  \partial 8*2 f'8 f' |
  \transpose c' c {\A}
  f'4. \small f'8\3 e'\3 ees'\3 d'\3 des'\3 | \normalsize
  \bar "||" \break
  \small
  \Aa
  \bar "||" \break
  \Ab
  \bar "||" \break
  \normalsize
  \transpose c' c {\B}
  \bar "||" \break
  \small
  \Ba
  \bar "||" \break
  \normalsize
  \transpose c' c {\A}
  f'4. \small f'8\1 e'\1 ees'\2 d'\2 des'\2 | \normalsize
  \bar "||" \break
  \small
  \transpose c' c {\brass}
  \bar "||" \break
  \trumpetsolo
  \bar "||" \break
  \normalsize
  \transpose c' c {\B}
  \bar "||" \break
  \small
  \Ba
  \bar "||" \break
  \codaa
  \break
  \codab
  \bar "|."
}

\include "../muziko.ly"
