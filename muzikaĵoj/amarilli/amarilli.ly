\include "../ĉiea.ly"

#(set-global-staff-size 26)

\header {
  titolo-xx = "Amarilli mia bella"
  titolo-he = "אמרילי יפתי"
  titolo-eo = "Amarilio mia bela"
  komponisto-xx = ""
  komponisto-he = "ג׳וליו קצ׳יני"
  komponisto-eo = "Giulio Caccini"
  ikono = "💓"
}

\include "../titolo.ly"


melodio = {
  \time 4/4
  \key c \minor
  c'2. g4\4 |
  aes2 e4 f |
  g2\4 g4\4 c' |
  bes g\4 bes4. aes8 |
  g2\4 c' ~ |
  c'4 bes8 aes bes2 |
  aes c' ~ |
  c'4 bes aes2 ~ |
  aes4 g8\4 g\4 g2\4 |
  f1 |
  \repeat volta 2 {
	aes2. g8\4 f |
	g2\4 r8 e e f |
	g4\4 e f2 |
	g1\4 |
	\break aes4. f8 g4\4 f8 e |
	f2 g\4 |
	aes2. bes8 aes |
	g2\4 ~ g8\4 e e f |
	g4\4 ees d2 |
	c r4 e8 e |
	f2. e16. f32 g8\4 |
	e2 r4 g8\4 g\4 |
	a4 bes2 a16.\4 g32\4 a8\4 |
	bes2.\4 b8 b |
	c'2. aes8. g16\4 |
	g1\4 |
  }
  \alternative {
	{ f1 | }
	{ f2 c'4 c' | }
  }
  d'2 ~ d'8 c'16 bes a8 bes |
  c'2. bes8. a16 |
  a8. bes16\4 a8. bes16\4 c' a bes\4 g\4 a32 g\4 a f ~ f8 |
  g1\4 |
  f |
  \bar "|."
}

\include "../muziko.ly"
