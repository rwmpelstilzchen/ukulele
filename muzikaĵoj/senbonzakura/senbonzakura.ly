\include "../ĉiea.ly"

\header {
  titolo-xx     = "千本桜"
  titolo-he     = "סמבונזקורה"
  titolo-eo     = "Sembonzakura"
  komponisto-xx = "黒うさP"
  komponisto-he = "קורוסה-פי"
  komponisto-eo = "Kurosa-P"
  ikono         = "🄼"
}

\include "../titolo.ly"

melodio = {
  \key c \major
  \time 4/4
  \tempo 4 = 154
  a8. a g8\4 a8. a g8\4 |
  a8. a g8\4 a4 c' |
  a8. a g8\4 a8. a g8\4 |
  a4 c' d' e' |
  \break
  d'8 e' a16 g\4 a g\4 d'8 e' a16 g\4 a g\4 |
  d'8 e' a16 g\4 a g\4 c'8 \tuplet 3/2 { b16 c' b } a8 g\4 |
  d' e' a16 g\4 a g\4 d'8 e' a16 g\4 a g\4 |
  a8 c'\3 e'\2 c'' b'16 c'' b' a' g'8 e'\2 |
  \break
  d'\2 e'\2 a16 g\4 a g\4 d'8 e' a16 g\4 a g\4 |
  d'8 e' a16 g\4 a g\4 c'8 \tuplet 3/2 { b16 c' b } a8 g\4 |
  a g16\4 a c'8\2-3 a16 c'\2 e'8 d'16 e' g'8 e'16 g' |
  c'8\2 \tuplet 3/2 { b16\2 c'\2 b\2 } a8 g\4 a g\4 a c' |
  \pageBreak
  %\break
  \repeat volta 2 {
    d' e' a16 g\4 a g\4 d'8 e' a16 g\4 a g\4 |
    d'8 e' a16 g\4 a g\4 c'8 \tuplet 3/2 { b16 c' b } a8 g\4 |
    d' e' a16 g\4 a g\4 d'8 e' a16 g\4 a g\4 |
    c8 c e c' b16 c' b a g8\4 e |
	\break
    d' e' a16 g\4 a g\4 d'8 e' a16 g\4 a g\4 |
    d'8 e' a16 g\4 a g\4 c'8 \tuplet 3/2 { b16 c' b } a8 g\4 |
    d16 c d g\4 a g\4 e d c8 c d e |
    a8. a g8\4 a2 |
	\break
    a4 a8. g16\4 a8 c' c' d' |
    a4 a8. g16\4 a8 g\4 e g\4 |
    a4 a8. g16\4 a8 c' c' d' |
    e'4 d'8 e'16 d' c'4-. a-. |
	\break
    a a8. g16\4 a8 c' c' d' |
    a4 a8. g16\4 a8 g\4 e g\4 |
    a4 a8. g16\4 a8 c' c' d' |
    e'4 d'8 e'16 d' c'4-. a-. |
	\break
    c' b a g\4 |
    g8\4 g16\4 a e8 d e2 |
    e8 g\4 a4 d' b |
    c' b8 g\4 a2 |
	\break
    c'4 b a g\4 |
    g8\4 g16\4 a e8 d e4 e8 g\4 |
    a a r a c'4 d' |
    b2 r4 a8 c'\2 |
	\break
    d'8.\2 d'\2 e'8\2 e'4.\2 e'8\2 |
    g' a' d'\2 c'\2 e'4\2 a8 c'\2 |
    d'8.\2 d'\2 e'8\2 e'4.\2 e'8\2 |
    f'\2 e'\2 d'\2 c'\2 c'4\2 a8 c'\2 |
	\break
    d'8.\2 d'\2 e'8\2 e'4.\2 e'8\2 |
    g' a' d'\2 c'\2 e'4\2 a8 c'\2 |
	\set TabStaff.minimumFret = #8
    f'4 e' d' c' |
    d'8 e' b g\4 a4 a8 c' |
	\break
    d'8. d' e'8 e'4. e'8 |
    g' a' d' c' e'4 a8 c' |
    d'8. d' e'8 e'4. e'8 |
    f' e' d' c' c'4 a8 c' |
	\break
    d'8. d' e'8 e'4. e'8 |
    g' a' d' c' e'4 a8 c' |
    f'4 e' d' c' |
    d'8 c' e' g' a'2 |
  }
  \break
  \set TabStaff.minimumFret = #0
  \acciaccatura { gis'8 } a' a' a' a' a' a' a'16 g' e'8 |
  \set TabStaff.minimumFret = #8
  \acciaccatura { c' } d' d' d' d' d' d' d'16 c' a8 |
  \set TabStaff.minimumFret = #0
  a a a a a a a16 g\4 e8 |
  d e16 d e g\4 gis a b d' e' c' b8 a16 b |
  \break
  \set TabStaff.minimumFret = #7
  \set TabStaff.restrainOpenStrings = ##t
  e' c' a e' ~ e' e' c' a e' c' a e' ~ e' e' c' a |
  f' d' bes f' ~ f' f' d' bes f' d' bes f' ~ f' f' d' bes |
  f' c' a f' ~ f' f' c' a f' c' a f' ~ f' f' c' a |
  \set TabStaff.minimumFret = #5
  d' b g\4 d' ~ d' d' b g\4 e' b gis e' ~ e' e' b gis |
  \break
  \set TabStaff.restrainOpenStrings = ##f
  \set TabStaff.minimumFret = #10
  e' c' a e' a' e' c' a d' c' a e' \tuplet 3/2 { c''16 b' a' } e'16 c' |
  f' d' bes f'32 a' bes'16 f' d' bes f' d' bes f' \tuplet 3/2 { d'16 c' bes } \set TabStaff.minimumFret = #0 f16 d |
  a f c f32 b c'16 f a c' f' c' a f f d c c |
  c'8 a16 f c f a c' d' b g\4 d d g\4 b d' |
  e'8. dis' e'16 dis' e'2 |
  \break
  c'4 b a g\4 |
  g8\4 g16\4 a e8 d e4 r |
  e8 g\4 a4 d' b |
  c' b8 g\4 a4 r |
  \break
  c' b a g\4 |
  g8\4 g16\4 a e8 d e4 e8 g\4 |
  a a r a c'4 d' |
  b2. a8 c'\2 |
  \break
  %d'8. d' e'8 e'4. e'8 |
  %g' a' d' c' e'4 a8 c' |
  %d'8. d' e'8 e'4. e'8 |
  %f' e' d' c' c'4 a8 c' |
  d'8.\2 d'\2 e'8\2 e'4.\2 e'8\2 |
  g' a' d'\2 c'\2 e'4\2 a8 c'\2 |
  d'8.\2 d'\2 e'8\2 e'4.\2 e'8\2 |
  f'\2 e'\2 d'\2 c'\2 c'4\2 a8 c'\2 |
  \break
  %d'8. d' e'8 e'4. e'8 |
  %g' a' d' c' e'4 a8 c' |
  d'8.\2 d'\2 e'8\2 e'4.\2 e'8\2 |
  g' a' d'\2 c'\2 e'4\2 a8 c'\2 |
  \set TabStaff.minimumFret = #8
  f'4 e' d' c' |
  d'8 e' d' e' e'4 b8 d' |
  \break
  e'8. e' fis'8 fis'4. fis'8 |
  a' b' e' d' fis'4 b8 d' |
  e'8. e' fis'8 fis'4. fis'8 |
  g' fis' e' d' d'4 b8 d' |
  \break
  e'8. e' fis'8 fis'4. fis'8 |
  a' b' e' d' fis'4 b8 d' |
  g'4 fis' e' d' |
  e'8 d' fis' a' b'2 |
  \break
  e'8 fis' b16 a b a e'8 fis' b16 a b a |
  e'8 fis' b16 a b a d'8 \tuplet 3/2 { cis'16 d' cis' } b8 a |
  e' fis' b16 a b a e'8 fis' b16 a b a | 
  b8 \set TabStaff.minimumFret = #12 a fis'-2 d'' cis''16 d'' cis'' b' a'8 fis' |
  \break
  \set TabStaff.minimumFret = #7
  e' fis' b16 a b a e'8 fis' b16 a b a |
  \set TabStaff.minimumFret = #9
  e'8-3 fis' b16 a b a d'8 \tuplet 3/2 { cis'16 d' cis' } b8 a |
  e'16 d' fis' a' b' a' fis' e' b8 d' e' fis' |
  b8. b a8 b4. a16 ais |
  b8. b a16 ais b2 \bar "|."
}

old = \transpose g, c \relative e {
    \clef "treble" \key g \major \numericTimeSignature\time 4/4 | % 1
    \tempo 4=154 e8.  e8. d8  e8.  e8. d8  | % 2
    e8.  e8. d8  e4 g4 | % 3
    e8.  e8. d8  e8.  e8. d8  | % 4
    e4 g4 a4 b4 | % 5
    a8  b8  e,16  d16 e16 d16  a'8  b8  e,16  d16 e16 d16 
     | % 6
    a'8  b8  e,16  d16 e16 d16  g8  
    \times 2/3  {
        fis16 g16 fis16  }
    e8  d8  | % 7
    a'8  b8  e,16  d16 e16 d16  a'8  b8  e,16  d16 e16 d16  | % 8
    e8  g8 b8 g'8  fis16  g16 fis16 e16  d8  b8  | % 9
    a8  b8  e,16  d16 e16 d16  a'8  b8  e,16  d16 e16 d16 
     | 
    a'8  b8  e,16  d16 e16 d16  g8  
    \times 2/3  {
        fis16 g16 fis16  }
    e8  d8  | % 11
    e8  d16 e16  g8  e16 g16  b8  a16 b16  d8  b16 d16  | % 12
    g,8  
    \times 2/3  {
        fis16 g16 fis16  }
    e8  d8  e8  d8 e8 g8  \repeat volta 2 {
        | % 13
        a8  b8  e,16  d16 e16 d16  a'8  b8  e,16  d16 e16 d16 
         | % 14
        a'8  b8  e,16  d16 e16 d16  g8  
        \times 2/3  {
            fis16 g16 fis16  }
        e8  d8  | % 15
        a'8  b8  e,16  d16 e16 d16  a'8  b8  e,16  d16 e16 d16 
        | % 16
        g,8  g8 b8 g'8  fis16  g16 fis16 e16  d8  b8  | % 17
        a'8  b8  e,16  d16 e16 d16  a'8  b8  e,16  d16 e16 d16 
         | % 18
        a'8  b8  e,16  d16 e16 d16  g8  
        \times 2/3  {
            fis16 g16 fis16  }
        e8  d8  | % 19
        a16  g16 a16 d16  e16  d16 b16 a16  g8  g8 a8 b8  |
        
        e8.  e8. d8  e2 ^\markup { \musicglyph
            #"scripts.caesura.straight" } | % 21
        e4 e8.  d16  e8  g8 g8 a8   | % 22
        e4 e8.  d16  e8  d8 b8 d8  | % 23
        e4 e8.  d16  e8  g8 g8 a8  | % 24
        b4 a8  b16 a16  g4 -. e4 -. | % 25
        e4 e8.  d16  e8  g8 g8 a8  | % 26
        e4 e8.  d16  e8  d8 b8 d8  | % 27
        e4 e8.  d16  e8  g8 g8 a8   | % 28
        b4 a8  b16 a16  g4 -. e4 -. | % 29
        g4 fis4 e4 d4 | 
        d8  d16 e16  b8  a8  b2 | % 31
        b8  d8  e4 a4 fis4 | % 32
        g4 fis8  d8  e2 | % 33
        g4 fis4 e4 d4 | % 34
        d8  d16 e16  b8  a8  b4 b8  d8   | % 35
        e8  e8  r8 e8 g4 a4 | % 36
        fis2 r4 e8  g8  | % 37
        a8.  a8. b8  b4. b8 | % 38
        d8  e8 a,8 g8  b4 e,8  g8  | % 39
        a8.  a8. b8  b4. b8 | 
        c8  b8 a8 g8  g4 e8  g8   | % 41
        a8.  a8. b8  b4. b8 | % 42
        d8  e8 a,8 g8  b4 e,8  g8  | % 43
        c4 b4 a4 g4 | % 44
        a8  b8 fis8 d8  e4 e8  g8  | % 45
        a8.  a8. b8  b4. b8 | % 46
        d8  e8 a,8 g8  b4 e,8  g8  \pageBreak | % 47
        a8.  a8. b8  b4. b8 | % 48
        c8  b8 a8 g8  g4 e8  g8  | % 49
        a8.  a8. b8  b4. b8 | 
        d8  e8 a,8 g8  b4 e,8  g8  | % 51
        c4 b4 a4 g4 | % 52
        a8  g8 b8 d8  e2 }
     | % 53
    \acciaccatura { dis8 } e8  e8 e8 e8  e8  e8  e16  d16 b8  | % 54
    \acciaccatura { g8 } a8  a8 a8 a8  a8  a8  a16  g16 e8  | % 55
    e8  e8 e8 e8  e8  e8  e16  d16 b8  | % 56
    a8  b16 a16  b16  d16 dis16 e16  fis16  a16 b16 g16  fis8  e16
    fis16   | % 57
    b16  g16 e16 b'16 ~  b16  b16 g16 e16  b'16  g16 e16 b'16 ~  b16
     b16 g16 e16  | % 58
    c'16  a16 f16 c'16 ~  c16  c16 a16 f16  c'16  a16 f16 c'16 ~ 
    c16  c16 a16 f16  | % 59
    c'16  g16 e16 c'16 ~  c16  c16 g16 e16  c'16  g16 e16 c'16 ~ 
    c16  c16 g16 e16   | 
    a16  fis16 d16 a'16 ~  a16  a16 fis16 d16  b'16  fis16 dis16 b'16
    ~  b16  b16 fis16 dis16  | % 61
    b'16  g16 e16 b'16  e16  b16 g16 e16  a16  g16 e16 b'16  
    \times 2/3  {
        g'16  fis16 e16  }
    b16  g16   | % 62
    c16  a16 f16 c'32 e32  f16  c16 a16 f16  c'16  a16 f16 c'16 
    \times 2/3  {
        a16  g16 f16  }
    c16  a16  | % 63
    e'16  c16 g16 c32 fis32  g16  c,16 e16 g16  c16  g16 e16 c16 
    c16  a16 g16 g16   | % 64
    g'8  e16 c16  g16  c16 e16 g16  a16  fis16 d16 a16  a16  d16
    fis16 a16  | % 65
    b8.  ais8.  b16  ais16  b2 | % 66
    g4 fis4 e4 d4 | % 67
    d8  d16 e16  b8  a8  b4 r4 | % 68
    b8  d8  e4 a4 fis4  | % 69
    g4 fis8  d8  e4 r4 | 
    g4 fis4 e4 d4 | % 71
    d8  d16 e16  b8  a8  b4 b8  d8  | % 72
    e8  e8  r8 e8 g4 a4 | % 73
    fis2. e8  g8  | % 74
    a8.  a8. b8  b4. b8 | % 75
    d8  e8 a,8 g8  b4 e,8  g8   | % 76
    a8.  a8. b8  b4. b8 | % 77
    c8  b8 a8 g8  g4 e8  g8  | % 78
    a8.  a8. b8  b4. b8 | % 79
    d8  e8 a,8 g8  b4 e,8  g8  | 
    c4 b4 a4 g4 | % 81
    a8  b8 a8 b8  b4 fis8  a8   | % 82
    b8.  b8. cis8  cis4. cis8 | % 83
    e8  fis8 b,8 a8  cis4 fis,8  a8  | % 84
    b8.  b8. cis8  cis4. cis8 | % 85
    d8  cis8 b8 a8  a4 fis8  a8  | % 86
    b8.  b8. cis8  cis4. cis8  | % 87
    e8  fis8 b,8 a8  cis4 fis,8  a8  | % 88
    d4 cis4 b4 a4 | % 89
    b8  a8 cis8 e8  fis2 | 
    b,8  cis8  fis,16  e16 fis16 e16  b'8  cis8  fis,16  e16 fis16
    e16  | % 91
    b'8  cis8  fis,16  e16 fis16 e16  a8  
    \times 2/3  {
        gis16 a16 gis16  }
    fis8  e8  \pageBreak | % 92
    b'8  cis8  fis,16  e16 fis16 e16  b'8  cis8  fis,16  e16 fis16
    e16  | % 93
    fis8  e8 cis'8 a'8  gis16  a16 gis16 fis16  e8  cis8  | % 94
    b8  cis8  fis,16  e16 fis16 e16  b'8  cis8  fis,16  e16 fis16
    e16   | % 95
    b'8  cis8  fis,16  e16 fis16 e16  a8  
    \times 2/3  {
        gis16 a16 gis16  }
    fis8  e8  | % 96
    b'16  a16 cis16 e16  fis16  e16 cis16 b16  fis8  a8 b8 cis8  | % 97
    fis,8.  fis8. e8  fis4. e16  eis16  | % 98
    fis8.  fis8. e16 eis16  fis2 
	\bar "|."
}

\include "../muziko.ly"
