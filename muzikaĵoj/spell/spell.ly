\include "../ĉiea.ly"

\header {
  titolo-xx     = "Under your Spell"
  titolo-he     = "תחת כישופך"
  titolo-eo     = "Sub via sorĉo"
  komponisto-xx = ""
  komponisto-he = "ג׳וס ווידון"
  komponisto-eo = "Joss Whedon"
  ikono         = "🜨"
}

\include "../titolo.ly"

melodio =  {
  \time 4/4
  \key c \major
  \repeat volta 2 {
	\repeat unfold 2 {
	  r4 g8\4 g8\4 ~ g8\4 e4 g8\4~ |
	  g8\4 a4 g8\4 ~ g8\4 d4. |
	  fis8 fis fis fis~ fis e fis g\4 |
	}
	\alternative {
	  { g2\4 r | }
	  { g4\4 r4 r r8 e }
	}
	\break
	e4. g8\4~ g\4 c d4~ |
	d2 r |
	e8 e4 e8~ e e g4\4 |
	g2\4 r |
	\break
	c8 c'4 b8~ b c'4 g8\4~ |
	g4\4 r a8 g\4 f e |
	d4 r a8 g\4 f e |
	d4 e8 f~ f4 r8 c8 |
	\break
	c8 c'4 b8~ b c'4 g8\4~ |
	g4\4 r a8 g\4 f e |
	d4 r a8 g\4 f e |
  }
  \alternative {
	{ d4 e8 f8~ f2 | }
	{ d4 e8 f8~ f4 r8 d |}
  }
  e4 r a a8 cis'~ |
  cis'8 b4.~ b2~ |
  b1 |
  \compressFullBarRests
  R1*4 |
  \pageBreak
  r2 r4 r8 e8 |
  e4. g8\4~ g\4 c d4~ |
  d2 r2 |
  e8 e4 e8~ e e g4\4 |
  g2.\4 r4 |
  \break
  \repeat unfold 2 {
	c8 c'4 b8~ b c'4 g8\4~ |
	g4\4 r a8 g\4 f e |
	d4 r a8 g\4 f e |
  }
  \alternative {
	{ d4 e8 f~ f4 r8 c8 | }
	{ d4 e8 f~ f4 r8 d8 | }
  }
  \break
  e4 r a a8 cis'~ |
  cis'8 b4.~ b2~ |
  b1 |
  R1*2 |
  \repeat volta 3 {
	r4 a8 a~ a b4 cis'8~ |
	cis'4. b8~ b2 |
  }
}

\include "../muziko.ly"
