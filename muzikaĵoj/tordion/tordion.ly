\include "../ĉiea.ly"

\header {
  titolo-xx     = "Tordion"
  titolo-he     = "טורדיון"
  titolo-eo     = "Tordiono"
  komponisto-xx = ""
  komponisto-he = ""
  komponisto-eo = ""
  ikono         = "🔀"
}

\include "../titolo.ly"

melodio = {
  \key d \minor
  \time 3/4
  \repeat volta 2 {
	d8 e f g\4 f e |
	d4. e8 f g\4 |
	a g\4 f f g\4 e |
	f4 e8 d c4 |
	\break
	d8 e f g\4 f e |
	d4 f e |
	d2 c4 |
	d2. |
  }
  \repeat volta 2 {
	\break
	a4. g8\4 a bes |
	a2 a4 |
	c'8 bes a g\4 f e |
	f4. e8 d4 |
	\break
	a4. g8\4 a bes |
	a4 g8\4 f e4 |
	d2 c4 |
	d2. |
  }
}

\include "../muziko.ly"
