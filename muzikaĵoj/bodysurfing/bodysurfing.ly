\include "../ĉiea.ly"

\header {
  titolo-xx     = "Bodysurfing"
  titolo-he     = "גלישת־גוף"
  titolo-eo     = "Korpo-surfado"
  komponisto-xx = ""
  komponisto-he = "אוטה־סאן"
  komponisto-eo = "Ohta-San"
  ikono         = "🌊"
}

\include "../titolo.ly"

repeatGliss = #(define-music-function (parser location grace)
  (ly:pitch?)
  #{
    % the next two lines ensure the glissando is long enough
    % to be visible
    \once \override Glissando.springs-and-rods
      = #ly:spanner::set-spacing-rods
    \once \override Glissando.minimum-length = #3.5
    \once \hideNotes
    \grace $grace \glissando
  #})

A = {
  \repeat volta 2 {
	bes'8 d'\2 g' a' bes'8 d'\2 g' a' |
	bes'8 d'\2 g' a' bes' a' g' bes' |
	a'8 c'\2 f' g' a' c'\2 f' g' |
	a'8 c'\2 f' g' a' g' f' a' |
	g'8 bes\2 ees' f' g' bes\2 ees' f' |
	g'8 bes\2 ees' f' g'2\glissando |
  }
  \alternative {
	{
	  fis'8 fis'4 g'4 g' bes'8 ~ |
	  bes'8 bes' bes' bes' bes'4 a' |
	}
	{
	  %{repeatGliss%}
	  d'8 d'4 ees'4 ees' d'8 ~ |
	  d'8 d' d' d' d'4 d' |
	}
  }
}

B = {
  c'4 r8 c'4 c'8 r8 c'8 |
  c'4 r8 c'4 c'8 r8 c'8 |
  a8 bes4 a8 bes4 a8 bes8 ~ |
  bes8 a8 bes4 a4 bes4 |
  \break
  e'4 r8 e'4 e'8 r8 e'8 |
  e'4 r8 e'4 e'8 r8 e'8 |
  \repeat unfold 2 {d'16 d'16 d'16 d'16 ees'16 ees'16 ees'16 ees'16} |
  d'8 d'8 ees'8 d'4. d'4 |
  \break
  bes'4 r8 bes'4 bes'8 r8 bes'8 |
  bes'4 r8 bes'4 bes'8 r8 bes'8 |
  a'4 r8 g'4 g'8 r8 g' |
  a'4 r8 g'4 g'8 r8 g' |
  \break
  c''4 r8 c''4 c''8 r8 c'' |
  c''4 r8 c''4 c''8 r8 c'' |
  a'4 r8 g'4 g'8 r8 g' |
  d'8 d' d' d'4 d'8 d'4 |
}
melodio = {
  \tempo 4 = 180
  \time 4/4
  \key g \minor
  \set Timing . beamExceptions = #'()
  \A
  \break
  \repeat volta 2 {
	\repeat unfold 2 {
	  a16( bes8.) bes2:16 a4:16 |
	  a4. g4 a4. |
	}
	\repeat unfold 2 {
	  a16 a8. a2:16 g4:16 |
	  g4. f4 g4. |
	}
  }
  \break
  a8 a r a4 bes4. |
  c'4 r8 bes4 bes8 r8 bes |
  a4 r8 bes4 bes8 r8 bes8 |
  c'4 r8 bes4 bes8 r8 bes |
  \break
  \B
  \pageBreak
  \A
  \break
  \repeat unfold 2 {
	g8 d' d' c' d' d' c' f' |
	% 3/5 @ orig
	r8 d' ees'( d') ees'( d') bes4\2 |
  }
  \break
  \hideNotes \grace c8\glissando \unHideNotes %hack
  g8\3 bes\2 g\3 cis'4\2\bendAfter #4 cis'4\2\bendAfter #4 d'8\2
  c'4\2 bes8\2 bes4\2 a8\2 g16\3 bes\2 d'8 |
  ees'8 f' f'(\glissando g') g'(\glissando a') a' a'(\glissando |
  bes'8) a'4 g' g'8 g'4:16 |
  g'8 a'4 bes'4 a'8 a'4:16 |
  a'16 g' g' g' g'4:16 g'16 f' f' f' f'4:16 |
  f'16 ees' ees' ees' ees'4:16 ees'16 d' d' d' d'4:16 |
  \repeat unfold 2 {\repeat unfold 4 {ees'16 d' d' d'} |}
  \repeat unfold 2 {\repeat unfold 4 {g'16 f' d' d'} |}
  g'4:16 a':16 bes':16 a':16 |
  <f bes>2(\glissando <d' g'>) | 
  a4 r8 bes4 bes8 r8 bes |
  bes8 a4 g d8 g a |
  bes4 a8 bes4 c'8(\glissando d') d' ~ |
  d'4 d'4 c' bes |
  \break
  \B
  % 4/5 + 6 measures
  \pageBreak
  \repeat volta 2 {
	d'4 r8 r4 r4 r8 |
	r4 r8 r4 r4 r8 |
	f'4 r8 r4 r4 r8 |
	r4 r8 r4 r4 r8 |
	ees'4 r8 r4 r4 r8 |
	r4 r8 r4 r4 r8 |
	d'4 d'8 ees'4 ees'4 d'8 ~ |
	d'8 d' d' d' d'4 d' |
  }
  \break
  \repeat unfold 4 {bes'16 a' g' g'} |
  bes'16 a' g'4:16 bes'16 a' g'4:16 bes'16 a' g' g' |
  \repeat unfold 4 {a'16 g' f' f'} |
  a'16 g' f'4:16 a'16 g' f'4:16 a'16 g' f'16 f'16 |
  \repeat unfold 4 {g'16 f' ees' ees'} |
  g'16 f' ees'4:16 g'16 f' ees'4:16 g'16 f' ees' ees' |
  d'4:16 ees'4:16 f'4:16 g'4:16 |
  a'4:16 bes'4:16 c''4:16 d''4:16 |
  \repeat unfold 4 {bes'16 a' g' g'} |
  % p. 5/5
  d''1:16 |
  \repeat unfold 4 {a'16 g' f' f'} |
  c''1:16 |
  \repeat unfold 4 {g'16 f' ees' ees'} |
  bes'1:16 |
  f'8 ees' d' ees' d' c'\2 d' c'\2
  \bendAfter #4
  %{\bendAfter%}
  |
  c'8\2 bes\2 c'\2 bes\2 g2\4
  \break
  \repeat volta 2 {
	bes'8 d'\2 g' a' bes'8 d'\2 g' a' |
	bes'8 d'\2 g' a' bes' a' g' bes' |
	a'8 c'\2 f' g' a' c'\2 f' g' |
	a'8 c'\2 f' g' a' g' f' a' |
	g'8 bes\2 ees' f' g' bes\2 ees' f' |
  }
  \alternative {
	{
	  g'8 bes\2 ees' f' g'2\glissando |
	  d'8 d'4 ees'4 ees' d'8 ~ |
	  d'8 d' d' d' d'4 d' |
	}
	{
	  g'8 bes\2 ees' f' g' bes\2 ees' f' |
	  fis'4. ees'8 d'16 ees' d'8 c'\2 ees' |
	  c'4\2\bendAfter #4 c'8\2 bes\2 g\2 bes\2 g\2 f\3 |
	}
  }
  \pageBreak
  g8\2 bes\2 c'\2 bes\2 d'8.\2 g'8 d'8.\2 |
  g'8 d'8.\2 g'8 d'8.\2 g'8 d'4\2 |
  g'4. %{glissando%} d'8\2 g' a' bes'4 |
  a'4 g' f' ees' |
  d'16 ees' d'8 c'\2 bes\2 g\2 bes\2 c'\2 d'\2 |
  c'8\2 bes\2 g2.\3 |
  \bar "|."
}

\include "../muziko.ly"
