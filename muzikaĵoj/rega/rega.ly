\include "../ĉiea.ly"

\header {
  titolo-xx     = ""
  titolo-he     = "רגע עם דודלי"
  titolo-eo     = "Rega kaj Dodli"
  komponisto-xx = ""
  komponisto-he = "רומן קונסמן"
  komponisto-eo = "Roman Kunsman"
  ikono         = "🎁"
}

\include "../titolo.ly"

melodio = {
  \time 4/4
  \key d \major
  fis8 a a8. a16 fis8 a a a |
  fis8\3-2 a\2 d' e' fis' d' a\2 fis\3 |
  g8.\4 a16 b8 g\4 e fis g4\4 |
  fis8. a16 d8 fis e cis d4 |

  \break

 % fis8 a a a fis a a a |
 % fis8 a d' e' fis' d' a fis |
 % g8\4 a b g\4 e fis g4\4 |
 % fis8. a16 d8 fis e cis d4 |
  \interim
  b8. d'16 g8\4 b a fis g4\4 |
  \repeat unfold 2 {g16\4 d g\4 d g8\4 d8} |
  d'1 |
  \bar "||"
  \deinterim

  \break

  fis8 a a a fis a a a |
  fis8\3-2 a\2 d' e' fis' d' a\2 fis\3 |
  g8\4 a b g\4 e fis g4\4 |
  fis8. a16 d8 fis e cis d4 |
  \interim
  \transpose c d { g8. g16 g8 g8 c'2 |}
  \deinterim
  \bar "|."
}

\include "../muziko.ly"
