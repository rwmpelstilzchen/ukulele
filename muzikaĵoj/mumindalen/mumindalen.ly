\include "../ĉiea.ly"

\header {
  titolo-xx     = "楽しいムーミン一家"
  titolo-he     = "מנגינות מעמק המומינים"
  titolo-eo     = "Melodioj de Muminvalo"
  komponisto-xx = "白鳥澄夫"
  komponisto-he = "סומיאו שירטורי"
  komponisto-eo = "Sumio Ŝiratori"
  ikono         = "𓃯"
}

\include "../titolo.ly"


% Originally in C major
snufkin_a = {
  \mark "I"
  \key d \major
  \time 4/4
  \partial 8 d8 |
  g a b4 ais8 b d'4 |
  b8 g\4 e2 ~ e8 e |
  a b c'4. c'8 d'4 |
  a8 c' b2 ~ b8 g\3 |
  \break
  e' d' c'4\2 c'8\2 d' e'4 |
  fis'8 e' d'4 b8\2 a\2 g4\3 ~ |
  g8\3 a16\2 b\2 a4\2 a8\2 b\2 c'\2 e' |
  d'1 |
  \break
  fis4 e8 fis d4 e |
  fis fis8 a d'4 a |
  b a8 g fis4 d |
  e g8 fis e2 |
  \break
  fis4 e8 fis d4 e8 fis |
  g fis g b d'4 cis'8 b |
  a4 fis8 d g fis e4 |
  d2 ~ d4. d'8 |
  \break
  b4 b8 d' cis'4 b |
  a fis8 e d4. e16 fis |
  e4. e8 e fis g b |
  a2 ~ a4. d'8 |
  \break
  b4 b8 d' cis'4 b |
  a fis8 e d4. e16 fis |
  g4. e8 cis4 e |
  d1 |
  \bar "|."
}


% Originally in C major
snufkin_b = {
  \mark "II"
  \key f \major
  \time 6/4
  a4.\4 gis8\4 a\4~ a\4 a4.\4 gis8\4 a~\4 a\4 |
  a4.\4 f8 e g f2 c4 |
  \break
  bes4. a8 bes ~ bes bes4. a8 bes ~ bes |
  c'4. bes8 g bes a2 c8 f |
  \break
  a4.\4 gis8\4 a\4~ a\4 a4.\4 gis8\4 a~\4 a\4 |
  bes4.\4 g8 e g f4. g8 a f |
  \break
  c d e f g4 bes8 a g4 g |
  g f4. e8 f2. |
  \break
  d'4. cis'8 d' ~ d' d'4. cis'8 d' ~ d' |
  d'4. bes8 f d' c'2. |
  \break
  c8 d e f g4 bes8 a g4 g |
  bes a g f4. e8 f4\fermata |
  \bar "|."
}


% Originally in D minor
snufkin_c = {
  \mark "III"
  \key a \minor
  \time 4/4
  \repeat volta 2 {
	a a8 b c'4 f |
	e b2. |
	gis4\4 gis8\4 a b gis\4 e d |
	c4 b a2 |
	c'8-1\4( g\4 c'\4) d' e'4 c'\4 |
	d'8 b\2 g2\4 b8\2 c'\4 |
	d' b\2 d' e' f' d' b\2 d' |
	c'\4( g\4 c'\4) d' e' d' b4\2\fermata |
  }
  a1 |
  \bar "|."
}

% Originally in C major
snufkin_d = {
  \mark "VI"
  \key d \major
  \time 4/4
  \repeat volta 2 {
	a4 fis fis fis8 a |
	g4\4 g\4 e2 |
	g4\4 e e e8 b |
	a4\4( gis\4 a2\4) |
	a4 fis fis8 a d' cis' |
	b4 a g2\4 |
  }
  \alternative {
	{
	  fis4 a g\4 cis |
	  e2 d |
	}
	{
	  e4 fis g\4 cis |
	  e d8( cis d2) |
	}
  } 
  \bar "|."
}

% Originally in C major (high)
snufkin_e = {
  \mark "V"
  \time 6/4
  \key f \major
  c4 \appoggiatura f4 a2 gis4\4 a c'-. |
  c d2 f4 bes2 |
  bes8 a g2 g4 g a |
  bes d' c' b8 d' c'2 |
  c4 \appoggiatura f4 a2 gis4-2\2 a\2 c'-.\1 |
  c'\4 bes2\2 a8 bes\2 d'2 |
  e'8 f'-4 c'2-2\4 d4-2 bes2 |
  d8 e f2 ~ f2. |
  \bar "|."
}

aliaj_a = {
  \mark "VI"
  \time 4/4
  \key c \minor
  \repeat volta 2 {
	g2.\4 c'4 |
	bes g2\4 f4 |
	ees c8 ees aes g f ees |
	f4 g2.\4 |
	c8 d ees d ees4 c' |
	bes2. ~ bes8 g\4 |
	aes2 c'4 ees' |
	d'4. c'8 b2 |
  }
  c'1 |
  \bar "|."
}

melodio = {
  \tempo 4 = 100
  \snufkin_a
  \break\allowPageTurn
  \set Score.currentBarNumber = #1
  \tempo 4 = 120
  \snufkin_b
  \break\allowPageTurn
  \set Score.currentBarNumber = #1
  \tempo 4 = 100
  \snufkin_c
  \break\allowPageTurn
  \set Score.currentBarNumber = #1
  \snufkin_d
  \break\allowPageTurn
  \set Score.currentBarNumber = #1
  \snufkin_e
  \break\allowPageTurn
  \set Score.currentBarNumber = #1
  \aliaj_a
}

\include "../muziko.ly"
