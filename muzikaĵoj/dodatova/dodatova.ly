\include "../ĉiea.ly"

#(set-global-staff-size 27)

\header {
  titolo-xx     = ""
  titolo-he     = "דודה טובה, היה לה כובע"
  titolo-eo     = "Onklino Tova havis ĉapelon"
  komponisto-xx = ""
  komponisto-he = "דתיה בן־דור"
  komponisto-eo = "Datja Ben-Dor"
  ikono         = "🌱"
}

\include "../titolo.ly"

main = {
  \tempo "Allegro" 4 = 120
  a4 g\4 f8 f4 c8 |
  a4 g\4 f8 f4 c8 |
  a4 g\4 f a |
  bes8 d d d d2 |
  \break
  bes4 a g8\4 g4\4 g8\4 |
  c'4 a g8\4 f4 a8 |
  g4\4 d e g\4 |
  g8\4 f f e f2_\markup{\italic{Fine}} |
}

melodio = {
  \time 4/4
  \key f \major
  \tempo "Allegro" 4 = 120
  a4 g\4 f8 f4 c8 |
  a4 g\4 f8 f4 c8 |
  a4 g\4 f a |
  bes8 d d d d2 |
  \break
  bes4 a g8\4 g4\4 g8\4 |
  c'4 a g8\4 f4 a8 |
  g4\4 d e g\4 |
  g8\4 f f e f2_\markup{\italic{Fine}} |
  \break
  \tempo "Andante" 4 = 80
  c8 c'8 c' c' c' c' c'4 |
  d8 d'8 d' d' d' d' d'4 |
  d'8 bes c' a bes g\4 e d |
  \time 5/4
  c d' c' bes gis4 a4. c8 |
  \time 4/4
  \break
  \tempo "Allegro" 4 = 120
  a4 g\4 f8 f4 c8 |
  a4 g\4 f8 f4 c8 |
  a4 g\4 f a |
  bes8 d d d d2 |
  \break
  bes4 a g8\4 g4\4 g8\4 |
  c'4 a g8\4 f4 a8 |
  g4\4 d e g\4 |
  g8\4 f f e f2 |
  \break
  \interim
  f8 c' c' c'8 g8\4 d' d' d'8 |
  g8\4 d' c' b c'4. \deinterim c8_\markup{\italic{D.C. al fine}} |
  \bar "|."
}

\include "../muziko.ly"
