\include "../ĉiea.ly"

\header {
  titolo-xx     = ""
  titolo-he     = "ערב של שושנים"
  titolo-eo     = "Vespero de rozoj"
  komponisto-xx = ""
  komponisto-he = "יוסף הדר"
  komponisto-eo = "Josef Hadar"
  ikono         = "🌹"
}

\include "../titolo.ly"

melodio = {
  \key d \minor
  \time 4/4
  \repeat volta 2 {
    d d8 c d4 f |
    g2.\4 d4 |
    g\4 g8\4 f g4\4 a |
    f2. r4 |
	%\break
    f f8 e f4 g\4 |
    e4. d8 c2 |
    e4 e8 d e4 f |
    d2. r4 |
  }
  \break

  \mark "Var. 1"
  \repeat volta 2 {
    a a8 g\4 a4 f |
    g2.\4 d4 |
    g\4 g8\4 f g4\4 a |
    f2. r4 |
    g\4 g8\4 f g4\4 a |
    f4. e8 d2 |
    e4 e8 d e4 f |
    d2. r4 |
  }
  \break

  \mark "Var. 2"
  \repeat volta 2 {
    a a8 g\4 a4 \transpose d a { f |
    g2.\4 d4 |
    g\4 g8\4 f g4\4 a |
    f2. r4 |
    g\4 g8\4 f g4\4 a |
    f4. e8 d2 |
    e4 e8 d e4 f |
    d2. r4 |
  }
  }
}

\include "../muziko.ly"
