% https://youtu.be/vEUDjHrq640

\include "../ĉiea.ly"
#(set-global-staff-size 27)

\header {
  titolo-xx     = ""
  titolo-he     = "בדינרי"
  titolo-eo     = "Badinerie"
  komponisto-xx = ""
  komponisto-he = "יוהן סבסטיאן באך"
  komponisto-eo = "Johann Sebastian Bach"
  ikono         = "🦉"
}

\include "../titolo.ly"

melodio =  {
  \key a \major
  \time 2/4
  \repeat volta 2 {
    %\set Timing . measurePosition = #(ly:make-moment -1 4 0 1)
    \partial 4 { fis'8^\markup{IX\super{3}} a'16 fis' | }
    cis'8\2 fis'16 cis'\2 a8\3 cis'16 a\2 |
    fis4\3 cis16 fis a fis |
    gis\4 fis gis\4 fis eis gis\4 b gis\4 |
    a8 fis fis' a'16 fis' |
    cis'8\2 fis'16 cis'\2 a8\3 cis'16 a\2 |
    fis4\3 a8-.\3 a-.\3 |
    a-.\3 a-.\3 fis' a\3 |
    a\3\trill gis\3 cis'-.\3 cis'-.\3 |
    cis'\3 cis'\3 a' cis'\3 |
    cis'\2\trill bis\2 gis16\3 cis'\2 e'\1 cis'\2 |
    dis'\4 cis'\2 dis'\4 cis'\2 bis\2 dis'\4 fis' dis'\4 |
    e'\1 dis'\4 e'\1 dis'\4 cis'\2 e'\4 cis'\2( bis\2 |
    cis'\2) fis' cis'\2( bis\2 cis'\2) gis' cis'\2( bis\2 |
    cis'\2) a' cis'\2( bis\2 cis'\2) a' gis' fis' |
    gis' e'\2 dis'\2 cis'\2 e'8\2 dis'\2\trill |
    cis'4\2
  }\pageBreak
  \repeat volta 2 {
    cis'8\1^\markup{IV\super{3}} e'16 cis'\1 |
    gis8 cis'16 gis e8\3 gis16 e\3 |
    cis4 g8(\4 fis)\3 |
    b8\2( ais16\2) cis'\4 e'8 d'16\4 cis'\2 |
    d'8\4 b\2 d'\4 fis'16 d'\4 |
    b8\2 d'16\4 b\2 gis8\3 b16\2 gis\3 |
    e4 ~ e16 a\2 cis' a\2 |
    b\4 a\2 b\4 a\2 gis\2 b\4 d' b\4 |
    cis' b\4 cis' b\4 a\2 cis' a(\2 gis |
    a\2) d' a\2( gis\2 a\2) e' a(\3 gis\3 |
    a\3) fis' a\3( gis\3 a\3) fis' e'\4 d'\2 |
    e'\4 cis'\2 b\3 a\3 cis'8\2 b\3\trill |
    a4\3 cis'8\2-. cis'\2-. |
    cis'\2-. cis'\2-. a'\1 cis'\2 |
    cis'\2\trill b\3 b\3-. b\3-. |
    b\3 b\3 gis' b\3 |
    b\3\trill a fis' a'16 fis' |
    \grace { e'8\2( } d'4\2) ~ d'8 fis'32\4( e'\2 d'\2 cis'\2) |
    b4\3 ~ b8 d'32( cis' b a) |
    g16 b d' b g( fis) g( fis) |
    eis8-. cis-. d( cis) |
    fis( eis16) gis b8 a16 gis |
    a8 fis32( gis a b cis'8) a16 cis'\2 |
    fis'8 cis'\2 b16\3 a\3 gis\3 a\3 |
    \grace { gis8\3( } fis4\3)
  }
}

\include "../muziko.ly"
