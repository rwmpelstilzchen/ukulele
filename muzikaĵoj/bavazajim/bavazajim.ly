\include "../ĉiea.ly"

\header {
  titolo-xx     = ""
  titolo-he     = "ברווזיים"
  titolo-eo     = "Du anasoj"
  komponisto-xx = ""
  komponisto-he = "שרה לוי-תנאי"
  komponisto-eo = "Sara Levi-Tanaj"
  ikono         = "🦆"
}

\include "../titolo.ly"

melodio = {
  \key d \minor
  \time 4/4
  \partial 4 f4
  \repeat volta 2 {
	a4 a g\4 f |
	d4 f2 d4 |
	f4 f e e|
	d2 d4 f4 |
  }
  \break
  \repeat unfold 2 {
	g2.\4 a4 |
	d2. f4 |
	a2. bes4 |
  }
  \alternative {
	{ g2.\4 f4 | }
	{ g2.\4 d4 | }
  }
  \break
  f4 f e e |
  d2 d4 d4 |
  f4 f e e |
  d2 d2 |
  \bar "|."
}

\include "../muziko.ly"
