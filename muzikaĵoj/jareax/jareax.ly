\include "../ĉiea.ly"

\header {
  titolo-xx     = ""
  titolo-he     = "ילדי הירח"
  titolo-eo     = "La lunidoj"
  komponisto-xx = ""
  komponisto-he = "שלמה יידוב"
  komponisto-eo = "Ŝlomo Jidov"
  ikono         = "☮"
}

\include "../titolo.ly"

melodio = {
  \tempo "Allegretto"
  \key ees \major
  \time 2/2
  \partial 8*2 g8\4 aes |
  \repeat volta 2 {
	bes4 bes bes c'8 bes ~ |
	bes2. g4\4 |
	a bes8 a4. f4 |
	c f2. |
	\break
	r4 r8 g\4 aes4 g8\4 f |
	ees4 d ees f |
	g\4 bes bes g8\4 bes ~ |
	bes2 r4 g\4 |
	\break
	bes bes bes c'8 ees' ~ |
	ees'4 c' bes g\4 |
	a bes a f |
	c c8 f ~ f2 |
	\break
	r4 aes g\4 f |
  }
  \alternative {
	{
	  ees d ees f8 bes ~ |
	  bes1 ~ |
	  bes2 r4 r8 f |
	  \break
	  f4 g\4 aes bes |
	  g\4 f ees d8 ees ~ |
	  ees1 ~ |
	  ees2. g8\4 aes |
	  \break
	}
	{
	  ees4 d ees f8 bes ~ |
	  bes2 c'4 des' ~ |
	  des'2. r8 f |
	  \break
	  f4 g\4 aes aes |
	  bes g\4 f ees8 ees ~ |
	  ees1 ~ |
	  ees2. r8^\segno ees |
	}
  }
  \break

  ees'4 ees'8 ees'4. ees'4 |
  ees' ees' ees' ees' |
  d'2. ~ d'8 c' |
  bes4 r8 g\4 aes4 bes |
  \break
  c'2 bes4 ees ~ |
  ees c ees f |
  g1\4 ~ |
  g\4 |
  \break
  r4 g\4 f ees |
  d g\4 c' d' |
  ees'1 ~ |
  ees'2. ces'4 |
  \break
  bes2 bes4. bes8 |
  bes4 g2.\4 |
  r1 |
  r2 r4 bes |
  \break
  bes2 bes4. bes8 |
  bes4 ges2. |
  r1 |
  r2 r4 bes |
  \break
  bes2 bes4. bes8 |
  bes4 ges2. |
  r1 |
  aes4 aes aes8 g\4 ees4 |
  %\break
  ees1 ~ |
  ees ~ |
  ees ~ |
  ees_\markup{\italic{Fine}} |
  \bar "||"
  \break
  
  \pageBreak

  bes4 bes8 bes4. g4\4 |
  bes c'8 bes4. ~ bes8 g\4 |
  a4 a a f |
  c f2. |
  \break
  r4 aes aes g8\4 f |
  ees4 d ees f |
  r g2\4 bes4 |
  c' bes8 bes4. g4\4 |
  \break
  bes bes bes c'8 ees' ~ |
  ees'4 c' bes g8\4 a ~ |
  a4 a a f |
  c c8 f ~ f2 |
  \break
  r4 aes g\4 f |
  ees d ees f |
  bes1 ~ |
  bes2. r8 f |
  \break
  f4 g\4 aes aes |
  bes g\4 g8\4 f ees4 |
  ees1 ~ |
  ees2._\markup{\italic{D.S. al Fine}}
  \bar "||"
}

\include "../muziko.ly"
