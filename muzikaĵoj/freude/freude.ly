\include "../ĉiea.ly"

\header {
  titolo-xx     = "An die Freude"
  titolo-he     = "אוֹדָה לשמחה"
  titolo-eo     = "Odo al ĝojo"
  komponisto-xx = ""
  komponisto-he = "לודוויג ון בטהובן"
  komponisto-eo = "Ludwig van Beethoven"
  ikono         = "☺"
}

\include "../titolo.ly"

melodio = \transpose g, c {
  \key c \major
  \time 4/4
  <e>4 <e>4 <f>4 <g>4 |
  <g>4 <f>4 <e>4 <d>4 |
  <c>4 <c>4 <d>4 <e>4 |
  <e>4. <d>8 <d>2 |
  \break
  <e>4 <e>4 <f>4 <g>4 |
  <g>4 <f>4 <e>4 <d>4 |
  <c>4 <c>4 <d>4 <e>4 |
  <d>4. <c>8 <c>2 |
  \break
  <d>4 <d>4 <e>4 <c>4 |
  <d>4 <e>8 <f>8 <e>4 <c>4 |
  <d>4 <e>8 <f>8 <e>4 <d>4 |
  <c>4 <d>4 <g,>2 |
  \break
  <e>4 <e>4 <f>4 <g>4 |
  <g>4 <f>4 <e>4 <d>4 |
  <c>4 <c>4 <d>4 <e>4 |
  <d>4. <c>8 <c>2 |
  \bar "|."
}

\include "../muziko.ly"
