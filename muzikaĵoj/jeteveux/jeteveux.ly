\include "../ĉiea.ly"

\header {
  titolo-xx     = "Je te veux"
  titolo-he     = "אני רוצה אותך"
  titolo-eo     = "Mi volas vin"
  komponisto-xx = ""
  komponisto-he = "אריק סטי"
  komponisto-eo = "Erik Satie"
  ikono         = "🐧"
}

\include "../titolo.ly"

melodio = {
  \time 6/8
  \key c \major
  \partial 4 e8 g\4 |
  d'4.^\segno
  c'4 e'8 |
  b4.
  a4. |

  b4.
  a4 e8 |
  b4. ~
  b8 d e |

  g4.\4 ~
  g8\4 d e |
  a4. ~
  a8 d e |

  g4\4 f8 ~
  f8 g4\4 |
  e4.
  d8 e g\4 |
\break
  d'4.
  c'4 e'8 |
  b4.
  a4 r8 |

  b4.
  a4 g8\4 |
  a4.
  d8 f g\4 |

  a8\2 b\2 c'\2
  d'8\1 e'\1 f'\1 |
  g'8 e' c'\2
  a8\2 c'\2 e'\1 |

  g4\4 f8
  e8 d4 |
  c4._\markup{\italic{Fine}} ~
  c8 b c' |
  \bar "||"
  \pageBreak

  \key g \major
  e4. ~
  e8 e fis |
  d4. ~
  d8 fis g\4 |

  d'4 d'8 ~
  d'8 d'4 |
  e'4.
  d'8 b c'|

  e4. ~
  e8 e fis |
  d4. ~
  d8 b g\4 |

  fis4 fis8 ~
  fis8 fis4 |
  b4.
  d'8 b c' |
\break
  e4. ~
  e8 e fis |
  d4. ~
  d8 fis g\4 |

  d'4 d'8 ~
  d'8 d'4 |
  fis'4.
  e'8 d' b\2 |

  e4. ~
  e8 e fis |
  d4. ~
  d8 g\4 fis |

  e8 g\4 c'
  d'4 d'8 |
  g'4.
  g8\4 e g\4_\markup{\italic{D.S. al Fine}} \key c \major |
  \bar "|."
}

\include "../muziko.ly"
