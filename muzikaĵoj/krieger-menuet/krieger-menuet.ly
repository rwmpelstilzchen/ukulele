\include "../ĉiea.ly"

\header {
  titolo-xx     = ""
  titolo-he     = "מנואט בלה מינור"
  titolo-eo     = "Menueto (a-minoro)"
  komponisto-xx = ""
  komponisto-he = "יוהן קריגר"
  komponisto-eo = "Johann Krieger"
  ikono         = "µ"
}

\include "../titolo.ly"

melodio = \transpose a f\relative e' {
  \key a \minor
  \time 3/4
  \repeat volta 2 {
	e4 c4 a4 | % 2
	f'2. | % 3
	d4 b4 g4 | % 4
	e'2.  | % 5
	a,4. c8 b8  a8  | % 6
	gis4 e4 e'4 | % 7
	d8  c8  b4. a8 | % 8
	a2. |}
	\break
	\repeat volta 2 {
	  c4 d4 e4 | 
	  a,2. | % 11
	  b4 c4 d4 | % 12
	  g,2. | % 13
	  c4 d4 e4  | % 14
	  b4 g4 g'4 | % 15
	  f8  e8  d4. c8 | % 16
	  c2. | % 17
	  e4 c4 a4 | % 18
	  f'2.  | % 19
	  d4 b4 g4 | 
	  e'2. | % 21
	  a,4. c8 b8  a8  | % 22
	  gis4 e4 e'4 | % 23
	  d8  c8  b4. a8 | % 24
	  a2. |}
	}
}

\include "../muziko.ly"
