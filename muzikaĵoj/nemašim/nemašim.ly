\include "../ĉiea.ly"

\header {
  titolo-xx     = ""
  titolo-he     = ""
  titolo-eo     = ""
  komponisto-xx = ""
  komponisto-he = ""
  komponisto-eo = ""
  ikono         = ""
}

\include "../titolo.ly"

melodio = {
  \tempo 4 = 180
  \time 6/8
  \key g \major
  \partial 4 a4 |
  fis8 g a2 |
  fis4 b2 a4 fis2 	|
  e8 fis g fis e fis |
  a4. fis8 a4 |
  \break
  fis8 g a2|
  fis4 a2|
  fis8 g8 fis8|
  e8 fis8 d8|

}

\include "../muziko.ly"
