\include "../ĉiea.ly"

#(set-global-staff-size 25)

\header {
  titolo-xx     = ""
  titolo-he     = "שיר ערש"
  titolo-eo     = "Lulkanto"
  komponisto-xx = ""
  komponisto-he = "סשה ארגוב"
  komponisto-eo = "Saŝa Argov"
  ikono         = "👑"
}

\include "../titolo.ly"

melodio = {
  \key g \minor
  \time 3/4
  \set Timing . beamExceptions = #'()
	\partial 8 d8 \bar "||"
	g\4 g\2 d c d ees |
	d4 d r8 d |
	g\4 g\2 d c d ees |
	d2 r8 d |
	g\4 bes a bes g\4 f |
	g4\4 g\4 r8 ees |
	g\4 bes a bes g\4 a |
	d4 r8 d ees d |
	ees4. c'8[ c' c'] |
	bes4 d r8 d |
	bes d a4 a|
	g2.\4 |
	r4 r r8 f |
	bes\4 d' c' bes\4 a\4 c'|
	g2\4 r8 a |
	bes c' a g\4 a f|
	bes4 bes r8 f |
	d' bes a g\4 a c' |
	\time 2/4
	g\4 g\4 a bes |
	\time 3/4
	\set Timing . beamExceptions = #'()
	c'4. g8\4[ f g\4] |
	a4 a r8 f |
	bes\4 d' bes4.\4 g8\2 |
	bes\4 d' bes4.\4 g8\2 |
	bes\4 d' bes\4 g\2 a a|
	g4\4 g\4 r|
	r2. |
	g8\4 d d4 d |
	f8 d d2 |
	g8\4 d d4 d |
	f8 d d2 |
	ees8 d ees g\4 a g\4|
	d2. |
	ees8 d ees g\4 a g\4|
	d2 r8 f |
	g\4 a bes bes bes bes |
	g2.\4 |
	bes2 a4 |
	g2.\4 |
	\bar "|."
}

\include "../muziko.ly"
