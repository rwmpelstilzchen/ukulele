\include "../ĉiea.ly"

\header {
  titolo-xx     = ""
  titolo-he     = "שתי בנות"
  titolo-eo     = "Du inoj"
  komponisto-xx = ""
  komponisto-he = "נחום נרדי"
  komponisto-eo = "Naĥum Nardi"
  ikono         = "②"
}

\include "../titolo.ly"

melodio = {
  \time 2/4
  \key f \major
  \repeat unfold 2 {
    a16 bes c'8 c' c' |
    c'16 d' c' d' bes a g8\4 |
    a g16\4 a f8 c' |
   
  }
  \alternative {
	{ bes a16 bes g8\4 r | }
	{ bes a16 g\4 c'8 c | }
  }
  \repeat unfold 2 {
    c g\4 g\4 g\4 |
    c' g\4 g\4 g\4 |
    a g\4 f16 e d8 |
  }
  \alternative {
	{ g\4 f16 a g8\4 c | }
	{ g\4 f16 e f4 | }
  }
  \bar "|."
}

\include "../muziko.ly"
