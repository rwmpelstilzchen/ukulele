\include "../ĉiea.ly"

\header {
  titolo-xx     = ""
  titolo-he     = "כבר אחרי חצות"
  titolo-eo     = "Post noktomezo"
  komponisto-xx = ""
  komponisto-he = "נורית הירש"
  komponisto-eo = "Nurit Hirŝ"
  ikono         = "🕛"
}

\include "../titolo.ly"

melodio = {
  \time 4/4
  \key d \major
  fis fis8 g\4 a4 fis |
  b2. r4 |
  e8 e fis g\4 a b cis' b ~ |
  b a ~ a2 fis8 e |
  \break d d d e fis4. d8 e e e fis g4.\4 e8 cis cis cis d e e e fis |
  d2._\markup \line { \italic "Fine"} r8 g\4 |
  \break g4\4 g8\4 a b4 d' |
  cis'4. b8 a2 g8\4 g\4 g\4 g\4 g\4 e g\4 fis fis d e fis2 r8 |
  \break g\4 g\4 g\4 g\4 a b d'4 |
  cis'4. b8 a cis'4. |
  a8 a a b c' b c' d' |
  g1_\markup \line { \italic "D.C." \italic "al" \italic "Fine"} |
  \bar "|."
 
}

\include "../muziko.ly"
