\include "../ĉiea.ly"

\header {
  titolo-xx     = ""
  titolo-he     = "צליל האוויר"
  titolo-eo     = "La sono de la aero"
  komponisto-xx = ""
  komponisto-he = "בייטו רומרו"
  komponisto-eo = "Bieito Romero"
  ikono         = "🜁"
}

\include "../titolo.ly"



melodio = {
  \key d \minor
  \time 6/8
  \partial 4 d8 e
  \repeat volta 2 {
	f8. e16 f8 g8.\4 f16 g8\4 |
	a8. g16\4 f8 d8. f16 a8 |
	c'8. d'16 c'8 b8. c'16 b8 |
	b a8. g16\4 a8 d e |
	f8. e16 f8 g8.\4 f16 g8\4 |
	a8. g16\4 f8 d8. f16 g8\4 |
	f8. e16 d8 c8. d16 e8
  }
  \alternative {
	{ d4. ~ d8 d e | }
	{ d4. ~ d8 a16 g\4 f e | }
  }
  \repeat volta 2 {
	d4 e8 c4 e8 |
	d8. e16 f8 g8.\4 f16 e8 |
	d4 e8 c4 e8 |
	e d8. c16 d8 a16 g\4 f e |
	d4 e8 c4 e8 |
	d8. e16 f8 g8.\4 f16 e8 |
	f8. e16 d8 e8. d16 c8
  }
  \alternative {
	{ d4. ~ d8 a16 g\4 f e | }
	{ d2. | }
  }
  \bar "|."
}

\include "../muziko.ly"
