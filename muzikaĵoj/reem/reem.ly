\include "../ĉiea.ly"

\header {
  titolo-xx     = "The Red Haired Boy"
  titolo-he     = "הילד הג׳ינג׳י"
  titolo-eo     = "La rufa knabo"
  komponisto-xx = ""
  komponisto-he = ""
  komponisto-eo = ""
  ikono         = "👧"
}

\include "../titolo.ly"

melodio = {
  \key f \mixolydian
  \time 2/2
  \repeat volta 2 {
	c8 d f g\4 f4 a8 bes |
	c' d' c' a bes4 a8 bes |
	c'4 f f8 g\4 a f |
	g\4 f ees4 ees r |
	\break
	c8 d f g\4 f4 a8 bes |
	c' d' c' a bes4 a8 bes\2-1 |
	c'4\2 f' f'8 d'\2 c'\2 bes\2
  }
  \alternative {
	{ a4 f f r }
	{ a f f c'8 d' }
  }
  \break
  \repeat volta 2 {
	ees' d' c' d' ees' d' c' d' |
	ees' d' c' a bes4 a8 bes |
	c'4 f f8 g\4 a f |
	g4\4 ees ees r |
	\break
	c8 d f g\4 f4 a8 bes |
	c' d' c' a bes4 a8 bes\2-1 |
	c'4\2 f' f'8 d'\2 c'\2 bes\2
  }
  \alternative {
	{ a4 f f c'8 d' | }
	{ a4 f f r | }
  } \bar "|."
  }

\include "../muziko.ly"
