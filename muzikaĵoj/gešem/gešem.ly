\include "../ĉiea.ly"

\header {
  titolo-xx     = ""
  titolo-he     = "שיר הגשם"
  titolo-eo     = "Kanto al la pluvo"
  komponisto-xx = ""
  komponisto-he = "יואל ולבה"
  komponisto-eo = "Joel Valbe"
  ikono         = "☁️"
}

\include "../titolo.ly"

melodio = {
  \time 4/4
  \key d \minor
  d8 r f r e f d r |
  g\4 d g\4 d f e d r |
  d r f r e f d r |
  g\4 d g\4 d f e d r |
  g\4 bes a16 g\4 f e d2 |
  g8\4 bes a16 g\4 f e d4.\fermata c16 e |
  \time 2/4
  d8_\markup{\italic{Fine}} r r4 |
  \bar "||"
  \break

  \time 4/4
  e8 d16 c g8\4 c f16 f f e d4 |
  g8\4 g16\4 a bes8 g16\4 g\4 a8 a16 g\4 f4 |
  a8 a16 c' g4\4 f8 f16 e d4 |
  g8\4 d g\4 d \slashedGrace d f e d4_\markup{\italic{D.C. al Fine}} |
  \bar "|."
}

\include "../muziko.ly"
