\include "../ĉiea.ly"

\header {
  titolo-xx     = ""
  titolo-he     = "בורה בלה מינור"
  titolo-eo     = "Bourrée (a-minoro)"
  komponisto-xx = ""
  komponisto-he = "יוהן קריגר"
  komponisto-eo = "Johann Krieger"
  ikono         = "🐟"
}

\include "../titolo.ly"

melodio = {
  \key d \minor
  \partial 4 a8 g\4 |
  \repeat volta 2 {
    f4 d g\4 bes |
    a g8\4 f e4 a8 g\4 |
    f4 e8 d cis4. d8 |
   
  }
  \alternative {
	{ d2. a8 g\4 | }
	{ d2. f8 e | }
  }
  \break
  \repeat volta 2 {
    f4 g8\4 a bes4 e8 d |
    e4 f8 g\4 a4 d8 c |
    d4 g8\4 f e4 ~ e8 f |
    f2. a8 g\4 |
    f4 d g\4 bes |
    a g8\4 f e4 a8 g\4 |
    f4 e8 d cis4 ~ cis8 d |
  } 
  \alternative {
	{d2. f8 e |}
	{d2. r4 |}
  }
  \bar "|."
}

\include "../muziko.ly"
