\include "../ĉiea.ly"

\header {
 titolo-xx  = "Shovel Knight"
 titolo-he  = "אביר/ת המעדר"
 titolo-eo  = "Ŝovelila Kavaliro"
 komponisto-xx = ""
 komponisto-he = "ג׳ק קאופמן"
 komponisto-eo = "Jake Kaufman"
 ikono   = "♠"
}

\include "../titolo.ly"

melodio = {
 \key c \minor %??
 \time 4/4 |
 g4\4 c4 a2 ~ |
 a4 f4 g4\4 c4 |
 a2 ~ a4 f8 g8\4 |
 as4 c'4 bes4 es4 |
 es8 f8 g2\4 bes4 |
 as1 |
 R1 |
 \break
 \repeat volta 2 {
	g4\4 c4 a4. f8 |
	g4\4 c4 a4. f16 g16\4 |
	as4 c'4 bes4 es4 |
	es8 f8 g2\4 es8 g8\4 |
	f4. c8 c2 |
	c'4. f8 g4\4 c16 d16 es16 f16 |
	\break
	g4\4 c4 a4. f8 |
	g4\4 c4 a4. f16 g16\4 |
	as4 c'4 bes4 es4 |
	es8 f8 g2\4 bes4 |
	as4. des8 des2 |
	f16 g16\4 f16 g16\4 f16 g16\4 f16 g16\4 es16 f16 as16 cis'16 g16\4
	b16 d'16 f'16 |
	\break
	g4\4 c4 a4. f8 |
	g4\4 c4 a4. f16 g16\4 |
	as4 c'4 bes4 es4 |
	es8 f8 g2\4 es8 g8\4 |
	f4. c8 c2 |
	c'4. f8 g4.\4 g16\4 as16 |
	\break
	bes4 es4 c'4. c'8 |
	d'4 g8\4 d'8\2-2 es'4\2 f'4\2 |
	g'4 bes'4 a'4 g'8 f'8 |
	g'1 | \bar "||"
	\break
	g4\4 c4 a4 f4 |
	g4\4 c4 c'4 bes4 |
	\times 2/3 { f8 bes8 bes8 }
	bes2. |
	\times 2/3 { f'8 bes'8 bes'8 }
	bes'4 \times 2/3 { f4 c8 }
	\times 2/3 { f8 g8\4 as8 }
	 |
	g4\4 c4 a4 f4 |
	g4\4 c4 c'4\2 bes4\2 |
	\times 2/3 { f8\3 bes8\2 bes8\2 } \times 2/3 { c'8\2 f'8 f'8 } f'4 \times 2/3 { f'8 bes'8 bes'8 }|
	bes'4 \times 2/3 { f4 c8 } \times 2/3 { f8 g8\4 as8 } |
 }
}

\include "../muziko.ly"
