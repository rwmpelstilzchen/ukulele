\include "../ĉiea.ly"

\header {
  titolo-xx     = ""
  titolo-he     = "הטיול הקטן"
  titolo-eo     = "La vojaĝeto"
  komponisto-xx = ""
  komponisto-he = "נעמי שמר"
  komponisto-eo = "Naomi Ŝemer"
  ikono         = "👣"
}

\include "../titolo.ly"

melodio = {
  \time 4/4
  \key c \major
  \repeat volta 2 {
	g8\4 g\4 a g\4 e4 g\4 |
	g8\4 g\4 a g\4 d4 g\4 |
	g8\4 g\4 a g\4 b4 a8 g\4 |
	d4 fis g\4 r_\markup{\italic{Fine}} |
  }
  \break
  \key c \minor
  \repeat unfold 2 {
	c'8 c' c' bes aes4 bes8 c' |
	d'4 d' r2 |
	c'8 c' c' bes aes4 bes8 c' |
  }
  \alternative {
	{ g4\4 f g\4 r | }
	{ g\4 bes c' r _\markup{\italic{D.C. al Fine}} | }
  }
  \bar "|."
}

\include "../muziko.ly"
