\include "../ĉiea.ly"

#(set-global-staff-size 27)

\header {
  titolo-xx     = "BWV 1056R"
  titolo-he     = "לרגו"
  titolo-eo     = "Largo (g-minoro)"
  komponisto-xx = ""
  komponisto-he = "יוהאן סבסטיאן באך"
  komponisto-eo = "Johann Sebastian Bach"
  ikono         = "🎹"
}

\include "../titolo.ly"


melodio = {
  \tempo 4 = 30
  \key d \minor
  \time 4/4
  a-2\2\mordent ~ a16\2 bes-2\2 c'\2 d'\2 \appoggiatura a8\2 g4\2 ~ g16\2 a\2 bes\2 c'\2 |
  f8\3 f'16 d' bes8\2 ~ bes32 c'\2 bes\2 a\2 bes16\2 g' e'\4 c'\2 bes8\2\trill a16\2 bes\2 |
  \appoggiatura bes8\2 a\2 g16\3 f\3 r4 f' ~ f'16 e'32 d' e'16 f' |
  g4\3 ~ g16 b d' f' e'4 ~ e'32 d' c' b a16 g |
  f8 ~ f32 g f e f16 a c' e' d'4 ~ d'32 c' b c' d' b g f |
  e16 g b d' c'4 ~ c'16 d'32 e' f' e' f' a' b8.\2\mordent c'16\2 |
  c'4\2 r ees' ~ ees'16 \appoggiatura d' c' \appoggiatura bes a g |
  fis4 ~ fis16 g a bes\3 c'\2 d'\2 ees'\2 fis' a' c'\2 ~ c'32\2 d'\2 ees'\2 d'\2 |
  c'16\3( bes\3) a\3\prall g\3 bes'8. d'16\2 \appoggiatura ees'\2 d'32\2 c'\2 d'16\2 bes'16. \tuplet 3/2 { a'64 g' fis' } \appoggiatura { fis'16*1/2 } g'8. d'16\2 |
  d'32\2 b\2 c'16\2 ~ c'32\2 d'64\2 ees'\2 d'32\2 c'\2 a'8. \tuplet 3/2 { c'32\2 bes\3 a\3 } bes16\3 d'\2 g' a' a'8. g'16 |
  \mark "H" g'4 r bes\3 ~ bes32\3 c'\3 bes\3 a\3 bes16\3 g' |
  \appoggiatura bes8\3 a4\3 ~ a16\3 f\3 g\3 a\3 bes32\2( c'\2 bes\2 a\2 bes\2 c'\2 d'\2 ees'\2) f'16 ees'32 d' ees' a ees'16( |
  ees'8) d' r16 g a\2\turn bes \tuplet 3/2 { e16 d e } \tuplet 3/2 { f16 e f } \tuplet 3/2 { g16 a g } \tuplet 3/2 { f16 g a } |
  \tuplet 3/2 { bes16\2 a\2 bes\2 } g'8 ~ g'32 a bes c' bes a g16 a( c' f' d) c'\2( bes\2 e' f') |
  a8\2\prall g\3 r4 a ~ a32 g\3 bes\2 a\2 c'\2 bes\2 d'16\2 |
  d'\2 g8.\3 ~ g32\3 f\3 a\3 g\3 bes\2 a\2 c'16\2 c'\2 f\3 f' d'\2 \appoggiatura c'8\2 bes4\2 ~ |
  bes16\2 g' e' c' c'( bes\trill a bes) \appoggiatura bes a bes32 c' bes a g fis fis8( ees32\prall) d ees16 |
  d( e) \appoggiatura f e( f) f4 ~ f32 g f e f bes g16 e8.\trill f16 |
  f a d'32 b c'16 c'8. c'16 ~ c' d'32 e' f' d' a16 bes ~ bes32 a g a bes16 |
  \appoggiatura f e f32 g g\prall f g16 a8 ~ a64 g f e f a cis32 d16 d'16. e'64 d' cis'32 d' a8( g16.) a32 |
  a1\fermata |
  \bar "|."
}

\include "../muziko.ly"
