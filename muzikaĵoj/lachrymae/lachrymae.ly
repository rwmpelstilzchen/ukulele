\include "../ĉiea.ly"

#(set-global-staff-size 26)

\header {
  titolo-xx     = "Pavan Lachrymae"
  titolo-he     = "זִלגו דמעותי"
  titolo-eo     = "La pavano de la larmoj"
  komponisto-xx = ""
  komponisto-he = "ג׳ון דוולנד"
  komponisto-eo = "John Dowland"
  ikono         = "😢"
}

\include "../titolo.ly"

flow = {
  \mark "V. 1"
  \time 8/4
  \key g \minor
  \repeat volta 2 {
	g4.\4 f16 ees d4 bes2 a8 g\4 fis2 |
	g4\4 d4. d8 f f
	ees4 c d4.\fermata^"(1-a)" a8 |
	bes4 g\4 a fis
	g8\4 bes8. a16 g8\4 fis4 bes |
	a8 g\4 g4.\4 fis16( e fis4)
	g1\4 |
  }
  \break

  \repeat volta 2 {
	bes4. a8 g\4 f bes4. a16 g\4 a4 bes f |
	r8 g4\4 fis8 g\4 ees d4
	r8 c ees4 r8 ees g4\4 |
	r8 g\4 bes4 r8 f a8. bes16
	c'4 r8 c ees8. f16 g4\4 |
	r8 bes4 a8 bes8. a16 g\2( fis g8\2)
	fis1 |
  }
  \break

  \repeat volta 2 {
	fis4. g8\4 a4 bes
	a8 g\4 g2\4 fis4 |
	g4.\4 bes8 a bes g4\4
	fis2 r2 |
	d'4. a8 c'4.\fermata^"(1-a)" g8\4
	bes4 a g\4 fis |
	bes a8 g\4 g4.\4 fis16( e
	fis4) g2.\4 |
  }
}

vaneyck = {
  \mark "V. 2"
  %\time 8/4
  %\key g \minor

  \repeat volta 2 {
    g2.\4 f8 ees d2 bes ~ |
    bes4 a g2\4 fis2. fis4 |
    g2.\4 g4\4 d4. ees8 f4 f |
    ees2 c d2. a4 |
    \break
	bes4. a8 g4\4 g\4 a4. g8\4 fis4 fis |
    g8\4 a bes4 a g\4 fis2 bes |
    a4 g\4 g2.\4 fis8( e fis2) |
    g\breve\4 |
  }
  \break

  \repeat volta 2 {
    bes2. a4 g\4 f bes2 ~ |
    bes4 a8 g\4 a2 bes2. f4 |
    f g2\4 fis4 g\4 ees d2 |
    c4 c8 d ees2. ees8 f g2\4 ~ |
    \break
	g4\4 g8\4 a bes2. f8 g\4 a4. bes8 |
    c'2. c8 d ees4. f8 g4.\4 a8 |
    bes2. a4 g\4 bes4. a8 g4\4 |
    fis\breve |
  }
  \break
  \repeat volta 2 {
    \partial 1 fis2. g4\4 |
    a2 bes a4 g\4 g2\4 ~ |
    g4\4 fis8 e fis2 g2.\4 bes4 |
    a bes g2\4 fis a ~ |
	\partial 1 a4 fis8 g\4 a4 \transpose a, a {a,} |
    \break
	d2 d'2. a8 bes c'2 ~ |
    c'4 g8\4 a bes2 a4 bes g\4 a |
    fis2 bes a4 g\4 g2\4 ~ |
    g4\4 fis8( e fis2) g\4 |
  } 
}

melodio = {
  \flow
  \pageBreak
  \vaneyck
}

\include "../muziko.ly"
