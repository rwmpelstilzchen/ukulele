\include "../ĉiea.ly"

#(set-global-staff-size 27)

\header {
  titolo-xx     = "Für Elise"
  titolo-he     = "לאֶלִיזֶה"
  titolo-eo     = "Por Elizo"
  komponisto-xx = ""
  komponisto-he = "לודוויג ון בטהובן"
  komponisto-eo = "Ludwig van Beethoven"
  ikono         = "𝆏"
}

\include "../titolo.ly"

melodio = {
  \clef "treble"
  \key a \minor
  \time 3/8
  \tempo 8 = 144
  \partial 8 e'16-4( dis'-3 |
  \repeat volta 2 {
	e' dis' e' b\2 d' c' |
	a8) r16 c( e a |
	b8) r16 e( gis\4 b |
	c'8) r16 e( e' dis' |
	\break
	e' dis' e' b\2 d' c' |
	a8) r16 c( e a |
	b8) r16 e( c' b\4 |
  }
  \alternative {
	{ a4) e'16 dis'| }
	{ a8 r16 b\2-3( c' d' | }
  }
  \break
  \repeat volta 2 {
	e'8.) g16\4( f' e' |
	d'8.) f16\3( e' d' |
	c'8.) e16( d' c' |
	b8) e16 e( e') e( |
	\break
	e') e' \transpose e'' e' {e''} dis'( e') dis'( |
	e') dis'( e') dis'( e' dis' |
	\break
	e' dis' e' b\2 d' c' |
	a8) r16 c( e a |
	b8) r16 e( gis\4 b |
	c'8) r16 e( e' dis' |
	\break
	e' dis' e' b\2 d' c' |
	a8) r16 c( e a |
	b8) r16 e( c' b\4 |
  }
  \alternative {
	{ a8) r16 b\2 c' d' | }
	{ a8 r16 c'(-. c'-. c')-. | }
  }
  \break
  %\set TabStaff.minimumFret = #8
  f_"dolce"( a c'8\3) f'16.\2(-> e'32\2) |
  e'8\2( d'\2) bes'16.(-> a'32) |
  a'16( g' f' e'\2 d'\2 c'\2 |
  bes8\3 a\3) bes64\3( a\3)( g32\4 a\3 bes\3 |
  c'4\3) d'16\2( dis'\2 |
  e'8.\2) e'16\2( f'\2 a) |
  \tuplet 3/2 { d'16\3(\turn c'\3 b\3 } c'8\3 d'16.\3 b32\3 |
  c'\2) g'( g\4 g' a\3 g' b\3 g' c'\3 g' d'\2 g' |
  e'\2) g'( c'' b' a' g' f' e'\2 d'\2 g' f' d'\2 |
  %c') g'( g g' a g' b g' c' g' d' g') |
  c'\2) g'( g\4 g' a\3 g' b\3 g' c'\3 g' d'\2 g') |
  %e'( g' c'' b' a' g' f' e' d' g' f' d' |
  e'32\2( g' c'' b' a' g' f' e'\2 d'\2 g' f' d'\2 |
  e'\2 f'\2 e'\2 dis'\2 e'\2 b\3 e'\2 dis'\2 e'\2 b\3 e'\2 dis'\2 |
  e'8.) b16\2( e' dis' |
  e'8.) b16\2( e') dis'( |
  e') dis'( e') dis'( e') dis'( |
  \break
  e' dis' e' b\2 d' c' |
  a8) r16 c( e a |
  b8) r16 e( gis\4 b |
  c'8) r16 e( e' dis' |
  \break
  e' dis' e' b\2 d' c' |
  a8) r16 c( e a |
  b8) r16 e( c' b\4 |
  a8) r16 b\2-3( c' d' |
  \break
  e'8.) g16\4( f' e' |
  d'8.) f16\3( e' d' |
  c'8.) e16( d' c' |
  b8) e16 e( e') e( |
  \break
  e') e' \transpose e'' e' {e''} dis'( e') dis'( |
  e') dis'( e') dis'( e' dis' |
  \break
  e' dis' e' b\2 d' c' |
  a8) r16 c( e a |
  b8) r16 e( gis\4 b |
  c'8) r16 e( e' dis' |
  \break
  e' dis' e' b\2 d' c' |
  a8) r16 c( e a |
  b8) r16 e( c' b |
  a)-. a-. a-. a-. a-. a-. |
  \break
  cis'4.\2-3(-> |
  d'4\2)-> e'16( f') |
  f'4-> f'8( |
  e'4.) |
  %\break
  d'4\2 c'16\2( b\2 |
  a4\2) a8\2 |
  a\2( c'\2 b\2 |
  a4.\2) |
  \break
  cis'\2( |
  d'4\2) e'16( f') |
  f'4 f'8 |
  f'4. |
  %\break
  ees'4( d'16\2 c'\2 |
  bes4\2) a8\3( |
  gis4\3) gis8\3( |
  a4\3) r8 |
  b\3 r r |
  %\tuplet 3/2 { a16( c' e' } \tuplet 3/2 { a16 c' e' } \tuplet 3/2 { d'16 c' b) } |
  %\tuplet 3/2 { a16( c' e' } \tuplet 3/2 { a'16 c'' e'' } \tuplet 3/2 { d''16 c'' b') } |
  %\tuplet 3/2 { a'16( c'' e'' } \tuplet 3/2 { a''16 c''' e''' } \tuplet 3/2 { d'''16 c''' b'') } |
  %\tuplet 3/2 { bes''16( a'' gis'' } \tuplet 3/2 { g''16 fis'' f'' } \tuplet 3/2 { e''16 dis'' d'' } |
  %\tuplet 3/2 { cis''16 c'' b' } \tuplet 3/2 { bes'16 a' gis' } \tuplet 3/2 { g'16 fis' f') } |
  \break
  \tuplet 3/2 { a16\3( c'\2 e' } \tuplet 3/2 { a16\3 c'\2 e' } \tuplet 3/2 { d'16\2 c'\2 b\2) } |
  \transpose a'' a' {
	\tuplet 3/2 { a'16( c' e' } \tuplet 3/2 { a'16\3 c''\2 e'' } \tuplet 3/2 { d''16\2 c''\2 b'\2) } |
	\transpose a'' a' {
	  \tuplet 3/2 { a''16( c'' e'' } \tuplet 3/2 { a''16\3 c'''\2 e''' } \tuplet 3/2 { d'''16\2 c'''\2 b''\2) } |
	}
	\tuplet 3/2 { bes''16( a'' gis'' } \tuplet 3/2 { g''16 fis'' f'' } \tuplet 3/2 { e''16 dis'' d'' } |
  }
  \tuplet 3/2 { cis''16 c'' b' } \tuplet 3/2 { bes'16 a' gis' } \tuplet 3/2 { g'16 fis' f') } |
  \break
  e'16^"rall. 60" ( \tempo 4=60 dis' e' b\2 d' c' |
  a8^"a tempo 72" ) \tempo 4=72 r16 c( e a |
  b8) r16 e( gis b\4 |
  c'8) r16 e( e' dis' |
  \break
  e' dis' e' b\2 d' c' |
  a8) r16 c( e a |
  b8) r16 e( c' b\4 |
  a8) r16 b\2-3( c' d' |
  \break
  e'8.) g16\4( f' e' |
  d'8.) f16\3( e' d' |
  c'8.) e16( d' c' |
  b8) e16 e( e') e( |
  \break
  e') e' \transpose e'' e' {e''} dis'( e') dis'( |
  e') dis'( e') dis'( e' dis' |
  \break
  e' dis' e' b\2 d' c' |
  a8) r16 c( e a |
  b8) r16 e( gis\4 b |
  c'8) r16 e( e' dis' |
  \break
  e' dis' e' b\2 d' c' |
  a8) r16 c( e a |
  b8_"morendo"^"rit. 52") \tempo 4=52 r16 e( c' b\4 |
  a4)-- \bar "|."
}

xmlmelodio = { \relative e' {
    \repeat volta 2 {
        \repeat volta 2 {
            \clef "treble" \key c \major \time 3/8 \partial 8 e16  (  dis16  | % 1
            e16  dis16 e16 b16 d16 c16  | % 2
            a8 ) r16 c,16 (  e16 a16  | % 3
            b8 ) r16 e,16 (  gis16 b16  | % 4
            c8 ) r16 e,16 (  e'16 dis16  | % 5
            e16  dis16 e16 b16 d16 c16   | % 6
            a8 ) r16 c,16 (  e16 a16  | % 7
            b8 ) r16 e,16 (  c'16 b16  }
        \alternative { {
                | % 8
                a4 ) }
            } s8 }
    \alternative { {
            | % 9
            a8 r16 b16 (  c16 d16  }
        } \repeat volta 2 {
        | 
        e8. ) g,16 (  f'16 e16  | % 11
        d8. ) f,16 (  e'16 d16  | % 12
        c8. ) e,16 (  d'16 c16  | % 13
        b8 ) e,16 e16 (  e'16 )  e,16 (  | % 14
        e'16 ) e16 (  e'16 )  dis,16 (  e16 )  dis16 ( | % 15
        e16 ) dis16 (  e16 )  dis16 (  e16 dis16  | % 16
        e16  dis16 e16 b16 d16 c16  | % 17
        a8 ) r16 c,16 (  e16 a16  | % 18
        b8 ) r16 e,16 (  gis16 b16  | % 19
        c8 ) r16 e,16 (  e'16 dis16   | 
        e16  dis16 e16 b16 d16 c16  | % 21
        a8 ) r16 c,16 (  e16 a16  | % 22
        b8 ) r16 e,16 (  c'16 b16  }
    \alternative { {
            | % 23
            a8 ) r16 b16  c16 d16  }
        {
            | % 24
            a8 r16 c16 ( -.  c16 -. c16 ) -.  }
        } | % 25
    f,16 _"dolce, sweetly" ^"" (  a16 c8 )  f16. ( ->  e32 )  | % 26
    e8 (  d8 )  bes'16. ( ->  a32 )   | % 27
    a16 (  g16 f16 e16 d16 c16  | % 28
    bes8  a8 )  bes64 (  a64 ) ( g32 a32 bes32  | % 29
    c4 ) d16 (  dis16  | 
    e8. ) e16 (  f16 a,16 )  | % 31
    \times 2/3  {
        d16 ( \turn  c16 b16 }
    c8  d16.  b32  | % 32
    c32 )  g'32 ( g,32 g'32  a,32  g'32 b,32 g'32  c,32  g'32 d32 g32
      | % 33
    e32 )  g32 ( c32 b32  a32  g32 f32 e32  d32  g32 f32 d32  | % 34
    c32 )  g'32 ( g,32 g'32  a,32  g'32 b,32 g'32  c,32  g'32 d32 g32
    )  | % 35
    e32 (  g32 c32 b32  a32  g32 f32 e32  d32  g32 f32 d32  
    | % 36
    e32  f32 e32 dis32  e32  b32 e32 dis32  e32  b32 e32 dis32  | % 37
    e8. ) b16 (  e16 dis16  | % 38
    e8. ) b16 (  e16 )  dis16 ( | % 39
    e16 ) dis16 (  e16 )  dis16 (  e16 )  dis16 ( | 
    #40
    e16  dis16 e16 b16 d16 c16   | % 41
    a8 ) r16 c,16 (  e16 a16  | % 42
    b8 ) r16 e,16 (  gis16 b16  | % 43
    c8 ) r16 e,16 (  e'16 dis16  | % 44
    e16  dis16 e16 b16 d16 c16  | % 45
    a8 ) r16 c,16 (  e16 a16  | % 46
    b8 ) r16 e,16 (  c'16 b16  | % 47
    a8 ) r16 b16 (  c16 d16  | % 48
    e8. ) g,16 (  f'16 e16  | % 49
    d8. ) f,16 (  e'16 d16  | 
    c8. ) e,16 (  d'16 c16  | % 51
    b8 ) e,16 e16 (  e'16 )  e,16 ( | % 52
    e'16 ) e16 (  e'16 )  dis,16 (  e16 )  dis16 ( | % 53
    e16 ) dis16 (  e16 )  dis16 (  e16 dis16  | % 54
    e16  dis16 e16 b16 d16 c16   | % 55
    a8 ) r16 c,16 (  e16 a16  | % 56
    b8 ) r16 e,16 (  gis16 b16  | % 57
    c8 ) r16 e,16 (  e'16 dis16  | % 58
    e16  dis16 e16 b16 d16 c16  | % 59
    a8 ) r16 c,16 (  e16 a16  | 
    b8 ) r16 e,16 (  c'16 b16  | % 61
    a16 ) -.  a16 -. a16 -. a16 -. a16 -. a16 -.   | % 62
    cis4. ( -> | % 63
    d4 ) -> e16 (  f16 )  | % 64
    f4 -> f8 ( | % 65
    e4. ) | % 66
    d4 c16 (  b16  | % 67
    a4 ) a8 | % 68
    a8 (  c8 b8  | % 69
    a4. ) | 
    cis4. ( | % 71
    d4 ) e16 (  f16 )  | % 72
    f4 f8 | % 73
    f4. | % 74
    es4 ( d16  c16  | % 75
    bes4 ) a8 (  | % 76
    gis4 ) gis8 ( | % 77
    a4 ) r8 | % 78
    b8 r8 r8 | % 79
    \times 2/3  {
        a16 (  c16 e16  }
    \times 2/3  {
        a,16  c16 e16  }
    \times 2/3  {
        d16  c16 b16 )  }
    | 
    \times 2/3  {
        a16 (  c16 e16  }
    \times 2/3  {
        a16  c16 e16  }
    \times 2/3  {
        d16  c16 b16 )  }
    | % 81
    \times 2/3  {
        a16 (  c16 e16  }
    \times 2/3  {
        a16  c16 e16  }
    \times 2/3  {
        d16  c16 b16 )  }
     | % 82
    \times 2/3  {
        bes16 (  a16 gis16  }
    \times 2/3  {
        g16  fis16 f16  }
    \times 2/3  {
        e16  dis16 d16  }
    | % 83
    \times 2/3  {
        cis16  c16 b16  }
    \times 2/3  {
        bes16  a16 gis16  }
    \times 2/3  {
        g16  fis16 f16 )  }
    | % 84
    e16 ^"rall. 60" (  dis16 e16 b16 d16 c16  | % 85
    a8 ^"a tempo 72" ) r16 c,16 (  e16 a16   | % 86
    b8 ) r16 e,16 (  gis16 b16  | % 87
    c8 ) r16 e,16 (  e'16 dis16  | % 88
    e16  dis16 e16 b16 d16 c16  | % 89
    a8 ) r16 c,16 (  e16 a16  | 
    b8 ) r16 e,16 (  c'16 b16  | % 91
    a8 ) r16 b16 (  c16 d16  | % 92
    e8. ) g,16 (  f'16 e16   | % 93
    d8. ) f,16 (  e'16 d16  | % 94
    c8. ) e,16 (  d'16 c16  | % 95
    b8 ) e,16 e16 (  e'16 )  e,16 ( | % 96
    e'16 ) e16 (  e'16 )  dis,16 (  e16 )  dis16 ( | % 97
    e16 ) dis16 (  e16 )  dis16 (  e16 dis16  | % 98
    e16  dis16 e16 b16 d16 c16  | % 99
    a8 ) r16 c,16 (  e16 a16   | 
    b8 ) r16 e,16 (  gis16 b16  | % 101
    c8 ) r16 e,16 (  e'16 dis16  | % 102
    e16  dis16 e16 b16 d16 c16  | % 103
    a8 ) r16 c,16 (  e16 a16  | % 104
    b8 _"morendo, dying away" ^"rit. 52" ) r16 e,16 (  c'16 b16  | % 105
    a4 ) -- \bar "|."
    }
}

\include "../muziko.ly"
