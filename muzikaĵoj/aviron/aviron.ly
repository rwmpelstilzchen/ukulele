\include "../ĉiea.ly"

\header {
  titolo-xx     = ""
  titolo-he     = "אוירון"
  titolo-eo     = "Aviadilo"
  komponisto-xx = ""
  komponisto-he = "פניה שלונסקי־וגמן"
  komponisto-eo = "Fanja Ŝlonski-Vegman"
  ikono         = "🛩"
}

\include "../titolo.ly"

melodio = {
  \key d \major
  \time 2/4
  d8. e16 fis8 d |
  e8 cis d4 |
  fis8. g16 a8 a |
  g8 b a4 |
  \break
  \repeat unfold 2 {
	d'8 cis'16( b) a8 a |
	g8 g fis4 |
	g8 e a8. g16 |
  }
  \alternative {
	{ fis8 g a4 | }
	{ fis8 e d4 |}
  }
  \bar "|."
}

\include "../muziko.ly"
