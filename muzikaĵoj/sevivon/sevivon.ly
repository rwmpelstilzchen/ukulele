\include "../ĉiea.ly"

\header {
  titolo-xx     = ""
  titolo-he     = "סובב הסביבון"
  titolo-eo     = "La turbo turbas"
  komponisto-xx = ""
  komponisto-he = ""
  komponisto-eo = ""
  ikono         = "⟳"
}

\include "../titolo.ly"

melodio = {
  \key c \major
  c'4 a8 a a a bes a |
  g a f2.|
  c'4 a8 a a a bes a |
  g a f2. |
  \break
  c8 a g2. |
  c8 a g2. |
  g8 a b c'8 ~ c'2 |
  R1 |
  \break
  d'4 c'2 a4 |
  d'4 c'2 a4 |
  d'4 c' c' a |
  d' c'2. |
  \break
  c'4 a8 a a a bes a |
  g a f2.|
  c'4 a8 a a a bes a |
  g a f2. |
  \break
  c8 a g2. |
  c8 a g2. |
  g8 a b c'8 ~ c'2 ~ |
  c'1 |
  \bar "|."
}

\include "../muziko.ly"
