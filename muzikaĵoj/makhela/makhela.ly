\include "../ĉiea.ly"

#(set-global-staff-size 24)

\header {
  titolo-xx     = ""
  titolo-he     = "מקהלה עליזה"
  titolo-eo     = "La ĝoja ĥoro"
  komponisto-xx = ""
  komponisto-he = "נורית הירש"
  komponisto-eo = "Nurit Hirŝ"
  ikono         = "🎤"
}

\include "../titolo.ly"

melodio = {
  \time 4/4
  \key bes \major
  \partial 8*3 f8 g\4 a |
  \repeat volta 4 {
    bes8. bes16 bes8 bes bes4 r8 a |
    g8.\4 a16 g8\4 f g4\4 r8 f |
    g8.\4 a16 g8\4 f g\4 g\4 a bes |
    c'8. d'16 c'8 d' c'4 r8 g\4 |
    c'8. c'16 c'8 d' ees' d' c' ees' |
    d' c' bes a bes4 r8 a |
    g\4 a bes8. a16 g8\4 a bes8. a16 |
    g8\4 a bes4 r8 b c'4 |
    c'16 c'8 c'16 c'8 c' c'4 f |
    g\4 r r d' |
    bes r r8 a g\4 f |
    g4\4 g8\4 c' r bes a g\4 |
    a8. bes16 a8 g\4 a g\4 f d |
    ees r r ees ees f g\4 a |
    bes4 c'8 d' ~ d'4 r |
    ees' r d'8. c'16 bes8 a |
  }
  \alternative {
	{ bes4 r r8 f g\4 a }
	{ bes4 r r2 | }
  }
  ees'4 r d'8. c'16 bes8 a |
  bes4 r r2 |
  \bar "|."
}

\include "../muziko.ly"
