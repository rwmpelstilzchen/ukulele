\include "../ĉiea.ly"

\header {
  titolo-xx     = ""
  titolo-he     = "שיר לשלום"
  titolo-eo     = "Kanto por la paco"
  komponisto-xx = ""
  komponisto-he = "יאיר רוזנבלום"
  komponisto-eo = "Jair Rozenblum"
  ikono         = "🕊"
}

\include "../titolo.ly"

melodio = {
  \key d \minor
  \time 4/4
  \tempo 4 = 120
  \repeat volta 2 {
    \mark "A+B"
    d4 a4 a4. a8           | 
    a4 g8\4 g4.\4 r8 g8\4  | 
    c'4 bes4 a8 g4\4 a8~   | 
    a2. r4                 | 
    f4 f4 f4. a8           | 
    a4 g8\4 g4.\4 r8 f8    | 
  }
  \alternative {
    {
      f4 e4 g8\4 a4 f8~    | 
      f2. r4               | 
    }
    {
      f4 e4 g8\4 e4 d8~    | 
      d2. r4               | 
    }
  }
  \break
  \mark "C"
  c4 bes4 bes4. c'8        | 
  bes4 a8 a4. r8 c8        | 
  c4 bes4 bes8 c'4 a8~     | 
  a4. bes8 a8 g8\4 f8 e8   | 
  d4 a4 a4. a8             | 
  a4 g8\4 g4.\4 r8 f8      | 
  f4 e4 g8\4 e4 d8~        | 
  d2 r8 \mark"D" d8 f8 a8  | 
  d'2 d'2                  | 
  d'8 c'16 bes16 c'2 r8 a8 | 
  bes4 a4 g8\4 d'4 a8~     | 
  a2 r8 d8 f8 a8           | 
  d'2 d'2                  | 
  d'8 c'16 bes16 c'2 r8 a8 | 
  bes4 a4 g8\4 f4 e8~      | 
  e2 a2^"D.C."             | 
  d1                       |
  \bar "|."
}

\include "../muziko.ly"
