\include "../ĉiea.ly"

\header {
  titolo-xx     = "Μισιρλού"
  titolo-he     = "Egiptino"
  titolo-eo     = "בת־מצרים"
  komponisto-xx = ""
  komponisto-he = ""
  komponisto-eo = ""
  ikono         = "𓁐"
}

\include "../titolo.ly"

melodio = {
  \time 4/4
  \key c \hijazkar
  \repeat volta 2 {
	c4. des8 e4 f |
	g4.\4 aes8\4 b4 aes8\4 g\4 |
	g1\4 ~ |
	g\4 |
	\break
	c4. des8 e4 f |
	g4.\4 aes8\4 b4 aes8\4 g\4 |
	g1\4 ~ |
	g\4 |
	\break
	aes8\4 g4\4 aes8\4 g4\4 f |
	g8\4 f4 g8\4 f4 e |
	e1 ~ |
	e |
	\break
	g8\4 f4 g8\4 f4 e |
	e8 des4 e8 des4 c8 c |
	c1 ~ |
	c
  } 
  \pageBreak
  \repeat volta 2 {
    f ~ |
    f2. e8 f |
    g1\4 ~ |
    g2.\4 f8 g\4 |
	\break
    aes2.\4 g8\4 aes\4 |
    b2. aes8\4 b |
    c'1 ~ |
    c' |
	\break
    des'8 c'4 des'8 c'4 bes |
    c'8 bes4 c'8 bes4 aes\4 |
    g1\4 ~ |
    g\4 |
	\break
    bes8 aes4\4 bes8 aes4\4 g\4 |
    g8\4 f4 g8\4 e4 des |
    c1 ~ |
    c
  }
  \break
  aes2.\4 g8\4 aes\4 |
  b2. aes8\4 b |
  c'1 ~ |
  c' |
  \bar "|."
}

\include "../muziko.ly"
