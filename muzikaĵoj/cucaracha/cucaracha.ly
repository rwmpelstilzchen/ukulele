\include "../ĉiea.ly"

\header {
  titolo-xx     = "La cucaracha"
  titolo-he     = "לה קוקרצ׳ה"
  titolo-eo     = "La lerta blato"
  komponisto-xx = ""
  komponisto-he = ""
  komponisto-eo = ""
  ikono         = "🐞"
}

\include "../titolo.ly"

melodio = {
   \key f \major
   \time 4/4
   \partial 8*3 { <c\3>8 <c\3>8 <c\3>8 | }
   \repeat volta 2 {
      <f\2>4. <a\1>4 <c\3>8 <c\3>8 <c\3>8 |
      <f\2>4. <a\1>4 <f\2>8 <f\2>8 <f\2>8 |
      <f\2>4 <f\2>8 <f\2>8 <e\2>8 <e\2>8 <d\3>8 <d\3>8 |
      <c\3>2 r8 <c\3>8 <c\3>8 <c\3>8 |
      <e\2>4. <g\4>4 <c\3>8 <c\3>8 <c\3>8 |
      <e\2>4. <g\4>4 <c'\1>8 <c'\1>8 <c'\1>8 |
      <c'\1>4 <c'\1>8 <d'\1>8 <c'\1>8 <bes\1>8 <a\1>8 <g\4>8 |
   }
   \alternative {
	 { <a\1>4. <f\2>4 <c\3>8 <c\3>8 <c\3>8 | }
	 { <f\2>1 | }
   }
   \break
   r4 <c\3>8 <c\3>8 <f\2>8 <f\2>8 <a\1>8 <a\1>8 |
   <c'\1>4. <a\1>2 r8 |
   r8 <c'\1>8 <c'\1>8 <d'\1>8 <c'\1>8 <bes\1>8 <a\1>8 <c'\1>8 |
   <bes\1>4 <g\4>2. |
   r8 <c\3>4 <c\3>8 <e\2>8 <e\2>8 <g\4>8 <g\4>8 |
   <bes\1>4. <g\4>2 r8 |
   r8 <c'\1>4 <d'\1>8 <c'\1>8 <bes\1>8 <a\1>8 <g\4>8 |
   <a\1>4. <f\2>2 r8 |
   \bar "|."
}

\include "../muziko.ly"
