\include "../ĉiea.ly"

\header {
  titolo-xx     = ""%\markup{\override #'(font-name . "Baekmuk Batang"){아리랑}}
  titolo-he     = "ארירנג"
  titolo-eo     = "Arirang"
  komponisto-xx = ""
  komponisto-he = ""
  komponisto-eo = ""
  ikono         = "☯"
}

\include "../titolo.ly"

melodio = {
  \time 3/4
  \key f \major
  c4. d8( c d)    | 
  f4. g8( f g)    | 
  a4 g8\4 a f d   | 
  c4. d8( c d)    | 
  f4. g8( f g)    | 
  a g\4 f d( c d) | 
  f4. g8( f4)     | 
  f2.             | 
  \bar "||"
  \break
  c'2 c'4         | 
  c' a g\4        | 
  a g8\4 a f d    | 
  c4. d8( c d)    | 
  f4. g8( f g)    | 
  a g\4 f d( c d) | 
  f4. g8( f4)     | 
  f2.             | 
  \bar "|."
}

\include "../muziko.ly"
