\include "../ĉiea.ly"

\header {
  titolo-xx     = ""
  titolo-he     = "גמלן: פנקור"
  titolo-eo     = "Gamelan: Pangkur"
  komponisto-xx = ""
  komponisto-he = ""
  komponisto-eo = ""
  ikono         = "🔨"
}

\include "../titolo.ly"

melodio = {
  \tempo 8 = 80
  \time 8/8
  \key c \slendro
  \set Timing.beatStructure = #'(4 4)
  \partial 8 g8\4 |
  \repeat volta 2 {
	d c d a d c a g\4 |
	a g\4 d c f d c a |
	d f d c g\4 f d c |
	f d c a d c a g\4 ~ |
  }
  \break
  \repeat volta 2 {
	g\4\repeatTie d ~ d c ~ c d ~ d a ~ |
	a d ~ d c ~ c a ~ a g\4 |
	a a ~ a ~ a g\4 g\4 a c |
	d c g\4 d ~ d c ~ c a ~ |
	\break
	a ~ a ~ a d g\4 f d c |
	d c f d g\4 f d c |
	g\4 a d c f d c a ~ |
	a d ~ d c ~ c a ~ a g\4\laissezVibrer |
  }
}

\include "../muziko.ly"
