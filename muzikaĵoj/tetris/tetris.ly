\include "../ĉiea.ly"

\header {
  titolo-xx     = "Коробейники"
  titolo-he     = "קורובייניקי"
  titolo-eo     = "Korobjejniki"
  komponisto-xx = ""
  komponisto-he = ""
  komponisto-eo = ""
  ikono         = "┻"
}

\include "../titolo.ly"

melodio = {
  \time 4/4
  \tempo 4 = 180
  \key c \major
  \repeat volta 2 {
	a4 e8 f8 g4\4 f8 e8 |
	d4 d8 f8 a4 g8\4 f8 |
	e4. f8 g4\4 a4 |
	f4 d4 d2 |
	\break
	r8 g4\4 bes8 d'4 c'8 bes8 |
	a4. f8 a4 g8\4 f8 |
	e4 e8 f8 g4\4 a4 |
	f4 d4 d4 r4 |
  }
  \break

  \repeat volta 2 {
    a2 f2 |
    g2\4 e2 |
    f2 d2 |
    cis2 e4 r4 |
	\break
    a2 f2 |
    g2\4 e2 |
    f4 a4 d'2 |
    cis'2 r2_\markup{\italic{D.C.}} |
  }
}

\include "../muziko.ly"
