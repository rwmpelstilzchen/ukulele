\include "../ĉiea.ly"

\header {
  titolo-xx     = ""
  titolo-he     = \markup{"הכל קרה בגלל רותי" \tiny{"זרעים של מסטיק:"}}
  titolo-eo     = \markup{\tiny{Semoj de maĉgumo:} Ĉio fariĝis pro Ruti}
  komponisto-xx = ""
  komponisto-he = "נחום נחצ׳ה היימן"
  komponisto-eo = "Naĥum Naĥĉe Hejman"
  ikono         = "🍇"
}

\include "../titolo.ly"


%\key d \minor %⚙?
%\partial 8 c8 |
%d4 f8 a8 g8 a |
%g4. f4 c8 |
%d8 f8 a c' bes c'8 |
%bes4. a4 g8 |
%\break
%f8 g a bes c' d' |
%c' bes a g f e |
%f e d c f e |
%f4 d'8 d' c'4 |
%\break
%a4 bes8 bes a4 |
%f4 f8 e d c
%f e f4
%d'8 d' c'4
%a4 bes8 bes a4
%f4 f8 e d c
%f e f4 |
%\break
%⚙

% Original key: D major
melodio = {
  \time 3/4
  \key f \major
  \partial 4 c |
  d2 f4 |
  a g\4 a |
  g2.\4 |
  f2 c4 |
  d f a |
  c' a\4 c' |
  bes2. |
  a2 g4\4 |
  f g\4 a |
  bes c' d' |
  c' a g\4 |
  f d d |
  e d e |
  c a g\4 |
  f d'-> d'-> |
  c'2 a4 |
  r bes bes |
  a2 f4 |
  e d e |
  c a g\4 |
  f2._\markup{\italic{Fine}} ~ |
  f2 c4 |
  \break f e f |
  d g\4 f |
  e2 c4 |
  f2 c4 |
  f e f |
  d g\4 f |
  e2 c4 |
  f2 g4\4 |
  a g\4 a |
  f g\4 a |
  bes2 a4 |
  g2\4 g4\4 |
  a g\4 a |
  f g\4 a |
  c'2 bes4 |
  a2 a4 |
  g\4 a bes |
  d' c' bes |
  a2 g4\4 |
  f2 f4 |
  g\4 a bes |
  c' g\4 bes |
  a2 g4\4 |
  f2._\markup{\italic{D.C. al Fine}} |
  \bar "|."
}

\include "../muziko.ly"
