\include "../ĉiea.ly"

%#(set-global-staff-size 24)

\header {
  titolo-xx     = "Doen Daphne d’over schoone Maeght"
  titolo-he     = "דפנה"
  titolo-eo     = "Dafna"
  komponisto-xx = ""
  komponisto-he = ""
  komponisto-eo = ""
  ikono         = "✌"
}

\include "../titolo.ly"

melodio = {
  \time 3/4
  \key d \dorian % really dorian?

  \mark "M. 1"
  \partial 4 d4 |
  \repeat volta 2 {
	f2 g4 |
	a2 d'4-3 |
	cis'4. d'8 e'4 |
	d'2 a4 |
	%\break
	c'4 a f|
	g4 e c |
	d4 f e |
  }
  \alternative {
	{ d2 d4 | }
	{ d2. | }
  }
  \break

  \repeat volta 2 {
	f'2-4 f'4 |
	e'2 e'4 |
	d'2 d'4 |
	cis'2 a4 |
	%\break
	a4. bes8 a4 |
	g4. a8 f4 |
	f4. g8 e4 |
	f2. |
  }
  \break

  \repeat volta 2 {
	\set TabStaff.minimumFret = #5
	c'4-3 c' d' |
	c'4 a f |
	c'4. d'8-1 e' f' |
	g'4 e' c' |
	\set TabStaff.minimumFret = #0
	\break
	a4. g8 f4 |
	e4 d d'-3 |
	cis'4. d'8 e'4 |
	d'2 a8 b |
	\break
	c'4 a f |
	g4 e c |
	d8 e f g e4 |
	d2. |
  }
  \bar "|."

  \pageBreak

  \mark "M. 2"
  \repeat volta 2 {
	\partial 4 d8 e |
	f4 e8 d g e |
	a4 f8 d d' e' |
	cis'4 a8 f'-4 e' cis' |
	d'4 d a8 b |
	\break
	c'8. a16 bes8 g a f |
	g8. e16 f8 d e c |
	a8. g16 f8 d' e cis' |
	d'4 d2 |
  }
  \break

  f'4 f\3 f' |
  e'4. c'8\4 e'4 |
  d'4 d d' |
  cis'4. b8 a4 |
  \break
  a4. f8 a4 |
  g4. a8 f4 |
  f4 e4. c16 e |
  f2. |
  \bar "||"
  \break

  f'8 a bes4\2 a8 f\3 |
  e'8 g\4 a4 g8\4 e |
  d8 f g4 f8 d |
  d'8 e' cis'4 a8 e'|
  \break
  cis'8 a a4 f8 c |
  g8 e a g f d|
  f8 c' a f g c |
  f2. |
  \break

  \repeat volta 2 {
	\set TabStaff.minimumFret = #5
	c'4 c'8 a d'4 |
	c'8. a16\3 bes8 g a f |
	c'4 f'8 e' d'4 |
	g'8. e'16 f'8 d' e' c' |
	\break
	\set TabStaff.minimumFret = #0
	a8. f16 bes8 a g f |
	e8 d e cis' d' d |
	a8 f b g cis' a |
	d'4 d a8 b |
	\break
	c'8. a16 bes8 c' a f |
	g8. e16 f8 g e c |
	a8. g16 f8 d e cis' |
	d'2 d4 |
  }
}

\include "../muziko.ly"
