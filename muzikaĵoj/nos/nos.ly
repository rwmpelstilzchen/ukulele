\include "../ĉiea.ly"

\header {
  titolo-xx     = "Ar Hyd y Nos"
  titolo-he     = "לאורך הליל"
  titolo-eo     = "Tra la nokto"
  komponisto-xx = ""
  komponisto-he = ""
  komponisto-eo = ""
  ikono         = "🌃"
}

\include "../titolo.ly"

%⚙ לבדוק את התווים
melodio = {
   \key f \major
   \time 4/4
   \repeat volta 2 {
      f4. e8 d4 f4 | 
      g4. f8 e4 c  | 
      d2 e4. e8      | 
      f1               | 
   }
   bes4 a4 bes4 c'4 | 
   d'4. c'8 bes4 a4       | 
   bes4 a4 g4 f4           | 
   a4. g8 f4 e4             | 
   f4. e8 d4 f4           | 
   g4. f8 e4 c4             | 
   d2 e4. e8                | 
   f1                         | 
   \bar "|."
}

\include "../muziko.ly"
