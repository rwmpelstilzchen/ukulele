#(set-default-paper-size "a3" 'landscape)

\include "../ĉiea.ly"

melodio = {
  \key g \minor
  \time 4/4
  g2\4 bes |
  d' ees' |
  fis r4 d' ~ |
  d' des'2 c'4 ~ |
  c' b2 bes4 ~ |
  bes a aes g\4 |
  fis d g\4 c' |
  bes2 a |
  g\4 bes |
  d'8\2 c'\2 d'\2 g' d'\2 bes\3 a\3 bes\3 |
  \break
  c'\2 d'\2 e'\1 fis' g' bes\3 c'\2 d'\2 |
  ees'\2 a bes c' d' c' bes a |
  bes\2-1 c'\2 d'\2 ees'\2 f' ees'\2 d'\2 c'\2 |
  d'\2 ees'\2 f' g' aes' f' ees'\2 d'\2 |
  e'\2-2 fis'\2 g' a' bes' g' fis'\2 e'\2 |
  fis'\2 g'\2 a' bes' c'' a' d'\3 a' |
  g' a' bes' c'' bes' a' g' fis' |
  g'4\2 d'\3 bes\3 g\4^\fermata |
  \bar "|."
}


\score {
  <<
	\new Staff \with { \omit StringNumber }
	{ \repeat unfold 4 { s1 \break } }
	\new TabStaff \with {
	  stringTunings = \stringTuning <g' c' e' a'>
	  \override TabNoteHead #'font-name = #'"Century Schoolbook L Bold"
	}
	{ \repeat unfold 4 { s1 \break } }
  >>
  \layout {
    indent = 0\in
    \context {
      \Staff
      \remove "Time_signature_engraver"
      %\remove "Clef_engraver"
      %\remove "Bar_engraver"
    }
    \context {
      \Score
      \remove "Bar_number_engraver"
    }
  }
}

\include "../muziko.ly"
