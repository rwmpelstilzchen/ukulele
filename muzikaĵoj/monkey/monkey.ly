\include "../ĉiea.ly"

\header {
  titolo-xx     = ""
  titolo-he     = ""
  titolo-eo     = ""
  komponisto-xx = ""
  komponisto-he = ""
  komponisto-eo = ""
  ikono         = ""
}

\include "../titolo.ly"

melodio = {
 % r <>2  r <>4  r <>8 \times 2/3 {  <e''>16 b' g'}   <fis'>2  <fis'>16  <d'>8.  <d'>4 e'  <fis'>8  <fis'>16 d'  <d'>8 e' e' b

 % <b>4  r  r <>8 g, g, a,  <a,>16  <b,>8.  <b,>4  <b,>2  <b,>8  <d>2  <cis>4  <cis>8

 % <cis>2 cis d e  <fis>8 g g a  <a>4 b

 % <b>4  r  r <>2  r <>1  
  
  <e'>8  <e'>16 g' fis' e'  <d'>8  <e'>4  <e'>8 d'-.

  <d'>16 c' b d'  <c'-.>8 c'-.  <b>4  <b>8 e'-.  <e'>8.  <g'>16 fis' e'  <d'>8  <e'>4  <e'>8  r16 fis'  <g'-.>8 g'-.  <a'>4  <fis'>8.  <g'>16 fis' e' d' fis'

  <g'-.>8 g'-. fis' fis'  <e'>8.  <g'>16 fis' e' d' fis'  <g'-.>8 g'-. fis' fis'  <e'>8.  <g'>16 fis' e'  <d'>8 e'-. e'-.  <e'>4  <e'>8 e'  <d'>16 c' b d'

  <c'-.>8 c'-.  <b>4 b  r  r <>2  <b>8 d' e' g'-. g'  r <>4  <g'>8  <c''>16  <c'>4  <b'>16 a' c''  <b'-.>8  <d'>2  <g'-.>8 g' g'

  <c'>8 g'  <a' c'> d'-.  <d'>8.  <a'>16  <b' d'>  <a' d'>  <g' d'>  <a' d'>  <b' g'>8 g'  <g' e'>  <e' b>  <e' b>4  <fis>8 g  <a>4  <a>2.

  <g>4  <g>2  <g>8 fis  <g>2 c'  <d'>1  <e'>8  <e'>16 g' fis' e'  <d'>8  <e'>4  <e'>8 d'-.

  <d'>16 c' b d'  <c'-.>8 c'-.  <b>4  <b>8 e'-.  <e'>8.  <g'>16 fis' e'  <d'>8  <e'>4  <e'>8  r <>16 fis'  <g'-.>8 g'-.  <a'>4  <fis'>8.  <g'>16 fis' e' d' fis'

  <g'-.>8 g'-. fis' fis'  <e'>8.  <g'>16 fis' e' d' fis'  <g'-.>8 g'-. fis' fis'  <e'>8.  <g'>16 fis' e'  <d'>8 e'-. e'-.  <e'>4  <e'>8 d'  <c'>16 b  <c'>8

  <b>4  <b>8 e'-.  <e'>8.  <g'>16 fis' e'  <d'>8 e'-. e'  <e'>8.  <g'>16 a' g' fis' a' g' e'  <fis'-.>8  <e'>4  <e'>8  r <>2  r <>8
}

\include "../muziko.ly"
