\include "../ĉiea.ly"

\header {
  titolo-xx     = ""
  titolo-he     = "היורה"
  titolo-eo     = "La unua pluvo"
  komponisto-xx = ""
  komponisto-he = "יואל אנגל"
  komponisto-eo = "Joel Engel"
  ikono         = "☔"
}

\include "../titolo.ly"

melodio = {
  \time 4/4
  \key c \major
  c' g8\4 r a4 e8 r |
  c' c' g\4 g\4 a a e e |
  c' c' g\4 g\4 a a e e |
  f e d4 e8 f g4\4 |
  c'8 b a4 b8 c' d'4 |
  c'8 r b r a r g\4 r |
  c' c' b r a a g\4 r |
  c'4 d' e' r |
  d' g\4 c' r |
  \bar "|."
}

\include "../muziko.ly"
