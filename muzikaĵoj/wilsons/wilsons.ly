\include "../ĉiea.ly"

\header {
  titolo-xx = ""
  titolo-he = "וילסון׳ז ויילד"
  titolo-eo = "Wilson’s Wilde"
  komponisto-xx = ""
  komponisto-he = ""
  komponisto-eo = ""
  ikono = "❦"
}

\include "../titolo.ly"

melodio = {
  \key d \major
  \time 3/4
  \partial 4 a4 |
  d'2 a4 |
  fis4 d g |
  fis4. e8 d4 |
  e2 a4 |
  d'2 a4 |
  fis4 d g |
  e4. d8 e4 |
  d2 a4 |
  \break
  d'2 a4 |
  fis4 d g\2 |
  fis4 g8 fis e d |
  e2 a4 |
  d'2 a4 |
  fis4 d8 e fis g |
  e8 d cis d e cis |
  < d %{d,%} >2. |
  \bar "||"

  \break
  < fis >4. g8 a4 |
  < a >4. b8 < a >4 |
  < b >4. cis'8 d'4 |
  < cis' >2 a4 |
  < fis >8 e fis g a4 |
  < a >8 g a b < a >4 |
  < b >8 a b cis' d'4 |
  < cis' >2 a4 |
  \bar "||"
  \break

  < fis >4 a2 |
  < g >4 b2 |
  < fis >4 a2 |
  < e >4 g2\4 |
  < fis >4 a2 |
  < g >4 b2 |
  < fis >2 < %{e%} cis %{a,%} >4 |
  < d >2. |
  \bar "||"
  \break

  < fis >8 g a2 |
  < g >8 a b2 |
  < fis >8 g a2 |
  < e >8 fis g2 |
  < fis >8 g a2 |
  < g >8 a b2 |
  < fis >4 < e >8 d < cis >4 |
  d2.
  \bar "|."
}

\include "../muziko.ly"
