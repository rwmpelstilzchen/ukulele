\include "../ĉiea.ly"

\header {
  titolo-xx     = ""
  titolo-he     = "בת שישים"
  titolo-eo     = "Sesdek jara"
  komponisto-xx = ""
  komponisto-he = "קובי אושרת"
  komponisto-eo = "Kobi Oŝrat"
  ikono         = "⛰"
}

\include "../titolo.ly"

melodio = {
  \key g \minor
  \time 4/4
  \tempo 4 = 120
  r f8 f f f f f |
  f4 bes2 a8 bes |
  c'4 bes a g8\4 ees ~ |
  ees1 |
  r4 ees8 ees ees ees ees ees |
  ees4 c'2 bes8 a |
  g4\4 f8 f ~ f2 |
  r1 |
  r4 d8 d d d d d |
  d4 bes2 a8 bes |
  c'4 b c' d'8 c' ~ |
  c'2. g8\4 a |
  c'4 bes a g\4 |
  bes g\4 d g\4 |
  e2 fis |
  g\4 bes4 a8 bes ~ |
  bes4 g\4 bes a8 bes ~ |
  bes4 ees bes bes |
  a d g\4 a8 bes ~ |
  bes2. a8 bes |
  c'4 bes c' bes8 d' ~ |
  d'4 bes f d' |
  c'2 a |
  d'1 |
  \pageBreak
  r4 g\4 bes\2^"V" c'\2 |
  d' g2\4 bes8\2 c'\2 |
  d'4 g2\4 c'8\2 d' |
  ees'4 d' c'\2 d'8 ees' ~ |
  ees'2. c'8\2 d' |
  ees'4 d' ees' d'8 f' ~ |
  f'4 ees' d' c'8\2 ees' ~ |
  ees'4 d' d' c'8\2 d' ~ |
  d'4 g\4 bes\2 c'\2 |
  d' g2\4 bes8\2 c'\2 |
  d'4 g2\4 c'8\2 d' |
  ees'4 d' f' ees'8 ees' ~ |
  ees'2. c'8\2 d' |
  f'4 ees' d' c'8\2 ees' ~ |
  ees'4 d' bes\2 g8\4 d' ~ |
  d'4 c'\2 bes\2 a8 bes\2 ~ |
  bes2.\2 c'8\2 d' |
  f'4 ees' c'\2 a8 ees' ~ |
  ees'4 d' bes\2 g\4 |
  e g\4 fis a |
  g1\4 \bar "|."
 
}

melodiof = {
  \key f \minor
  \time 4/4
  \tempo 4 = 120
  r ees8 ees ees ees ees ees |
  ees4 aes2\4 g8\4 aes\4 |
  bes4 aes\4 g\4 f8 des ~ |
  des1 |
  r4 des8 des des des des des |
  des4 bes2 aes8\4 g\4 |
  f4 ees8 ees ~ ees2 |
  r1 |
  r4 c8 c c c c c |
  c4 aes2\4 g8\4 aes\4 |
  bes4 a bes c'8 bes ~ |
  bes2. f8 g\4 |
  bes4 aes\4 g\4 f |
  aes\4 f c f |
  d2 e |
  f aes4\4 g8\4 aes\4 ~ |
  aes4\4 f aes\4 g8\4 aes\4 ~ |
  aes4\4 des aes\4 aes\4 |
  g\4 c f g8\4 aes\4 ~ |
  aes2.\4 g8\4 aes\4 |
  bes4 aes\4 bes aes8\4 c' ~ |
  c'4 aes\4 ees c' |
  bes2 g\4 |
  c'1 |
  r4 f aes\4 bes |
  c' f2 aes8\4 bes |
  c'4 f2 bes8 c' |
  des'4 c' bes c'8 des' ~ |
  des'2. bes8 c' |
  des'4 c' des' c'8 ees' ~ |
  ees'4 des' c' bes8 des' ~ |
  des'4 c' c' bes8 c' ~ |
  c'4 f aes\4 bes |
  c' f2 aes8\4 bes |
  c'4 f2 bes8 c' |
  des'4 c' ees' des'8 des' ~ |
  des'2. bes8 c' |
  ees'4 des' c' bes8 des' ~ |
  des'4 c' aes\4 f8 c' ~ |
  c'4 bes aes\4 g8\4 aes\4 ~ |
  aes2.\4 bes8 c' |
  ees'4 des' bes g8\4 des' ~ |
  des'4 c' aes\4 f |
  d f e g\4 |
  f1 \bar "|."
}


melodioe = {
  \displayLilyMusic\transpose e g {
  \key e \minor
  \time 4/4
  \tempo 4 = 120
  r4 d8 d d d d d       | 
  d4 g2 fis8 g          | 
  a4 g fis e8 c~        | 
  c1                    | 
  r4 c8 c c c c c       | 
  c4 a2 g8 fis          | 
  e4 d8 d~ d2           | 
  r1                    | 
  r4 b,8 b, b, b, b, b, | 
  b,4  g2 fis8 g        | 
  a4 gis a b8 a~        | 
  a2. e8 fis            | 
  a4 g fis e            | 
  g4 e b, e             | 
  cis2 dis2             | 
  e2 g4 fis8 g~         | 
  g4 e g fis8 g~        | 
  g4 c g g              | 
  fis4 b, e fis8 g~     | 
  g2. fis8 g            | 
  a4 g a g8 b~          | 
  b4 g d b              | 
  a2 fis                | 
  b1                    | 
  r4 e g a              | 
  b4 e2 g8 a            | 
  b4 e2 a8 b            | 
  c'4 b a b8 c'~        | 
  c'2. a8 b             | 
  c'4 b c' b8 d'~       | 
  d'4 c' b a8 c'~       | 
  c'4 b b a8 b~         | 
  b4 e g a              | 
  b4 e2 g8 a            | 
  b4 e2 a8 b            | 
  c'4 b d' c'8 c'~      | 
  c'2. a8 b             | 
  d'4 c' b a8 c'~       | 
  c'4 b g e8 b~         | 
  b4 a g fis8 g~        | 
  g2. a8 b              | 
  d'4 c' a fis8 c'~     |
  c'4 b g e             | 
  cis4 e dis fis        | 
  e1 \bar "|."
}
}

\include "../muziko.ly"
