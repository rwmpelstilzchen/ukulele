\include "../ĉiea.ly"

\header {
  titolo-xx     = "残酷な天使のテーゼ"
  titolo-he     = "זנקוקו נה טנשי נו טזה"
  titolo-eo     = "Zankoku na tenŝi no tēze"
  komponisto-xx = "佐藤英敏"
  komponisto-he = "הידטושי סטו"
  komponisto-eo = "Hidetoŝi Satō"
  ikono         = "😇"
}

\include "../titolo.ly"

melodio = {
  \key d \minor
  \time 4/4
  \tempo "Andante" 4 = 80
  d f g8.\4 f g8\4 |
  g4\4 c'8 bes a16 g8\4 a8. r8 |
  a4 c' d'8. g\4 f8 |
  c'4 a8 c' c'4 d' ~ |
  d'2. r4 |
  \break
  \tempo "Allegro" 4 = 130
  d8 r f r g8.\4 f g8\4 |
  g\4 g\4 c' bes a16 g8\4 a8. r8 |
  a r c' r d'8. g\4 f8 |
  c' c' a c' c'8. d' r8 |
  \break
  \repeat volta 2 {
	r4 f8 c16 c4 r16 r8 f |
	f8. g\4 c8 c4 r8 c |
	a8. bes a8 g8.\4 f g8\4 |
	a8. bes a8 d4 r8 d16 e |
	f8. f e8 e4 r8 f16 g\4 |
	bes8. a g8\4 f4 r8 a |
  }
  \alternative {
	{
	  a8. g\4 fis8 g4\4 d |
	  d4. e8 e4 r |
	}
	{
	  a8. g\4 fis8 g8.\4 a bes8 |
	  a2. r8 d16 e |
	}
  }
  \break
  f8. f e8 f8. f e8 |
  g8.\4 g\4 f8 e8. d e8 |
  f8. f e8 g8.\4 e d8 |
  %g4\4 a bes c' |
  c1 |
  \break
  \allowPageTurn
  f8. f e8 f8. f e8 |
  g8.\4 g\4 f8 e8. f g8\4 |
  a8. bes a8 g8.\4 f g8\4 |
  a2 a8. b cis'8 |
  \break
  \repeat volta 2 {
	d8 r f r g8.\4 f g8\4 |
	g\4 g\4 c' bes a16 g8\4 a8. r8 |
	a r c' r d'8. g\4 f8 |
  }
  \alternative {
	{ g\4 g\4 e g\4 f16 e8 f16 r8 f16 g\4 | }
	{ c' c' a c' c'8. d'16 r4 | }
  }
  \bar "|."
}

\include "../muziko.ly"
