\include "../ĉiea.ly"

\header {
  titolo-xx     = ""
  titolo-he     = "סליחות"
  titolo-eo     = "Pardono"
  komponisto-xx = ""
  komponisto-he = "עודד לרר"
  komponisto-eo = "Oded Lerer"
  ikono         = "𓂀"
}

\include "../titolo.ly"

melodio = {
  \key d \minor
  \time 3/4
  \repeat volta 2 {
	f'2 e'8 d' |
	c'2\2 bes8\2 g\3 |
	a2 g8\4 ~ g\4 |
	f e d4. r8 |
	\break
	f' e' d'4 c'8\2 bes\2 |
	a4. r8 g a |
	bes4 a gis |
	a2 r4 |
	\break
	g\4 a bes |
	c' d'4. bes8 |
	a4 a g\4 |
	f e4. r8 |
	\break
	d e f4 g8\4 a |
	c'4 bes g\4 |
	a bes gis |
	a2. |
	\break
	g4\4 a bes |
	c' d'4. bes8 |
	a4 a g\4 |
	f e4. r8 |
	\break
	d e f4 g8\4 a |
	c'4 bes g\4 |
	a f e |
	d2. |
	r2 d'8 a |
	\break
	f'2 d'8 a |
	bes2 c'8 d' |
	e'4 d' c'8 bes ~ |
	bes a ~ a r bes c' |
	\break
	d'2 bes8 a |
	gis2 e8 d |
	cis4 d e |
	f2 d'8 a |
	\break
	f'4 d' a |
	bes2 c'8 d' |
	e'4 d' c' |
	bes a bes8\2-2 c'\2 |
	d'2 e'8 f' |
	\break
  }
  \alternative {
	{
	  g'2 e'8 d' |
	  cis'4\2 d' e' |
	  d'2. |
	  R |
	  r2 f'8 f' |
	  \break
	}
	{
	  g'2 e'8 d' |
	  cis'4\2 d' e' |
	  d'2. ~ |
	  d'4 d' e' |
	  d'2. ~ |
	  d' |
	}
  }
  \bar "|."
}

\include "../muziko.ly"
