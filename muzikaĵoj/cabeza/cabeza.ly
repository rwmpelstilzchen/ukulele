\include "../ĉiea.ly"

\header {
  titolo-xx     = "Por una cabeza"
  titolo-he     = "פור אונה קבזה"
  titolo-eo     = "Pro unu kapo"
  komponisto-xx = ""
  komponisto-he = "קרלוס גרדל"
  komponisto-eo = "Carlos Gardel"
  ikono         = "🐴"
}

\include "../titolo.ly"


melodio = {
  \tempo 4 = 60
  \key c \major
  \time 2/4
  \partial 16*4 e16 f fis g\4 |
  a g\4 r8 fis16 g\4 a b |
  d' c' r8 e'16-3 f' d' e' |
  c' d' b c' b8. g16\4 |
  f8. r16 f'-3 e' g' f' |
  \break
  d'8. r16 d' cis' e'-3 d' |
  b8. r16 b c' cis' d' |
  e' d' b g\4 gis b a f |
  g8.\4 r16 e f fis g\4 |
  \break
  a g\4 r8 fis16 g\4 a b 
  d' c' r8 c'16 d' e' c' |
  d' c' d' e' d'8. c'16 |
  a'8. r16 d' c' a f |
  \break
  e d e f aes c'8 b32 a |
  c'8 g16\4 r c' d' e' c' |
  d'8 d' b16 c' d' b |
  c'8. r16 r4
  \break
  \key c \minor
  \repeat volta 2 {
	r8 ees' \tuplet 3/2 { ees'8 f' g' } |
	g'4 d'\2 |
	r8 c'\2 \tuplet 3/2 { c'8\2 d'\2 ees'\2 } |
	ees'4\2 bes\3 |
	\break
	r8 aes\2-2 \tuplet 3/2 { aes8\2 bes\2 c'\2 } |
	c'4\2 c'16-1 d' ees' c' |
  }
  \alternative {
	{
	  d'8 d' c'16 d' ees' c' |
	  ees'4 d' |
	}
	{
	  d'8 d' b16 c' d' b |
	  c'2 |
	  \bar "|."
	}
  }
}

badscoremelodio = {
  \time 4/4
  \key a \major
  \partial 8*4 cis8 d dis e |
  \repeat volta 2 {
	fis4 e dis8 e fis gis |
	b4 a cis'8 d' b cis' |
	a8 b4 gis8 a gis4 e8 |
	d2 d'8 cis' e' d' |
	b2 b8 ais cis' b |
	gis2 gis8 a ais b |
	cis'8 b gis fis e gis e d |
  }
}

\include "../muziko.ly"
