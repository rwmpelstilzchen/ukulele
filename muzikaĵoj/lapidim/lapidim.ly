\include "../ĉiea.ly"

\header {
  titolo-xx     = ""
  titolo-he     = "אנו נושאים לפידים"
  titolo-eo     = "Ni portas torĉojn"
  komponisto-xx = ""
  komponisto-he = "מרדכי זעירא"
  komponisto-eo = "Mordeĥaj Zeira"
  ikono         = "💡"
}

\include "../titolo.ly"

melodio = {
  \time 4/4
  \key c \minor
  c d8. ees16 c4 d8. ees16 |
  g2.\4 a8 bes |
  a g4\4 f8 g4\4 aes |
  f g8.\4 aes16 f4 g\4 |
  c' d'8. ees'16 d'8 c' r ees' |
  d' c' bes4 g8\4 g\4 aes bes |
  g4\4 aes bes4. bes8 |
  c'4 ees8. f16 g4\4 c8. d16 |
  ees4 g\4 bes8 aes g\4 f |
  d'2. c'8. bes16 |
  c'2. r4 |
  \bar "||"
  \break

  ees2 d4 ees |
  d2 c4 c |
  f2 d4 f |
  aes c' b g8.\4 g16\4 |
  c'4 bes8. aes16 g8\4 c4 d8 |
  ees4 f8. g16\4 aes8 g4\4 g16\4 aes |
  bes4. aes16 bes c'4. bes16 c' |
  d'4. c'16 d' ees'4 c' |
  ees2 d4 ees |
  d2 c4 c |
  f2 d4 f |
  aes c' b g8.\4 g16\4 |
  c'4 \tuplet 3/2 { bes8 g\4 bes } c'4 \tuplet 3/2 { bes8 g\4 bes } |
  c'2. bes8. c'16 |
  \time 2/4
  d'2 |
  \time 4/4
  ees'2. r4 |
  \bar "|."
}

\include "../muziko.ly"
