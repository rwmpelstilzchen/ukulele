\include "../ĉiea.ly"

\header {
  titolo-xx     = ""
  titolo-he     = "לגדול איתך ביחד: פילי הקטן"
  titolo-eo     = "Grandiĝi kun vi: Pili la malgranda"
  komponisto-xx = ""
  komponisto-he = "יעל תלם"
  komponisto-eo = "Jael Telem"
  ikono         = "🐘"
}

\include "../titolo.ly"

melodio = {
  \override TupletBracket.bracket-visibility = ##t
  \clef "treble"
  \key f \major
  \tupletUp
  \time 4/4

  \partial 2 \tuplet 3/2 {r4  f8}\tuplet 3/2 {f8 f8 f8} |
  d'2 \tuplet 3/2 {c'4 d'8} \tuplet 3/2 {c'4 bes8} |
  \tuplet 3/2 {a4 c'8 ~} c'4 ~ \tuplet 3/2 {c'4 f8} \tuplet 3/2 {f4 f8} |
  \tuplet 3/2 {d'4 c'8} bes4 ~ \tuplet 3/2 { bes8 f f } \tuplet 3/2 { f8 e f } |
  g2\4 ~ \tuplet 3/2 {g4\4 d8} \tuplet 3/2 { g8\4 g\4 a } |
  \break

  bes4 \tuplet 3/2 {d'4 c'8~} c'4 ~ \tuplet 3/2 {c'4 c8} |
  \tuplet 3/2 {a4 f8~} f2 \tuplet 3/2 {bes4 a8} |
  \tuplet 3/2 {g4\4 a8} \tuplet 3/2 {g4\4 f8} \tuplet 3/2 {e4 c8} \tuplet 3/2 {a4 g8} |
  f2 r4 \tuplet 3/2 {f4 f8} |
  \break
  \bar "||"

  \tuplet 3/2 { bes8 bes bes } \tuplet 3/2 {bes4 bes8} \tuplet 3/2 { a8 a a } a4 |
  r4 \tuplet 3/2 {g4\4 a8} bes4 \tuplet 3/2 {g4\4 c'8~} |
  c'2 \tuplet 3/2 { bes8 bes c' } \tuplet 3/2 {bes4 c'8} |
  \tuplet 3/2 {a4 f8~} f2 \tuplet 3/2 {f4 f8} |
  \break

  \tuplet 3/2 {bes4 bes8} \tuplet 3/2 {bes4 bes8} \tuplet 3/2 {a4 a8} \tuplet 3/2 {a4 a8} |
  \tuplet 3/2 {g4\4 g8\4} \tuplet 3/2 {g4\4 a8} bes4~ \tuplet 3/2 {bes4 g8\4} |
  \tuplet 3/2 {c'4 c'8} c'4 bes \tuplet 3/2 {c'4 a8~} |
  \tuplet 3/2 {a4 c'8~} c'4 ~ \tuplet 3/2 {c'4 f8} \tuplet 3/2 { f8 f f } |
  \break
  \bar "||"

  d'2 \tuplet 3/2 {c'4 d'8} \tuplet 3/2 {c'4 bes8} |
  \tuplet 3/2 {a4 c'8 ~} c'4 ~ \tuplet 3/2 {c'4 f8} \tuplet 3/2 {f4 f8} |
  \tuplet 3/2 {d'4 c'8} bes4 ~ \tuplet 3/2 { bes8 f f } \tuplet 3/2 { f8 e f } |
  g2\4 ~ \tuplet 3/2 {g4\4 d8} \tuplet 3/2 { g8\4 g\4 a } |
  \break


  bes4 \tuplet 3/2 {d'4 c'8~} c'4 ~ \tuplet 3/2 {c'4 c8} |
  \tuplet 3/2 {a4 f8~} f2 \tuplet 3/2 {bes4 a8} |
  \tuplet 3/2 {g4\4 a8} \tuplet 3/2 {g4\4 f8} \tuplet 3/2 {e4 c8} \tuplet 3/2 {a4 g8} |
  f2 r4 \tuplet 3/2 {r4 f8} |
  \break
  \bar "||"

  \tuplet 3/2 {bes4 bes8} \tuplet 3/2 {bes4 bes8} \tuplet 3/2 {a4 a8} \tuplet 3/2 {a4 a8} |
  g4\4 \tuplet 3/2 4 {g4\4 a8 bes4 a8 g4\4 c'8 ~} |
  c'2. bes4 |
  \tuplet 3/2 {a4 f8~} f2 \tuplet 3/2 {f4 f8} |
  \break

  \tuplet 3/2 4 {bes4 bes8 bes4 bes8 a4 a8 a4 a8} |
  g4\4 \tuplet 3/2 4 {g4\4 a8 bes4 a8 g4\4 c'8 ~} |
  c'2. bes4 |
  \tuplet 3/2 {a4 c'8 ~} c'2 \tuplet 3/2 {f4 f8} |
  \break
  \bar "||"

  \tuplet 3/2 4 {d'8 d' d' d'8 d' d' c'4 c'8 c'4 a8} |
  r2. \tuplet 3/2 {r4 c8} |
  \tuplet 3/2 4 {c'4 c'8 c'4 c'8 bes4 c'8 bes4 a8 ~} |
  a2. \tuplet 3/2 {f4 f8} |
  \break

  \tuplet 3/2 4 { g8\4 g\4 g\4  g8\4 g\4 a bes4 a8 bes4 a8 ~} |
  \tuplet 3/2 {a4 f8 ~} f2 \tuplet 3/2 {c4 c8} |
  \tuplet 3/2 {bes4 a8} \tuplet 3/2 {bes4 g8\4 ~}  g4\4 ~ \tuplet 3/2 {g4\4 c8} |
  \tuplet 3/2 4 {bes4 a8 bes4 g8\4 ~} g4\4 \tuplet 3/2 {c4 c8} |
  \break

  \tuplet 3/2 4 {bes4 a8 bes4 g8\4 ~} g4\4 ~ \tuplet 3/2 {g4\4 c8} |
  \tuplet 3/2 4 {bes4 a8 bes4 g8\4 ~} g2\4 ~ |
  g4\4 r r2 |
  r2 \tuplet 3/2 4 {r4 f8 f8 f f} |
  \break
  \bar "||"

  d'2 \tuplet 3/2 4 {c'4 d'8 c'4 bes8} |
  \tuplet 3/2 {a4 c'8 ~} c'4 ~ \tuplet 3/2 {c'4 f8} \tuplet 3/2 {f4 f8} |
  \tuplet 3/2 {d'4 c'8} bes4 ~ \tuplet 3/2 { bes8 f f } \tuplet 3/2 { f8 e f } |
  g2\4 ~ \tuplet 3/2 4 {g4\4 d8 g8\4 g\4 a} |
  \break

  bes4 \tuplet 3/2 {d'4 c'8 ~} c'4 ~ \tuplet 3/2 {c'4 c8} |
  \tuplet 3/2 {a4 f8 ~} f2 \tuplet 3/2 {bes4 a8} |
  \tuplet 3/2 4 {g4\4 a8 g4\4 f8 e4 c8 a4 g8} |
  f2 r |
  \bar "|."
}

\include "../muziko.ly"
