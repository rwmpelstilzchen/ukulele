\include "../ĉiea.ly"

\header {
  titolo-xx     = "さくらさくら"
  titolo-he     = "סקורה סקורה"
  titolo-eo     = "Sakura Sakura"
  komponisto-xx = ""
  komponisto-he = ""
  komponisto-eo = ""
  ikono         = "🌸"
}

\include "../titolo.ly"

melodio = 
{
  \time 4/4
  \key c \phrygianminor % is it right?
  c' c' d'2 |
  c'4 c' d'2 |
  c'4 d' ees' d' |
  c' d'8 c' aes2\4 |
  \break
  g4\4 ees g\4 aes\4 |
  g\4 g8\4 ees d2 |
  c'4 d' ees' d' |
  c' d'8 c' aes2\4 |
  \break
  g4\4 ees g\4 aes\4 |
  g\4 g8\4 ees d2 |
  c'4 c' d'2 |
  c'4 c' d'2 |
  g4\4 aes\4 d'8 c' aes4\4 |
  g2\4 r |
  \bar "|."
}


melodiotg = {
   \tempo 4=120
   \key f \major
   \time 4/4
   \oneVoice
   <d'\1>4 <d'\1>4 <e'\1>2 
   <d'\1>4 <d'\1>4 <e'\1>2 
   <d'\1>4 <e'\1>4 <f'\1>4 <e'\1>4 
   <d'\1>4 <e'\1>8 <d'\1>8 <bes\1>2 
   <a\1>4 <f\2>4 <a\1>4 <bes\1>4 
   <a\1>4 <a\1>8 <f\2>8 <e\2>2 
   <d'\1>4 <e'\1>4 <f'\1>4 <e'\1>4 
   <d'\1>4 <e'\1>8 <d'\1>8 <bes\1>2 
   <a\1>4 <f\2>4 <a\1>4 <bes\1>4 
   <a\1>4 <a\1>8 <f\2>8 <e\2>2 
   <d'\1>4 <d'\1>4 <e'\1>2 
   <d'\1>4 <d'\1>4 <e'\1>2 
   <a\1>4 <bes\1>4 <e'\1>8 <d'\1>8 <bes\1>4 
   <a\1>1 
   \bar "|."
}

\include "../muziko.ly"
