\include "../ĉiea.ly"

\header {
  titolo-xx     = ""
  titolo-he     = "סקיארזולה מרזולה"
  titolo-eo     = "Schiarazula marazula"
  komponisto-xx = ""
  komponisto-he = ""
  komponisto-eo = ""
  ikono         = "👿"
}

\include "../titolo.ly"

melodio = {
  \time 2/2
  \key c \major
  \partial 4 a |
  \repeat unfold 2 {
	a g\4 a g\4 |
	f f f e |
	d c d e |
  }
  \alternative {
	{ d d d a | }
	{ d d d g8\4 f | }
  }
  \break
  \repeat unfold 2 {
	e4 d e f |
	e d e a8 g\4 |
	f4 d d cis |
  }
  \alternative {
	{ d d d g8\4 f | }
	{ d1 |}
  }
  \bar "|."
}

\include "../muziko.ly"
