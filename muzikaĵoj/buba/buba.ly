\include "../ĉiea.ly"

\header {
  titolo-xx     = ""
  titolo-he     = "בובה קטנה"
  titolo-eo     = "La pupeto"
  komponisto-xx = ""
  komponisto-he = "יודה רונן"
  komponisto-eo = "Júda Ronén"
  ikono         = "🎎"
}

\include "../titolo.ly"

melodio = {
%  \key fis \minor
%  \time 4/4
%  cis'8 b cis'4 fis4 fis |
%  d'8 cis' d' cis' b2 |
%  b8 a\1 b4 e e |
%  cis'8 b cis' b a2 |
%  a8\4 gis\4 a4\4 d d |
%  b8 a\1 b a\4 gis2\4 |
%  gis4\4 fis fis fis ~ |
%  fis8 f fis2. |
%  \bar "||"
%  %cis'8\4 b\2 cis'4\4 dis' e' ~|
%  %e'8 e'8 d'\4 cis'\4 b2\2 |
%  cis'8 b cis'4 dis' e' ~|
%  e'8 e'8 d' cis' b2 |
%  d'4 cis' b8 a b4 |
%  gis4\2 a4.\2 b8 cis' d' | 
%  e'4 d'4 cis' b8 a |
%  %b4 gis a2 |
%  %fis8 gis a4 fis gis |
%  %f4 fis2. |
%  b4 gis a4.\2 gis8 |
%  fis8 gis a4\2 gis4 fis8 f | 
%  fis4 gis4 fis2 |
%  \bar "|."
  \key e \minor
  \time 4/4
  b8( a b4) e e |
  c'8 b c' b a2 |
  a8\4( g\4 a4\4) d d |
  b8\4 a\4 b\4 a\4 g2\4 |
  g8( fis g4) c c |
  a8\2 g\2 a\2 g\2 fis2 |
  fis4 e e e ~ |
  e8 ees e2.\3 |
  \bar "||"
  b8 a b4 cis' d' ~ |
  d'8 d' c' b a2 |
  c'4 b a8\4 g\4 a4\4 |
  fis g4. a8 b c' |
  d'4 c' b a8\4 g\4 |
  a4\4 fis g4. fis8 |
  e fis g4 fis e8 ees |
  e4\3 fis\3 e2\3 |
  \bar "|."
 
}

\include "../muziko.ly"
