\include "../ĉiea.ly"


\header {
  titolo-xx     = ""
  titolo-he     = "ילדת טבע: שש גמדות"
  titolo-eo     = "Naturido: ses nanoj"
  komponisto-xx = ""
  komponisto-he = "חוה אלברשטיין"
  komponisto-eo = "Ĥava Alberŝtejn"
  ikono         = "🍪"
}

\include "../titolo.ly"

melodio = {
  \key f \major
  f8 f f f8. c16 d8 c c ~ |
  c f f16 f f8 c d c c ~ |
  c f f16 f f8 c d4 e8 ~ |
  e f ~ f2. |
  \break
  f8 f16 f f8 c d c c4 |
  f8 f f c d c c4 |
  f8 f f c d4 e |
  f1 |
  \break
  f8 g g g8. f16 g8 f d |
  f g g g f g f d |
  f g g g f d4 e8 ~ |
  e f ~ f2. |
  \break
  f8 f f f c d c c |
  f f f f c d c c |
  f f f f c d4 e8 ~ |
  e f ~ f2. |
  \break \repeat unfold 2 {
    f8 g g g f g f d |
  }
  r f r d r f r d |
  r f' ~ f'2. |
  \bar "|."
}

\include "../muziko.ly"
