\include "../ĉiea.ly"

#(set-global-staff-size 27)

\header {
  titolo-xx     = ""
  titolo-he     = "שיר לשירה"
  titolo-eo     = "Kanto por Ŝira"
  komponisto-xx = ""
  komponisto-he = "קורין אלאל"
  komponisto-eo = "Korin Alal"
  ikono         = "🌛"
}

\include "../titolo.ly"

% Sefer Korin Alal
melodio = {
  \time 3/4
  \key c \major
  \partial 4 \tuplet 3/2 { r4 d8 } |
  \repeat volta 2 {
    \tuplet 3/2 { e4 d8 } e4 \tuplet 3/2 { r4 d8 } |
    f4 \tuplet 3/2 { r4 d8 } \tuplet 3/2 { d4 c8 } |
    \tuplet 3/2 { d4 e8 ~ } e4 r |
    c e c' |
	% different harmony
    a2. |
    \tuplet 3/2 { r4 f8 } \tuplet 3/2 { f4 e8 } \tuplet 3/2 { f4 e8 } |
    f2. |
    r4 r \tuplet 3/2 { r4 d8 }
  }
  \tuplet 3/2 { e4 a8 } \tuplet 3/2 { a4 g8\4 } \tuplet 3/2 { g4\4 fis8 } |
  fis4 \tuplet 3/2 { r4 e8 } \tuplet 3/2 { e4 d8 } |
  e4 e2 |
  c4 e c' |
  \tuplet 3/2 { a4 f8 } \tuplet 3/2 { f4 e8 } \tuplet 3/2 { f4 e8 } |
  f2. |
  r4 r \tuplet 3/2 { r4 d8 } |
  e2 \tuplet 3/2 { r4 a8 } |
  \tuplet 3/2 { a4 g8\4 } \tuplet 3/2 { g4\4 fis8 } \tuplet 3/2 { e4 d8 } |
  e4 e \tuplet 3/2 { r4 c8 } \tuplet 3/2 { c4 c8 } e4 \tuplet 3/2 { c'4 a8 ~ } |
  a4 \tuplet 3/2 { r4 g8\4 } \tuplet 3/2 { a4 g8\4 } |
  a2. |
  \pageBreak
  r4 r \tuplet 3/2 { r4 d8 } |
  b4 \tuplet 3/2 { r4 c'8 } \tuplet 3/2 { d'4 b8 } |
  a4 \tuplet 3/2 { r4 g8\4 } \tuplet 3/2 { a4 g8\4 } |
  a4 g2\4 |
  r2. |
  b2 \tuplet 3/2 { b4 c'8 } |
  \tuplet 3/2 { d'4 b8 } a4 \tuplet 3/2 { r4 g8\4 } |
  \tuplet 3/2 { a4 g8\4 } b2 |
  r2. |
  \tuplet 3/2 { r4 d8 } \tuplet 3/2 { b4 c'8 } \tuplet 3/2 { d'4 b8 } |
  \tuplet 3/2 { a4 g8\4 } \tuplet 3/2 { a4 g8\4 } \tuplet 3/2 { a4 g8\4 } |
  \tuplet 3/2 { a4 g8\4 ~ } g2\4 |
  r4 r \tuplet 3/2 { r4 d8 } |
  b4 \tuplet 3/2 { r4 c'8 } \tuplet 3/2 { d'4 b8 } |
  a2. |
  \tuplet 3/2 { r4 e8 } \tuplet 3/2 { g4\4 e8 } \tuplet 3/2 { g4\4 e8 } |
  g4.\4 r8 r4 |
  \bar "|."
}

% http://www.tavisraeli.co.il/song/%d7%a9%d7%99%d7%a8-%d7%9c%d7%a9%d7%99%d7%a8%d7%94/
melodiotavisraeli = {
  \time 3/4
  \key c \major %??
  \set Timing . beamExceptions = #'()
  \partial 8 e8 |
  \repeat volta 2 {
	e8 d e4. d8 |
	f4. d8 d c |
	d4 e2 |
	c4 e c' |
	a4. f8 f e |
	f e f2 |
	r r8 e |
  }
  \break e a a g\4 g\4 fis |
  fis4. e8 e d |
  e4 e2 |
  c4 e c' |
  a4. f8 f e |
  f e f2 |
  \break
  r r8 e |
  e2 ~ e8 a |
  a g\4 g\4 fis e d |
  e4 e ~ e8 c |
  c c e4 c' |
  a4. g8\4 a g\4 |
  a2. |
  \break
  r2 r8 d |
  b4. c'8 d' b |
  a4. g8\4 a g\4 |
  a4 g2\4 ~ |
  g2.\4 |
  \break
  b2 b8 c' |
  d' b a4. g8\4 |
  a g\4 b2 ~ |
  b2. |
  e8 b4 c'8 d' b |
  a g\4 a g\4 a g\4 |
  a4 g2\4 |
  \break
  r r8 d |
  b4. c'8 d' b |
  a2. |
  r8 e g\4 e g\4 e |
  g2.\4 |
  \bar "|."
}

\include "../muziko.ly"
