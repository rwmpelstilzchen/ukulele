\include "../ĉiea.ly"

#(set-global-staff-size 26)

\header {
  titolo-xx     = "Super Mario Bros."
  titolo-he     = "האחים סופר מריו"
  titolo-eo     = "Super Mario Fratoj"
  komponisto-xx = "近藤浩治"
  komponisto-he = "קוג׳י קונדו"
  komponisto-eo = "Kōĝi Kondō"
  ikono         = "🍄"
}

\include "../titolo.ly"

#(define (naturalize-pitch p)
   (let ((o (ly:pitch-octave p))
         (a (* 4 (ly:pitch-alteration p)))
         ;; alteration, a, in quarter tone steps,
         ;; for historical reasons
         (n (ly:pitch-notename p)))
     (cond
      ((and (> a 1) (or (eq? n 6) (eq? n 2)))
       (set! a (- a 2))
       (set! n (+ n 1)))
      ((and (< a -1) (or (eq? n 0) (eq? n 3)))
       (set! a (+ a 2))
       (set! n (- n 1))))
     (cond
      ((> a 2) (set! a (- a 4)) (set! n (+ n 1)))
      ((< a -2) (set! a (+ a 4)) (set! n (- n 1))))
     (if (< n 0) (begin (set! o (- o 1)) (set! n (+ n 7))))
     (if (> n 6) (begin (set! o (+ o 1)) (set! n (- n 7))))
     (ly:make-pitch o n (/ a 4))))

#(define (naturalize music)
   (let ((es (ly:music-property music 'elements))
         (e (ly:music-property music 'element))
         (p (ly:music-property music 'pitch)))
     (if (pair? es)
         (ly:music-set-property!
          music 'elements
          (map (lambda (x) (naturalize x)) es)))
     (if (ly:music? e)
         (ly:music-set-property!
          music 'element
          (naturalize e)))
     (if (ly:pitch? p)
         (begin
           (set! p (naturalize-pitch p))
           (ly:music-set-property! music 'pitch p)))
     music))

naturalizeMusic =
#(define-music-function (parser location m)
   (ly:music?)
   (naturalize m))

xmelodio = \naturalizeMusic\transpose cis c {
  
  \time 4/4
  d'8 d' r d' r bes d' r |
  f' r r4 f8 r r4 |
  \repeat volta 2 {
    bes8 r r f r4 d8 r |
    r g r a r aes g r |
    \tuplet 6/4 { f8 r d'\3 r f'\2 r } g'8 r ees' f' |
    r d' r bes c' a r4 |
    bes8 r r f r4 d8 r |
    r g r a r aes g r |
    \tuplet 6/4 { f8 r d' r f' r } g'8 r ees' f' |
    r d' r bes c' a r4 |
    r f'8 fes' ees' des' r d' |
    r fis g bes r g bes c' |
    r4 f'8 fes' ees' des' r d' |
    r bes' r bes' bes' r r4 |
    r f'8 fes' ees' des' r d' |
    r fis g bes r g bes c' |
    r4 des'8 r r c' r4 |
    bes8 r r4 r2 |
    r4 f'8 fes' ees' des' r d' |
    r fis g bes r g bes c' |
    r4 f'8 fes' ees' des' r d' |
    r bes' r bes' bes' r r4 |
    r f'8 fes' ees' des' r d' |
    r fis g bes r g bes c' |
    r4 des'8 r r c' r4 |
    bes8 r r4 r2 |
    bes8 bes r bes r bes c' r |
    d' bes r g f r r4 |
    bes8 bes r bes r bes c' d' |
    R1 |
    bes8 bes r bes r bes c' r |
    d' bes r g f r r4 |
    d'8 d' r d' r bes d' r |
    f' r r4 f8 r r4 |
    bes8 r r f r4 d8 r |
    r g r a r aes g r |
    \tuplet 6/4 { f8 r d' r f' r } g'8 r ees' f' |
    r d' r bes c' a r4 |
    bes8 r r f r4 d8 r |
    r g r a r aes g r |
    \tuplet 6/4 { f8 r d' r f' r } g'8 r ees' f' |
    r d' r bes c' a r4 |
    d'8 bes r f r r ges r |
    g ees' r ees' g r r4  |
    \tuplet 6/4 { a8 r g' r g' r } \tuplet 6/4 { g'8 r f' r ees' r } |
    d'8 bes r g f r r4 |
    d'8 bes r f r r ges r |
    g ees' r ees' g r r4 |
    a8 ees' r ees' \tuplet 6/4 { ees'8 r d' r c' r } |
    bes8 d r d bes, r r4 |
    d'8 bes r f r r ges r |
    g ees' r ees' g r r4 |
    \tuplet 6/4 { a8 r g' r g' r } \tuplet 6/4 { g'8 r f' r ees' r } |
    d'8 bes r g f r r4 |
    d'8 bes r f r r ges r |
    g ees' r ees' g r r4 |
    a8 ees' r ees' \tuplet 6/4 { ees'8 r d' r c' r } |
    bes8 d r d bes, r r4 |
    bes8 bes r bes r bes c' r |
    d' bes r g f r r4 |
    bes8 bes r bes r bes c' d' |
    R1 |
    bes8 bes r bes r bes c' r |
    d' bes r g f r r4 |
    d'8 d' r d' r bes d' r |
    f' r r4 f8 r r4 |
    d'8 bes r f r r ges r |
    g ees' r ees' g r r4 |
    \tuplet 6/4 { a8 r g' r g' r } \tuplet 6/4 { g'8 r f' r ees' r } |
    d'8 bes r g f r r4 |
    d'8 bes r f r r ges r |
    g ees' r ees' g r r4 |
    a8 ees' r ees' \tuplet 6/4 { ees'8 r d' r c' r }
  } \alternative { {
    |
    bes8 d r d bes, r r4
  } } |
  a8 ees' r ees' \tuplet 6/4 { ees'8 r d' r c' r } |
  bes4-- r8 f4-- r8 d4-- |
  g( a g ges |
  aes ges f2 ~ |
  f1) |
  \bar "|."
}

melodio = {
   \key a \major
   \time 4/4
   <cis'\1>8 <cis'\1>8 r8 <cis'\1>4 <a\1>8 <cis'\1>8 r8 |
   <e'\1>8 r4. <e\2>8 r4. |
   \repeat volta 2 {
      <a\1>8 r4 <e\2>8 r4 <cis\3>8 r8 |
      r8 <fis\2>8 r8 <gis\2>4 <g\2>8(\glissando <fis\2>8) r8 |
      <e\2>8 <a\3>4 <cis'\2>8 <fis'\1>4 <d'\4>8 <e'~\1>8 |
      <e'\1>8 <cis'\4>8 ~ cis'\4 <a\1>8( <b\1>8) <gis\4>4 r8 |
   }
   \break
   \repeat volta 2 {
      <a\1>4 <e'\1>8( <dis'\1>8 <d'\1>8) <c'\4>4 <cis'\4>8 |
      r8 <f\3>8( <fis\3>8) <a\2>4 <fis\3>8 <a\2>8 <b\2>8 |
      r4 <e'\1>8( <dis'\1>8 <d'\1>8) <c'\4>4 <cis'\4>8 |
      r8 <a'\1>4 <a'\1>8 <a'\1>4 r4 |
	  \break
      <a\1>4 <e'\1>8( <dis'\1>8 <d'\1>8) <c'\4>4 <cis'\4>8 |
      r8 <f\3>8 <fis\3>8 <a\2>4 <fis\3>8 <a\2>8 <b\2>8 |
      r4 <c'\1>4. <b\1>4. |
      <a\1>8 r4 <a\1>8 <a\1>8 r8 <a\1>8 r8 |
   }
   \break
   <a\1>8 <a\1>8~ a\1 <a\1>4 <a\1>8 <b\1>4 |
   <cis'\1>8 <a\1>8~ a\1 <fis\2>8 <a\1>4 <a\1>4 |
   <a\1>8 <a\1>8~ a\1 <a\1>4 <a\1>8 <a\1>8 <a\1>8 |
   <cis'\1>4 <cis'\1>8 <cis'\1>4 <cis'\1>8 <cis'\1>8 r8 |
   \break
   <a\1>8 <a\1>8~ a\1 <a\1>4 <a\1>8 <b\1>8 <a\1>8 |
   <cis'\1>8 <a\1>8~ a\1 <fis\2>8 <a\1>4 <a\1>4 |
   <cis'\1>8 <cis'\1>8 r8 <cis'\1>4 <a\1>8 <cis'\1>8 r8 |
   <e'\1>8 r4. <e\2>8 r4. |
   \break
   \repeat volta 2 {
      <a\1>8 r4 <e\2>8 r4 <cis\3>8 r8 |
      r8 <fis\2>8 r8 <gis\2>4 <g\2>8(\glissando <fis\2>8) r8 |
      <e\2>8 <a\3>4 <cis'\2>8 <fis'\1>4 <d'\4>8 <e'~\1>8 |
      <e'\1>8 <cis'\4>4 <a\1>8( <b\1>8) <gis\4>4 r8 |
   }
   \break
   \repeat volta 2 {
      <cis'\1>8 <a\2>8 a\2 <e\3>4 <e\3>8(\glissando <f\3>4) |
      <fis\3>8 <d'\1>8~ d'\1 <d'\1>8 <d'\1>4 <a\2>8 <ais\2>8 |
   }
   \alternative {
	 {
	   <b\2>8 <fis'\1>8. <fis'\1>8. <fis'\1>8. <e'\1>8. <d'\1>8 |
	   <cis'\1>8 <a\2>4 <fis\3>8 <e\3>2 |
	 }
	 {
	   <gis\2>8 <d'\1>8. <d'\1>8. <d'\1>8. <cis'\1>8. <b\4>8 |
	   <cis'\1>8 r8 <cis'\1>8 <cis'\1>8 <cis'\1>2 |
	 }
   }
   \break
   \repeat volta 2 {
      <cis'\1>8 <a\2>8 ~ a\3 <e\3>4 <e\3>8(\glissando <f\3>4) |
      <fis\3>8 <d'\1>8 ~ d'\1 <d'\1>8 <d'\1>4 <a\2>8 <ais\2>8 |
   }
   \alternative {
	 {
	   <b\2>8 <fis'\1>8. <fis'\1>8. <fis'\1>8. <e'\1>8. <d'\1>8 |
	   <cis'\1>8 <a\2>4 <fis\3>8 <e\3>2 
	 }
	 {
	   <gis\2>8 <d'\1>8. <d'\1>8. <d'\1>8. <cis'\1>8. <b\4>8 
	 }
   }
   \break
   \repeat volta 2 {
	 <a\1>8 <a\1>8 <fis\2>8 <fis\2>8 <g\4>8 <g\4>8 r4 |
	 r1 |
   }
   \repeat volta 3 {
      <a\2>8 <a\2>8 <fis\2>8 <fis\2>8 <g\2>8 <g\2>8 <gis\2>8 <gis\2>8 |
	  \mark\markup{"…"}
   }
   <a\1>8 <a\1>8 r4 <a'\1>4 r4 |
   \break
%   \repeat volta 2 {
%      <a\1>8 r4 <e\2>8 r4 <cis\3>8 r8 |
%      r8 <fis\2>8 r8 <gis\2>4 <g\2>8(\glissando <fis\2>8) r8 |
%      <e\2>8 <a\3>4 <cis'\2>8 <fis'\1>4 <d'\4>8 <e'~\1>8 |
%      <e'\1>8 <cis'\4>8 ~ cis'\4 <a\1>8( <b\1>8) <gis\4>4 r8 |
%   }
%   <a\1>1 |
   \bar "|."
}

\include "../muziko.ly"
