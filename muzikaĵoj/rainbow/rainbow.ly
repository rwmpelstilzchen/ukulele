\include "../ĉiea.ly"

\header {
  titolo-xx     = "Over the Rainbow"
  titolo-he     = "אי־שם מעבר לקשת"
  titolo-eo     = "Trans ĉielarko"
  komponisto-xx = ""
  komponisto-he = "הרולד ארלן"
  komponisto-eo = "Harold Arlen"
  ikono         = "🌈"
}

\include "../titolo.ly"

melodio = {
  \displayLilyMusic{
  \key d \major
  \time 4/4
  {
	\repeat volta 2 {
	  d2\3 d'2\1 |
	  cis'4\1 a8\1 b8\1 cis'4\1 d'4\1 |
	  d2\3 b2\1 |
	  a1\1 |\break
	  d2\3 g2\4 |
	  fis4\2 d8\3 e8\2 fis4\2 g4\4 |
	  e4\2 cis8\3 d8\3 e4\2 fis4\2 |
	}
	\alternative {
	  {d1\3 | }
	  {d2.\3 r8 a8\1 |}
	}
	\bar "||"
	\break

	\repeat unfold 4 {fis8\2 a8\1} |
	\repeat unfold 4 {g8\4 a8\1} |
	b2 b2~ |
	b2. r8 a8\1 |
	\repeat unfold 4 {fis8\2 a8\1} |
	\repeat unfold 4 {gis8\2 b8\4}|
	cis'2 cis' |
	e'2 a |
	\bar "||"
	\break

	d2\3 d'2\1 |
	cis'4\1 a8\1 b8\1 cis'4\1 d'4\1 |
	d2\3 b2\1 |
	a1\1 |
	d2\3 g2\4 |
	fis4\2 d8\3 e8\2 fis4\2 g4\4 |
	e4\2 cis8\3 d8\3 e4\2 fis4\2 |
	d1\3 |
	\bar "|."
  }
}
}

\include "../muziko.ly"
