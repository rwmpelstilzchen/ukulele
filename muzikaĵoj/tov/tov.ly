\include "../ĉiea.ly"

\header {
  titolo-xx     = ""
  titolo-he     = "הכבש הששה־עשה: לילה טוב"
  titolo-eo     = "La deksesa ŝafo: Bonan nokto"
  komponisto-xx = ""
  komponisto-he = "יוני רכטר"
  komponisto-eo = "Joni Reĥter"
  ikono         = "🌚"
}

\include "../titolo.ly"

melodio = {
  \time 4/4
  \key c \major

  \repeat volta 2 {
	e2 r |
	e8 f g\4 g\4 ~ g\4 f e d ~ |
	d4 r d8 e f f ~ |
	\time 2/4
	f8 c c c ~ |
	\time 4/4
	c8 d d4 r8 c d e ~ |
	e2 r |
	\break
	e8 f g\4 g\4 ~ g\4 a bes a ~ |
	a4 c8 c ~ c4 c8 c ~ |
	c8 d d4 r2 |
	e4. f16 e d4 r |
	\break
	g2\4 r |
	g8\4 aes\4 bes bes ~ bes aes\4 g\4 f ~ |
	f4 r f8 g\4 aes\4 aes\4 ~ |
	aes8\4 g\4 f aes\4 ~ aes\4 g\4 f g\4 ~ |
	\time 2/4
	g8\4 f f e ~
	\time 4/4
	e2 r4 r8 g8 | % g,
	e8 f g\4 g\4 ~ g\4 f e c' ~ |
	c'4 c r2 |
	f2 c2 |
	\time 2/4
	d4. c8 |
  }
  \alternative {
	{
	  \time 4/4
	  c2 r |
	  r1 |
	}
	{
	  \time 4/4
	  c2 r |
	}
  }
  \bar "|."
}

\include "../muziko.ly"
