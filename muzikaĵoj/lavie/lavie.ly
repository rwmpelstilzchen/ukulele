\include "../ĉiea.ly"

\header {
  titolo-xx     = "Il était une fois… la vie"
  titolo-he     = "היה היה… החיים"
  titolo-eo     = "Iam estis… la vivo"
  komponisto-xx = ""
  komponisto-he = "מישל לגרה"
  komponisto-eo = "Michel Legrand"
  ikono         = "💀"
}

\include "../titolo.ly"

melodio = {
  \time 4/4
  \key c \major
  g4\4 g\4 g2\4 |
  a4 a a2 |
  g4\4 g\4 g2\4 |
  f4 f f2 |
  \break
  c4 d8 e4 f4 g8\4~ | 
  g1\4 |
  a8 g\4 f e4 d4 e8 |
  f4 g8\4 c2 r8 |
  \break
  c4 d8 e4 f g8\4 ~ |
  g8\4 c'2. c'8 |
  b8 a8 a4 g8\4 a8 a4 |
  g1\4 |
  \break
  b8 c' d'4. c'8 b4 |
  a8 g8\4 g4\4 c'2 |
  a8 b8 c'4 b8 a4 g8\4 |
  a8 a4 g2\4 r8 |
  \break
  c4 d8 e4 f4 g8\4~ |
  g8\4 c'2.~ c'8 |
  b8 a g4\4 f8 e d4 |
  d2 c4 c4 |
  \break
  \repeat volta 2 {
	g4\4\dim g\4 g2\4 |
	a4 a a2 |
	g4\4 g\4 g2\4 |
	f4 f f2\ppp
	\break
  }
}

\include "../muziko.ly"
