\include "../ĉiea.ly"

\header {
  titolo-xx     = "Frozen: Let it Go"
  titolo-he     = "לשבור את הקרח: לשחרר"
  titolo-eo     = "Frosta: Liberu"
  komponisto-xx = ""
  komponisto-he = "קריסטן אנדרסון-לופז ורוברט לופז"
  komponisto-eo = "Kristen Anderson-Lopez kaj Robert Lopez"
  ikono         = "🌨"
}

\include "../titolo.ly"

melodio = {
  \compressFullBarRests
  \key f \major
  \time 4/4
  \interim
  e'8 f' a\2 e' ~ e' f'4. |
  e'8 f' a\2 f' ~ f' e'4 g8\4 |
  d' e' g\4 d' ~ d' e'4 g8\4 |
  c'2\2 bes\2 |
  \break
  e'8 f' a\2 e' ~ e' f'4. |
  f'8 e' a\2 f' ~ f' e'4 g8\4 |
  d' e' g\4 d' ~ d' e'4 g8\4 |
  c'2 b4. \deinterim g8\4 |
  \break
  a4 a a a8 a |
  a g\4 f f ~ f4 f8 f |
  g\4 g4.\4 ~ g8\4 f e d ~ |
  d2 r4 r8 d |
  \break
  a a a a ~ a4 c'8 c' |
  a ~ a4. r4 f8 f |
  g\4 a4. r8 g\4 f g\4 ~ |
  g2\4 r |
  \break
  r4 a8 a ~ a c'4 d'8 ~ |
  d' c'4 a8 ~ a c'4 c'8 ~ |
  c' c'4 bes8 a a4 bes8 |
  a1 |
  \break
  r4 a8 a g\4 f4 g8\4 ~ |
  g4\4 g8\4 a g4\4 f ~ |
  f d2. ~ |
  d2 r |
  \break
  r4 g8\4 g\4 ~ g\4 c'4 c'8 ~ |
  c' g4\4 g8\4 ~ g\4 d'4 d'8 |
  r4 d'8 c' d' d'4 c'8 |
  d' e'4 f'8 ~ f' e'4 e'8 |
  r4 g8\4 g\4 ~ g\4 c'4 c'8 |
  r4 g8\4 g\4 ~ g\4 d'4 d'8 ~ |
  d'1 ~ |
  d'2 r4 c'8 d' ~ |
  d' e'4. f'2 ~ |
  f' r8 d' e' f' ~ |
  f'2 r8 c'\2 c'\2 g' ~ |
  g'2. f'4 |
  d'8 d' d'4 d'8 e'4 f'8 ~ |
  f'2 r8 d' e' f' ~ |
  f'2 f'8 c'\2 a' g' ~ |
  g'2 r8 f' g' a' ~ |
  a' a'4 bes'8 a'4 g'8 f' |
  g' f'2. r8 |
  c''4. a' g'4\2 ~ |
  g'2\2 c'4\3 c'\3 |
  c''4. a' f'4\2 ~ |
  f'4.\2 r8 r4 f'8 f' |
  e'4. c'\2 c'4\2 ~ |
  c'2\2 r4 r8 r |
  bes4\4 bes8\4 a\4 bes\4 a\4 bes\4 bes\4 |
  a f4. r2 |
  R1*2 |
  \break
  r8 a a a a a4 a8 ~ |
  a f4. r4 f |
  c' c'8 bes ~ bes a4 g8\4 ~ |
  g2\4 r4 f8 f |
  \break
  a4 a8 a ~ a c'4 d'8 ~ |
  d'4 c' r c'\2 |
  f' f'8 e' d' d'4 d'8 ~ |
  d'1 |
  \break
  r4 g8\4 g\4 ~ g\4 c'4\2 c'8\2 ~ |
  c'\2 g4\4 g8\4 ~ g\4 c'4\2 d'8\2 |
  r4 c'8\2 d'\2 ~ d'\2 c'4\2 d'8\2 ~ |
  d' e'4 f'8 ~ f' g'4 c'8\2 ~ |
  c'4\2 g8\4 g\4 ~ g\4 c'4\2 c'8\2 ~ |
  c'\2 g4\4 c'8\2 ~ c'\2 g\4 d'4\2 ~ |
  d'2 r8 e'4. |
  f'1 |
  R |
  \break
  r2 r8 d' e' f' ~ |
  f'2 r8 c'\2 c'\2 g' ~ |
  g'2 r8 f' d' d' ~ |
  d' d' d' d'4 e' f'8 ~ |
  f' g' f'4 r8 d' e' f' ~ |
  f'2 r8 c'\2 a' g' ~ |
  g'2 r4 f'8 a' ~ |
  a' a'4 bes'4. a'8 g' |
  g' f'4. r2 |
  c''4. a' g'4\2 ~ |
  g'2.\2 c'4\3 |
  c''4. a' f'4\2 ~ |
  f'4. r8 r4 f'8 f' |
  e'4. c'\2 c'4\2 ~ |
  c'2\2 r |
  R1*4 |
  \break
  r4 bes8\3 a\3 ~ a\3 bes4\3 c'8\2 ~ |
  c'\2 d'4\2 ees'8\2 ~ ees'\2 f'4 aes'8 ~ |
  aes' g'4 f'8 ~ f' ees'4\2 f'8 ~ |
  f'1 |
  \break
  r4 bes8\3 a\3 ~ a\3 bes4\3 c'8\2 ~ |
  c'\2 d'4\2 ees'8\2 ~ ees'\2 f'4 aes'8 ~ |
  aes' g'4 f'8 ~ f' ees'4\2 aes'8 ~ |
  aes' g' f' g' f'2 |
  \break
  r4 c'8\2 b\2 ~ b\2 c'4\2 d'8\2 ~ |
  d'\2 e'4 f'8 ~ f' g'4 bes'8 ~ |
  bes' a'4 g'8 ~ g' f'4 g'8 ~ |
  g'1 |
  \break
  r4 a'8 a' ~ a' a'4 a'8 ~ |
  a'4 f'8 f' ~ f'4 r8 f' |
  g'4 f'8 g' g' a'4 bes'8 ~ |
  bes'1 ~ |
  \break
  bes'2 ~ bes'8 bes' bes' bes' |
  a'2 r8 c'\2 c'\2 g' ~ |
  g'2 r8 f' d' d' ~ |
  d' d' d' d' ~ d' e'4 f'8 ~ |
  f' g' f'4 r8 d' e' f' ~ |
  f'2 r8 c'\3 a' a' |
  g'2 r8 f'4\2 a'8 ~ |
  a' a'4 bes'8 ~ bes' a' g' f'\2 |
  g' f'4.\2 ~ f'4\2 r |
  c'' a'8 a' a'4 g' ~ |
  g'2 f'4\2 f'\2 |
  c'' a'8 a' ~ a'4 bes'8 a' ~ |
  a' g' f'2.\2 ~ |
  f'2\2 r4 f'8\2 f'\2 |
  e'4.\2 f'8\2 ~ f'4\2 c'' ~ |
  c''1 ~ |
  c''2. r8 aes\4 |
  bes4\4 bes8\4 a\4 bes\4 a\4 bes\4 bes\4 |
  a f4. r2 |
  \bar "|."
}   

\include "../muziko.ly"
