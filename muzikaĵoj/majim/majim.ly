\include "../ĉiea.ly"

\header {
  titolo-xx     = ""
  titolo-he     = "מה עם המים?"
  titolo-eo     = "Kio okazis pri la akvo?"
  komponisto-xx = ""
  komponisto-he = "נורית הירש"
  komponisto-eo = "Nurit Hirŝ"
  ikono         = "🚰"
}

\include "../titolo.ly"

melodio = \transpose g aes {
  \time 4/4
  \key g \major
  \partial 8 d8 |
  b,8 d d d c e4 e8 |
  d8 d e fis g4 d |
  b4 g e8. e16 a8 a|
  g8 fis e d b4 g8 g |
  \break
  c'8 c' a c' b8. g16 g8 b |
  a8 a g a b4 g8 b |
  a8 a a a a e fis g |
  a2 ~ a8 r8 r4 |
  \break
  g4 d8 d e4 d |
  g8 g d d e4 d |
  g8 g fis g a4 e8 e |
  g8 fis e fis g4 r |
  \break
  g8 g g d e e d d |
  g8 g g d e e d d |
  g8 g fis g a4 r8 e |
  g8 fis e fis g4 r |
  \bar "|."
}

\include "../muziko.ly"
