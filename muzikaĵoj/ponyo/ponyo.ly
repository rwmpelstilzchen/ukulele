\include "../ĉiea.ly"

\header {
  titolo-xx     = "崖の上のポニョ"
  titolo-he     = "פוניו על הצוק"
  titolo-eo     = "Ponjo sur la klifo"
  komponisto-xx = "久石譲"
  komponisto-he = "ג׳ו היסאישי"
  komponisto-eo = "Ĝo Hisaiŝi"
  ikono         = "🐠"
}

\include "../titolo.ly"

melodio = {
  \time 4/4
  \key f \major
  \interim
  c4. c16 c c4. c16 c |
  c8 c d c c dis c e\fermata |
  \deinterim
  \break
  \repeat volta 2 {
    c'4 a8 f4 c8 c c |
    d f bes d' c'4 r8 a |
    bes g\4 g\4 bes a f4 a8 |
    g\4 d e f g4\4 r |
	\break
    c' a8 f4 c8 c c |
    d f bes d' c'4 r8 a |
    bes g\4 g\4 bes a f r a |
    g\4 e r f4. r8 \interim e |
	\break
    a f a f bes f bes f |
    a f a f g\4 f g\4 f |
	\deinterim
	\break
    f4 c8 f g4\4 r |
    g\4 c8 g\4 a4 r |
    a8. f a8 bes c' d'4 |
    c'8. a f8 g4\4 r |
	\break
    f c8 f g4\4 r |
    g\4 c8 g\4 a4 r |
    a8 f f a bes c' d'4 |
    c'8 a4 c8 f4 r |
	\break
    r8 g\4 g\4 g\4 g4\4 f8 g\4 |
    a2 c' |
    r8 g4\4 a8 g\4 f f des |
    c4 a2 g4\4 |
	\break
    f8. f16 f8 f d' c' r4 |
    d8. d16 d8 d c' bes r4 |
    r a bes c' |
    f f g\4 a |
    g2.\4 r4 |
    c f g\4 a |
  }
  \break
  c' a8 f4 c8 c c |
  d f bes d' c'4 r8 a |
  bes g\4 g\4 bes a f4 a8 |
  g\4 d e f g4\4 r |
  \break
  c' a8 f4 c8 c c |
  d f bes d' c'4 r8 a |
  bes g\4 g\4 bes a f r a |
  g\4 e r f2 r8 |
  \break
  \interim
  c' a a f f c c a |
  des' bes bes f f des' des' bes |
  c' r4 c'16 c' d'8 d' e' e' |
  f' r r4 \acciaccatura { d'8( e' } f'4) r |
  \bar "|."
}

\include "../muziko.ly"
