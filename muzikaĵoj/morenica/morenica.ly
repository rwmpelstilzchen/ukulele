\include "../ĉiea.ly"

\header {
  titolo-xx     = "Morenica"
  titolo-he     = "שחרחורת"
  titolo-eo     = "Nigranjo"
  komponisto-xx = ""
  komponisto-he = ""
  komponisto-eo = ""
  ikono         = "●"
}

\include "../titolo.ly"

melodio = {
  \key d \minor
  \time 2/4
  \partial 8*2 bes8 a |
  bes4 a |
  bes4 c' |
  d'2 |
  c'8 bes a g\4 |
  f4 e8 d |
  e2 |
  bes2 |
  \break
  a4 g8\4 a |
  g4\4 f8 e |
  f2 |
  g8\4 a16( bes a8) g\4 |
  g2\4 ~ |
  g2\4 ~ |
  \break
  g4\4 bes8 a |
  bes4 a |
  bes4 c' |
  d'2 |
  c'8 bes a g\4 |
  f4 e8 d |
  e2 |
  bes2 |
  \break
  a4 g8\4 a |
  g4\4 f8 e |
  f2 |
  g8\4 a16( bes a8) g\4 |
  g2\4 ~ |
  g2\4 ~ |
  g2\4 |
  \break
  a8 g\4 f e |
  d4. e8 |
  c2 ~ |
  c2 |
  a8 g\4 f e |
  d8 f e d |
  c2 ~ |
  \break
  c4 c8 c |
  d4 e |
  f4 g\4 |
  a4 bes8 c' |
  a4 g\4 |
  f4. ees8 |
  g4\4 ees |
  d2 ~ |
  d2 |
  \bar "|."
}

\include "../muziko.ly"
