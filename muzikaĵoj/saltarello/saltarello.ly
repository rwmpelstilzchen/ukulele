\include "../ĉiea.ly"

\header {
  titolo-xx     = ""
  titolo-he     = "סלטרלו II"
  titolo-eo     = "Saltarello II"
  komponisto-xx = ""
  komponisto-he = ""
  komponisto-eo = ""
  ikono         = "⇅"
}

\include "../titolo.ly"

melodio = {
  \time 2/2
  \key d \dorian
  \tempo 4 = 160
  \mark "A" \repeat volta 2 {
	a d a d |
	a d a2 |
	g4\4 e8 f g\4 f e f |
	d4 a d2 |
	d4 e f8 e f e |
	c4 g\4 c2 |
	d g4.\4 e8
  }
  \alternative {
	{
	  f2 e8 d c d |
	  e2 a |
	  e f4 g\4 |
	}
	{
	  f2 e8 d c e |
	  d2 a |
	  d e4 f |
	}
  }
  \bar "||"
  \break

  \mark "B"
  g\4 d g\4 d |
  g\4 d g8\4 f e f |
  d4 a d2 |
  g8\4 f e f e d e f |
  g4\4 d g\4 d |
  g\4 d g8\4 f e f |
  d4 a d2 |
  c8 d e f d e f g\4 |
  \bar "||"
  \break
  \grace s16

  \mark "A"
  \repeat volta 2 {
	a4 d a d |
	a d a2 |
	g4\4 e8 f g\4 f e f |
	d4 a d2 |
	d4 e f8 e f e |
	c4 g\4 c2 |
	d g4.\4 e8 |
  }
  \alternative {
	{
	  f2 e8 d c d |
	  e2 a |
	  e f4 g\4
	}
	{
	  f2 e8 d c e |
	  d2 a |
	  d b4 c' |
	}
  }
  \bar "||"
  \break
  \grace s16 

  \mark "C"
  \repeat volta 2 {
	d'2 d'4 d' |
	d'2 d'4 d' |
	d'2 d'4 d' |
	b8 c' d' c' b c' d' c' |
	b c' d' b c' d' c' b |
  }
  \alternative {
	{ c' b a b c' b a4 | }
	{ c'8 b a b c' b g4\4 | }
  }
  \bar "||"
  \break
  \grace s16 

  \mark "A"
  \repeat volta 2 {
	a4 d a d |
	a d a2 |
	g4\4 e8 f g\4 f e f |
	d4 a d2 |
	c4 g\4 c2 |
	d g4.\4 e8 |
  }
  \alternative {
	{
	  f2 e8 d c d |
	  e2 a |
	  e f4 g\4 |
	}
	{
	  f2 e8 d c e |
	  d2 a |
	  d e4 f |
	}
  }
  \bar "||"
  \break

  \mark "B"
  g\4 d g8\4 f e f |
  d4 a d2 |
  g8\4 f e f e d e f |
  g4\4 d g\4 d |
  g\4 d g8\4 f e f |
  d4 a d2 |
  c8 d e f d e f g\4 |
  \bar "||"
  \break

  \mark "D"
  a4 d a d |
  a d a2 |
  d4 a d2 |
  d4 e f8 e f e |
  c4 g\4 c2 |
  d g4.\4 e8 |
  f2 e8 d c e |
  d2 a |
  d b4 c' |
  \bar "||"
  \break

  \mark "E"
  d'2 d'4 d' |
  d'2 d'4 d' |
  d'2 d'4 d' |
  d'1 |
  \bar "|."
}

\include "../muziko.ly"
