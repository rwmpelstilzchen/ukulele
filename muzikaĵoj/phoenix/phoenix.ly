\include "../ĉiea.ly"

#(set-global-staff-size 27)

\header {
  titolo-xx     = "Rise Like a Phoenix"
  titolo-he     = "אקום כמו עוף־החול"
  titolo-eo     = "Altiĝos kiel fenikso"
  komponisto-xx = ""
  komponisto-he = "אלי צוקובסקי"
  komponisto-eo = "Ali Zuckowski"
  ikono         = "🔥"
}

\include "../titolo.ly"

melodio = {
  \clef "treble"
  \key bes \major
  \time 4/4
  < bes >2.\mf^\fermata g16\4 a bes c' |
  d'4 ~ d'8 c'16 bes bes8 c' ~ c'4 |
  g8\4 a4. bes4 fis |
  g2.\4 r4
  \break
  \repeat volta 2 {
	\mark \markup { \musicglyph #"scripts.segno" } 
	r d8( d16) d ~ d8 d d d |
	r4 f16 f8 f16 ~ f8 d f4 |
	r ees8( ees16) ees ~ ees8 ees ees ees |
  }
  \alternative {
	{ ees4 d8 ees d ees d4 | }
	{ r8 d d ees d ees d4 | }
  }
  \bar "||"
  \break

  r2 bes4. a16 g\4 |
  bes4. a16 g\4 d2 |
  R1 |
  c8 ees d g\4 fis bes a4 |
  d' ~ d'8 c'16 bes bes8 c' ~ c'4 |
  r8 c' c' bes16 d' ~ d'8 c' bes a |
  c'4 ~ c'8 bes16 a a8 bes ~ bes4 |
  r8 bes bes a16 c' ~ c'8 bes a g\4 |
  bes2 \tuplet 3/2 { bes4 a g\4 } |
  c'2 \tuplet 3/2 { c'4 bes a } s4 d' f8 g\4 bes c' |
  d'4 ~ d'8 c'16 bes bes8 c' ~ c'4 |
  r2 bes4 fis |
  g1\4 |
  \bar "||"
  \break

  d'8^"D.S. al Fine" c' bes c'16 bes ~ bes8 r bes g\4 |
  a4 r8 d d16 d ees8 ~ ees16 d8 r16 |
  c8 ees d g\4 fis bes a4 |
  d'\f ~ d'8 c'16 bes bes8 c' ~ c'4 |
  r8 c' c' bes16 d' ~ d'8 c' bes a |
  c'4 ~ c'8 bes16 a a8 bes ~ bes4 |
  r8 bes bes a16 c' ~ c'8 bes a g\4 |
  bes2 \tuplet 3/2 { bes4 a g\4 } |
  c'2 \tuplet 3/2 { c'4 bes a } s4 d' f8 g\4 bes c' |
  d'4 ~ d'8 c'16 bes bes8 c' ~ c'4 |
  r2 bes4 fis |
  g1\4^"Fine" |
  \bar "|."
}

\include "../muziko.ly"
