\include "../ĉiea.ly"

\header {
  titolo-xx     = ""
  titolo-he     = "אדון שוקו"
  titolo-eo     = "Sinjoro Trinkĉokolado"
  komponisto-xx = ""
  komponisto-he = "יוני רכטר"
  komponisto-eo = "Joni Reĥter"
  ikono         = "🎩"
}

\include "../titolo.ly"

melodio = {
  \compressFullBarRests
  \time 4/4
  \key a \minor
  \partial 8. g16\4 g8\4 |
  bes8 bes16 bes a8 g16\4 g\4 bes8 bes16 a ~ a a g8\4 ~ |
  g8\4 bes a4 g16\4 g\4 bes a ~ a4 |
  r1 |
  \break
  bes8 bes16 bes a8 g\4 bes bes16 a ~ a8 g16\4 g\4 |
  bes8 bes16 bes a8 g16\4 g\4 bes8 bes16 bes a a g\4 g\4 ~ |
  g16\4 e8. ~ e4 r2 |
  \break
  e8 g\4 ~ g4\4 g16\4 g\4 g\4 bes ~ bes8 g16\4 e |
  g16\4 f8. r4 g16\4 g\4 g\4 \transpose g, g {g,\4 ~ g,4\4} |
  d16 d d d d4 d8 d d4 |
  \time 2/4
  r8 d8 d d |
  \time 4/4
  \transpose g, g {g,2\4} r2 |
  \break
  b16 b b8 ~ b16 d d8 e d r4 |
  b16 b b8 ~ b16 d d8 e d r4 |
  \break
  %\repeat volta 2 {
	cis'8 cis'16 cis' cis'2 r8 r16 a |
	c'16 c' c'8 bes8 bes16 a ~ a4 r8 r16 a |
	c'8 c'16 bes ~ bes bes bes a ~ a4 d16 e fis g\4 ~ |
	g2\4 r |
	\break
	b8 b ~ b4 g8\4 g\4 ~ g4\4 |
	%R1*4 |
	R1 |
	%\break
	bes8 a16 g\4 bes8 a16 g\4 bes16 bes a g\4 bes a g\4 bes ~ |
	bes16 g\4 bes8 ~ bes r8 r a16 g\4 bes a g\4 bes ~ |
	bes2 r |
	\break
	e8 g\4 ~ g4\4 g16\4 g\4 g\4 bes ~ bes8 g16\4 e |
	g16\4 f8. r4 g16\4 g\4 g\4 \transpose g, g {g,\4 ~ g,4\4} |
	d16 d d d d4 d8 d d4 |
	\time 2/4
	r8 d d d |
	\time 4/4
	g2.\4 r8 r16 d |
	\break
	g8\4 g16\4 e g\4 g8\4 bes16 ~ bes4 r8 r16 d |
	g8\4 g16\4 e g\4 g8\4 e16 d4 r |
  %}
  \break

  cis'8 cis'16 cis' cis'2 r8 r16 a |
  c'16 c' c'8 bes8 bes16 a ~ a4 r8 r16 a |
  c'8 c'16 bes ~ bes bes bes a ~ a4 d16 e fis g\4 ~ |
  g2\4 r |
  %\break
  b8 b ~ b4 g8\4 g\4 ~ g4\4 |

  %R1*11 |
  R1*1 |
  r2 r4 r8 r16 e16 |
  cis'8 cis'16 a cis'8 cis'16 a cis' cis' cis' a cis' cis' a cis' ~ |
  cis'2 r8 cis'16 a cis'8 cis'16 a |
  b16 a b8 ~ b4 a4 r8 r16 cis16|
  cis'16 cis' cis'8 ~ cis'8. a16 b b b cis' ~ cis' b8 cis16 |
  cis'16 cis' cis'8 ~ cis'8. a16 a a a b ~ b a8 a16 |
  c'16 c' c'8 c'8. a16 a a a b ~ b a8. |
  a2 fis16 e8. r4 |
  c'4. c'8 b4. b8 |
  a4 r4 r2 |
  c'4. c'8 b4. b8 |
  a4 r4 r2 |
  \break
  r2 r4 r8 r16 g\4 |
  bes8 bes16 g\4 bes8 a16 g\4 bes bes a g\4 bes bes a g\4 |
  g8\4 bes a4 g8\4 bes16 a ~ a4 |
  \time 2/4
  r4 r8 r16 d |
  \time 4/4
  bes8 bes16 bes a8 g16\4 bes ~ bes bes bes a ~ a g\4 g\4 g\4 ~
  g4\4 ~ g16\4 e d e ~ e4 r8 r16\fermata d |
  \tempo "Andante" 4 = 80 % checkthis⚙
  e8 g\4 ~ g8.\4 e16 g\4 g\4 g\4 bes ~ bes8. e16 |
  g16\4 g\4 g\4 e ~ e d d8 g16\4 g\4 g\4 e d4 |
  d8 d ~ d4 fis8 fis ~ fis4 |
  a8 a ~ a4 c'8 c' ~ c'4 |
  d'1 |
  \bar "|."
}

\include "../muziko.ly"
