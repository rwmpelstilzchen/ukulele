\include "../ĉiea.ly"

\header {
  titolo-xx     = ""
  titolo-he     = "ערב מול הגלעד"
  titolo-eo     = "Vespero antaŭ Gilead"
  komponisto-xx = ""
  komponisto-he = "מיקי גבריאלוב"
  komponisto-eo = "Miki Gavrielov"
  ikono         = "🐑"
}

\include "../titolo.ly"

melodio = {
  \time 4/4
  \key f \minor
  \partial 8*3 c8 f aes\4 |
  g4\4 f8 g\4 ~ g\4 f4 c8 ~ |
  c2 r8 f g\4 aes\4 |
  bes4 c'8 des' ~ des' ees'4 c'8 ~ |
  c'2 r8 c f aes\4 |
  g4\4 f8 g\4 ~ g\4 f4 des8 ~ |
  des2 r8 c f aes\4 |
  g4\4 bes8 aes\4 ~ aes\4 g4\4 f8 ~ |
  f2_\markup{\italic{Fine}} r8 aes\4 bes c' |
  \break des'4 c'8 bes ~ bes aes4\4 g8\4 ~ |
  g2\4 r8 g\4 aes\4 bes |
  c'4 c'8 ees' ~ ees' c'4 bes8 ~ |
  bes2 r8 aes\4 g\4 f |
  aes2\4 r8 aes\4 bes des' |
  c'2 r8 aes\4 g\4 f |
  g4\4 bes8 aes\4 ~ aes4\4 g8\4 f ~ |
  f2 r4 r8 f\3^"V" |
  f'4 f'8 ees' ~ ees'4 c'8\2 d' ~ |
  d'4 d'8 c'\2 ~ c'4 r8 f\3 |
  f'4^"VI" f'8 ees' ~ ees' c'4\2 des'8\4 ~ |
  des'\2 ees'4 c'8\2 ~ c'4\2 r8 c'\2 |
  ees'4 ees'8 ees' ~ ees'4 des'8 c' ~ |
  c' bes4 aes8\4 ~ aes4\4 r8 bes |
  c'4 des'8 c' ~ c'4 bes8 c' ~ |
  c'4 des'8 c' ~ c'2_\markup{\italic{D.C. al fine}} |
  \bar "|."
}

gmelodio = {
   \tempo 4=120
   \key aes \major
   \time 4/4
   \oneVoice
   <c\3>8 <f\2>8 <aes\4>8 <g\4>4 <f\2>8 <g\4>4 
   <f\2>4 <c\3>2. 
   <f\2>8 <g\4>8 <aes\4>8 <bes\1>4 <c'\1>8 <des'\1>4 
   <bes\1>4 <c'\1>2. 
   <c\3>8 <f\2>8 <aes\4>8 <g\4>4 <f\2>8 <g\4>4 
   <f\2>4 <c\3>2. 
   <c\3>8 <f\2>8 <aes\4>8 <g\4>4 <bes\1>8 <aes\4>4 
   <g\4>4 <f\2>2. 
   <aes\4>8 <bes\1>8 <c'\1>8 <des'\1>4 <c'\1>8 <bes\1>4 
   <aes\4>4 <g\4>2. 
   <g\4>8 <aes\4>8 <bes\1>8 <c'\1>4 <c'\1>8 <ees'\1>4 
   <c'\1>4 <bes\1>2. 
   <aes\4>8 <g\4>8 <f\2>8 <aes\4>2 <aes\4>8 
   <bes\1>8 <des'\1>8 <c'\1>2. 
   <aes\4>8 <g\4>8 <f\2>8 <g\4>4 <bes\1>8 <aes\4>4 
   <g\4>4 <f\2>2. 
   <f\2>8 <f'\1>4 <f'\1>8 <ees'\1>4 <c'\1>4 
   <des'\1>4 <des'\1>4 <c'\1>2 
   <f\2>8 <f'\1>4 <f'\1>8 <ees'\1>4 <c'\1>4 
   <des'\1>4 <ees'\1>4 <c'\1>2 
   <c'\1>8 <ees'\1>4 <ees'\1>8 <ees'\1>4 <des'\1>4 
   <c'\1>4 <bes\1>4 <aes\4>2 
   <f\2>8 <c'\1>4 <des'\1>8 <c'\1>2 
   <bes\1>8 <c'\1>4 <des'\1>8 <c'\1>2 
   <c\3>8 <f\2>8 <aes\4>8 <g\4>4 <f\2>8 <g\4>4 
   <f\2>4 <c\3>2. 
   <f\2>8 <g\4>8 <aes\4>8 <bes\1>4 <c'\1>8 <des'\1>4 
   <bes\1>4 <c'\1>2. 
   <c\3>8 <f\2>8 <aes\4>8 <g\4>4 <f\2>8 <g\4>4 
   <f\2>4 <c\3>2. 
   <c\3>8 <f\2>8 <aes\4>8 <g\4>4 <bes\1>8 <aes\4>4 
   <g\4>4 <f\2>2.
   \bar "|."
}

\include "../muziko.ly"
