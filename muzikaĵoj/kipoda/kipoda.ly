\include "../ĉiea.ly"

\header {
  titolo-xx     = ""
  titolo-he     = "ענן על מקל: קיפודה"
  titolo-eo     = "Nubo sur paliso: Histriko"
  komponisto-xx = ""
  komponisto-he = "דידי שחר"
  komponisto-eo = "Didi Ŝaĥar"
  ikono         = "✏"
}

\include "../titolo.ly"

melodio = {
  \key c \major
  \partial 2 c8 e4. |
  c8 d4. c8 e4. |
  d8 e8 d8 e8 c4. c8 |
  d4. c16 d16 e2 |
  d8 e8 d8 e8 c2 |
  d2 e2 |
  d8 e8 d8 e8 c2 |
  d2 c2 |
  \break
  g8\4 g8\4 g8\4 g8\4 g8\4 g8\4 g4\4 |
  f4 e4 f2 |
  c8 c8 e8 c8 d2 |
  c8 d8 e4 d4 c4 ~ |
  c2 c8 f8 e8 c8 |
  d1 |
  \break
  \repeat volta 2 {
    g8\4 g8\4 g8\4 g8\4 g8\4 g4.\4 |
    c8 c8 c8 f8 e8 c8 d4 ~ |
    d4 c8 c8 d8 e8 d4 ~ |
    d8 c8 c8 c8 f4 e4 |
    d4 c2. |
  }
  \alternative {
    {c8 f8 e8 c8 d2 |}
    {c8 f8 e8 d8 c2 |}
  }
  \bar "|."
}

\include "../muziko.ly"
