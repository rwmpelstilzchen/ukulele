\include "../ĉiea.ly"

\header {
  titolo-xx     = "Early One Morning"
  titolo-he     = "מוקדם בבוקר"
  titolo-eo     = "Frumatene"
  komponisto-xx = ""
  komponisto-he = ""
  komponisto-eo = ""
  ikono         = "🌅"
}

\include "../titolo.ly"

melodio = {
  \time 2/4
  \key d \major
  d d8 d |
  d fis a a |
  b g\4 e d |
  cis e cis4 |
  \break
  d4 d8 d |
  d fis a a |
  b g\4 e cis |
  d2
  \break
  \repeat volta 2 {
    e4 fis8 g\4 |
    a fis d4 |
    e fis8 g\4 |
    a fis d4 |
	\break
    d8 fis a d' |
    cis' b a g\4 |
    fis e d cis |
    d2 |
  } 
}

\include "../muziko.ly"
