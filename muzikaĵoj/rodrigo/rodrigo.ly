\include "../ĉiea.ly"

\header {
  titolo-xx     = ""
  titolo-he     = "רודריגו מרטינז"
  titolo-eo     = "Rodrigo Martinez"
  komponisto-xx = ""
  komponisto-he = ""
  komponisto-eo = ""
  ikono         = "🐄"
}

\include "../titolo.ly"

%⚙להוסיף מקצב עם DrumStaff
%⚙נכון לקרוא לזה „אוסטינטו” (ostinato)?

t = {
  d4 cis8 d8 e4 |
  f4 f4 f8 f8 |
  e4 e8 d4 d8 |
  cis4 r4 cis4 |
  d4 cis8 d8 e4 |
  f4 f4 f4 |
  e4 d8 d4 cis8 |
  d2. |
}

BA = {
  d4 e8 f8 g4\4 |
  a4 a4 a8 a8 |
  g4\4 g8\4 f4 d8 |
  e4 r4 e4 |
  f4 e8 f8 g4\4 |
  a4 a4 a4 |
  g4\4 f8 d8 e4 |
  d2 r4 |
}

BB = {
  \transpose d d' {
  d4 a,8 d8 c4 |
  f,4 f,4 f,8 f,8 |
  c4 c8 d4 d8 |
  a,4 r4 a,4 |
  d4 a,8 d8 c4 |
  f,4 f,4 f,4 |
  c4 d8 bes,8 a,4 |
  a2 r4 |
}
}

melodio = {
  \key d \dorian
  \time 3/4
  \BA
  \bar "||"
  \break
  \t
  \bar "|."
}



%melodio = {
%  \time 3/4
%  \key d \dorian
%  d4 cis8 d e4 |
%  f4 f f8 f |
%  e4 e8 d4 d8 |
%  cis4 r cis |
%  \break
%  d4 cis8 d e4 |
%  f4 f f |
%  e4 d8 d4 cis8 |
%  d2 r4 |
%  \bar "|."
%}

\include "../muziko.ly"
