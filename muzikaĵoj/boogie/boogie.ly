\include "../ĉiea.ly"

\header {
  titolo-xx     = "Hokey cokey"
  titolo-he     = "אוגי בוגי"
  titolo-eo     = "Ugobugo"
  komponisto-xx = ""
  komponisto-he = ""
  komponisto-eo = ""
  ikono         = "💃"
}

\include "../titolo.ly"

melodio = {
  \key f \major
  \partial 16*5 { c16 d8. c16 |}
  f4 f8. f4 c16 d8. c16 |
  f4 f8. f4 c16 d8. c16 |
  f4 f4 f4 d8. c16 |
  e8. dis16 e8. dis16 e4. ~ e16 c16 |
  %e8. dis16 e8. dis16 e8. dis16 e8. dis16 |
  %e8. dis16 e8. dis16 e2 |
  e8. dis16 e8. dis16 e8. g16\4 c8. c16 |
  e4 g4\4 g2\4 |
  c8 c4 c8 d4 e4 |
  f1 |
  \bar "|."
}

\include "../muziko.ly"
