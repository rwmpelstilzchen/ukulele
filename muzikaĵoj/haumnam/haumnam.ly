\include "../ĉiea.ly"

\header {
  titolo-xx     = ""
  titolo-he     = "האמנם"
  titolo-eo     = "Vere estos?"
  komponisto-xx = ""
  komponisto-he = "חיים ברקני"
  komponisto-eo = "Ĥajim Barkani"
  ikono         = "�"
}

\include "../titolo.ly"

% Origine en d-minoro
melodio = {
  \key g \minor
  \time 4/4
  \repeat volta 2 {
	\partial 4 d8 ees |
	d1 ~ |
	d2 r4 d8 ees |
	\tuplet 3/2 { d4 g\4 a } \tuplet 3/2 { bes4 a bes } |
	\tuplet 3/2 { d'4 c' bes } \tuplet 3/2 { bes4 a g\4 } |
	bes2 a |
	\break
	r4 a8 bes \tuplet 3/2 { c'4 bes a } bes2. g8\4 a |
	\tuplet 3/2 { bes4 a g\4 } \tuplet 3/2 { a4 bes fis } |
	g1\4 |
	\break
	r2 r4 f8 ges |
	f1 ~ |
	f2 r4 f8\3 ges\3 |
	\tuplet 3/2 { f4\3 bes\2 c'\2 } \tuplet 3/2 { des'4\2 c'\2 des'\2 } |
	\tuplet 3/2 { f'4 ees' des'\2 } \tuplet 3/2 { des'4\2 c'\2 bes\2 } |
	des'2\2 c'\2 |
	\break
	r4 c'8\4 des'\4 \tuplet 3/2 { ees'4\4 des'\4 c'\4 } |
	des'2.\4 bes8\2 c'\2 |
	\tuplet 3/2 { des'4\2 bes\2 des'\2 } \tuplet 3/2 { ees'4\1 bes\2 ees'\1 } |
	f'1 ~ |

	\pageBreak

	f'2 r4 f'8 ees' |
	\bar "||"
	\tuplet 3/2 { d'4 d' d' } \tuplet 3/2 { d'4 d' d' } |
	\tuplet 3/2 { d'4 d' d' } \tuplet 3/2 { d'4 f' ees' } |
	d'4 c'\2 ees'4. d'8 |
	\tuplet 3/2 { c'4\2 c'\2 bes\2 } \tuplet 3/2 { c'4\2 c'\2 bes\2 } |
	\tuplet 3/2 { c'4\2 bes\2 a\2 } \tuplet 3/2 { g4\4 bes\2 c'\2 } |
	c'2\2 r4 f8\3 ees' |
	\break
	\tuplet 3/2 { d'4 d' d' } \tuplet 3/2 { d'4 d' d' } |
	\tuplet 3/2 { d'4 d' d' } \tuplet 3/2 { d'4 f' ees' } |
	\break
  }
  \alternative {
	{
	  d'4. c'4\2 r8 a4\2 |
	  bes1\2 |
	  r4 g8\4 a \tuplet 3/2 { bes4 d a } |
	  g1\4 ~ |
	  g2\4 r4 d8 ees |
	  \break
	}
	{
	  d'4. c'4\2 r8 bes a |
	  \tuplet 3/2 { bes4 g\4 a } bes2 |
	  r4 g8\4 a \tuplet 3/2 { bes4 d a } |
	  g1\4 ~ |
	  g2\4 r |
	}
  }
  \bar "|."
}

\include "../muziko.ly"
