\include "../ĉiea.ly"

#(set-global-staff-size 24)

\header {
  titolo-xx     = "Super Mario World"
  titolo-he     = "העולם של סופר מריו"
  titolo-eo     = "La mondo de Super Mario"
  komponisto-xx = "近藤浩治"
  komponisto-he = "קוג׳י קונדו"
  komponisto-eo = "Kōĝi Kondō"
  ikono         = "🥚"
}

\include "../titolo.ly"

melodio = {
  \key f \major
  \time 4/4
  d' bes8. f16 e8 f16 g\4 ~ g4\4 |
  %< f a >8 \tuplet 3/2 { < f a >8 < f a >16 } < f a >8 \tuplet 3/2 { < f a >8 < f a >16 } < g bes >8 \tuplet 3/2 { < g bes >8 < g bes >16 } < ges bes >8 \tuplet 3/2 { < ges bes >8 < ges bes >16 } |
  %< f a >8 \tuplet 3/2 { < f a >8 < f a >16 } < f a >8 \tuplet 3/2 { < f a >8 < f a >16 } < g bes >8 < ges bes >4. |
  f8^\segno \tuplet 3/2 { f8 f16 } f8 \tuplet 3/2 { f8 f16 } g8\4 \tuplet 3/2 { g8 g16 } ges8 \tuplet 3/2 { ges8 ges16 } |
  f8 \tuplet 3/2 { f8 f16 } f8 \tuplet 3/2 { f8 f16 } g8 ges4. |
  %a8 \tuplet 3/2 { a8 a16 } a8 \tuplet 3/2 { a8 a16 } bes8 \tuplet 3/2 { bes8 bes16 } bes8 \tuplet 3/2 { bes8 bes16 } |
  %a8 \tuplet 3/2 { a8 a16 } a8 \tuplet 3/2 { a8 a16 } bes8 bes4. |
  \break
  \repeat volta 2 {
	a4 f8. c16 d f8 f8. r16 d |
	c8 f f c' a8. g16\4 ~ g4\4 |
	a f8. c16 d f8 f8. r16 d |
	c8 f bes16 a g\4 f ~ f2 |
  }
  \break
  a8. f c8 a8. f16 ~ f4 |
  aes16\4 f c8 aes8.\4 g16\4 ~ g2\4 |
  a8. f c8 a8. f16 ~ f4 |
  aes16\4 f c8 c'2. |
  \break
  a4 f8. c16 d f8 f8. r16 g\4 |
  a f c8 d8. f2 d16 |
  c'8 d' c' d' c'4 bes16 a g8\4 |
  f1 |
  \break
  %< d f >16 < bes, d >8 < d f >8. < e g >8 < f a >16 < e gis > < e g > < d fis >4 r16 |
  %< d f > < bes, d >8 < d f >8. < e g >8 < f a >2 |
  %< d f >16 < bes, d >8 < d f >8. < e g >8 < f a >16 < g bes > < a c' > < bes d' >4 r16 |
  %< d f > < bes, d >8 < d f >8. < e g >8 < c f >2
  f16 d8 f8. g8\4 a16 gis g\4 fis4 r16 |
  f d8 f8. g8\4 a2 |
  f16 d8 f8. g8\4 a16 bes c' d'4 r16 |
  f d8 f8. g8\4 f2_\markup{\italic{D.S.}} |
  %d16 bes,8 d8. e8 f16 e e d4 r16 |
  %d bes,8 d8. e8 f2 |
  %d16 bes,8 d8. e8 f16 g a bes4 r16 |
  %d bes,8 d8. e8 c2
  \bar "|."
} 

\include "../muziko.ly"
