\include "../ĉiea.ly"

\header {
  titolo-xx     = ""
  titolo-he     = "עץ־הכוכבים"
  titolo-eo     = "Stelarbo"
  komponisto-xx = ""
  komponisto-he = "נורית הירש"
  komponisto-eo = "Nurit Hirŝ"
  ikono         = "✨"
}

\include "../titolo.ly"

melodio = {
  \time 3/4
  \key f \minor
  \tupletUp

  \repeat volta 2 {
	\tuplet 3/2 { f4 g8\4 } \tuplet 3/2 { aes4 bes8 } \tuplet 3/2 { aes4 g8\4 } |
	f4 r \tuplet 3/2 { r4 f8 } |
	\tuplet 3/2 { c4 f8 } g4\4 aes |
	g\4 f r |
	\break
	\tuplet 3/2 { f4 g8\4 } \tuplet 3/2 { aes4 bes8 } \tuplet 3/2 { aes4 g8\4 } |
	f4 r r |
	\tuplet 3/2 { ees4 f8 } g4\4 ees |
	c' c' r |
	\break
	\tuplet 3/2 { a4 bes8 } \tuplet 3/2 { c'4 ees'8 } \tuplet 3/2 { des'4 c'8 } bes4 r r |
	\tuplet 3/2 { g4\4 bes8 } ees'4 bes |
	c' c' r |
	\break
	\tuplet 3/2 { g4\4 aes8 } \tuplet 3/2 { bes4\2 g8\4 } \tuplet 3/2 { bes4\2 des'8 } |
	f'4 r r |
	\tuplet 3/2 { e'4 f'8 } g'4 d'\2 |
	e' c'\2 r |
	\break
	\interim
	\tuplet 3/2 { r4 f8\3 } \tuplet 3/2 { a4 c'8\2 } \tuplet 3/2 { a4 f8\3 } |
	\tuplet 3/2 { r4 bes8\2} \tuplet 3/2 { des'4 f'8 } r4 |
	\tuplet 3/2 { r4 ees8 } \tuplet 3/2 { g4\4 bes8 } \tuplet 3/2 { g4\4 ees8 } |
	\tuplet 3/2 { r4 aes8 } \tuplet 3/2 { c'4 ees'8 } r4 |
	\tuplet 3/2 { r4 des'8 ~ } des'4 \tuplet 3/2 { c'4 bes8 } |
	\tuplet 3/2 { r4 aes8 ~ } aes4 g\4 |
	\deinterim
  }
  \break

  \repeat volta 2 {
	\tuplet 3/2 { f4 g8\4 } \tuplet 3/2 { aes4 bes8 } \tuplet 3/2 { aes4 g8\4 } |
	f4 r \tuplet 3/2 { r4 f8 } |
	\tuplet 3/2 { c4 f8 } g4\4 aes |
	g\4 f r |
	\break
	\tuplet 3/2 { f4 g8\4 } \tuplet 3/2 { aes4 bes8 } \tuplet 3/2 { aes4 g8\4 } |
	f4 r r |
	\tuplet 3/2 { c4 f8 } g4\4 aes |
	g\4 f r^"ad lib." |
  }
}



miamelodio = { % משמעיה
  \time 6/8
  \key d \minor
  \repeat volta 4 {
	d8 e f g f e |
	d2 c8 d |
	e4. f8 e4 |
	d2. |
	\break d8 e f g f e |
	d2 d8 e |
	f4. c8 bes4 |
	a2. |
	\break fis8 g a c' bes a |
	g2\4 e8 g |
	c'4. bes8 bes4 |
	a2. |
	\break g8 e g bes cis' d' |
	e'2 cis'8 d' |
	e'4. b8 cis'4 |
	a2. |
  }
}

\include "../muziko.ly"
