\include "../ĉiea.ly"

#(set-global-staff-size 27)

\header {
  titolo-xx     = "Morgenstemning"
  titolo-he     = "נעימת בוקר"
  titolo-eo     = "Matena humoro"
  komponisto-xx = ""
  komponisto-he = "אדוורד גריג"
  komponisto-eo = "Edvard Grieg"
  ikono         = "🌅"
}

\include "../titolo.ly"

melodio = {
  \key f \major
  \time 6/8
  \tempo "Allegretto pastorale" 4 = 90

  c'8( a g\4 f g\4 a) |
  c'( \acciaccatura { a16 bes } a8 g\4 f g16\4 a g\4 a) |
  \acciaccatura { a( bes } c'8)( a c') d'( a d') |
  c'( a g\4 f4) r8 |
  \break
  c'( a g\4 f g\4 a)  |
  c'( \acciaccatura { a16 bes } a8 g\4 f g16\4 a g\4 a) |
  \acciaccatura { a( bes } c'8)( a c' d' a d') |
  e'( cis' b a4.) |
  \break

  %\set TabStaff.minimumFret = #10
  e'8( cis'\2 b\2 a\3 b\2 cis'\2) |
  e'( \acciaccatura { cis'16\2 d'\2 } cis'8\2 b\2 a\3 b16\2 cis'\2 b\2 cis'\2)  |
  \acciaccatura { cis'\2( d'\2 } e'8)( cis'\2 e') f'( cis'\2 f') |
  e'( cis'\2 b\2 a4) r8 |
  \break
  %e'( cis' b a b cis') |
  %e'( \acciaccatura { cis'16 d' } cis'8 b a b16 cis' b cis') |
  e'8( cis'\2 b\2 a\3 b\2 cis'\2) |
  e'( \acciaccatura { cis'16\2 d'\2 } cis'8\2 b\2 a\3 b16\2 cis'\2 b\2 cis'\2)  |
  \acciaccatura { cis'\2( d'\2 } e'8)( cis'\2 e' f' d'\2 f')  |
  g'( e' d'\2 c'4.\2) |
  \break

  \set TabStaff.minimumFret = #0
  g'8( e' d' c'\2 d'16 e' d' e') |
  g'8( e' d' c'\2 d'16 e' d' e') |
  g'8( e' c'\2) g\4 e c |
  g'( e' c'\2) g\4 e c |
  \break

  c'(-> a g\4 f g\4 a)  |
  c'(-> \acciaccatura { a16 bes } a8 g\4 f g16\4 a g\4 a) |
  \acciaccatura { a( bes } c'8)(-> a c') d'(-> a d') |
  d'(-> bes a g\4) r r |
  \break
  d'(-> bes a g\4 a bes) |
  d'(-> \acciaccatura { bes16 c' } bes8 a g\4 a16 bes a bes)  |
  \acciaccatura { bes( c' } d'8)(-> bes d') e'(-> bes\2 e') |
  \acciaccatura { e'16( f' } e'8)(-> c'\2 e') f'(-> c'\2 f') |
  \break
  des'( bes aes ges aes bes) |
  des'( bes aes ges aes16 bes aes bes) |
  des'8( bes des' ees' bes ees' |
  des'2. ~  |
  \break
  des'8) r r r4 r8 |
  R2. |
  \break
  c'8( a g\4 f g\4 a) |
  c'( a g\4 f g16\4 a g\4 a) |
  c'8( a c' d' a d' |
  c'4. ~ c'4) r8 |
  \break
  c'(-> a d' c'4) r8  |
  c'(-> a d' c'4) r8 |
  d'2.(-> |
  a4. d') |
  \break
  c'8( a g\4 f g\4 a) |
  \tuplet 3/2 { d'32(\trill c' d' } c'16 \acciaccatura { a bes } a8 g\4 f g16\4 a g\4 a) |
  c'8( a c' d' b d') |
  f'32\trill e' f' e' f' e' f' e' f' e' f' e' f' e' ~ e'8. d'16 e' |
  f'4 r8 des'32\trill-> ces' des' ces' des' ces' bes ces' r8  |
  des'32\trill-> ces' des' ces' des' ces' bes ces' r8 des'32\trill-> ces' des' ces' des' ces' bes ces' r8 |
  c'( a c' d' b d') |
  f'32\trill e' f' e' f' e' f' e' f' e' f' e' f' e' ~ e'8. d'16 e'  |
  d''32\trill-> c'' d'' c'' d'' c'' b' c'' r8 d''32\trill-> c'' d'' c'' d'' c'' b' c'' r8 |
  f'32\trill e' f' e' f' e' f' e' f' e' f' e' f' e' ~ e'8. d'16 e'  |
  d'32\trill-> c' d' c' d' c' ces' c' r8 f32\3\trill e\3 f\3 e\3 f\3 e\3 ~ e16\3 d\3 e\3 |
  d'32\trill-> c' d' c' d' c' ces' c' r8 f32\3\trill e\3 f\3 e\3 f\3 e\3 ~ e16\3 d\3 e\3 |
  R2.*2 |
  \break
  c'8( a g\4 f g\4 a) |
  c'( a g\4 f g16\4 a g\4 a) |
  R2.*2 |
  c'2. |
  \override TextSpanner.bound-details.left.text = "poco rit."
  d'\startTextSpan |
  f' ~ |
  f' ~ |
  f'4 r8 r4 r8 \stopTextSpan \bar "|."
}

origmelodio =  \relative c' {
\key f \major \time 6/8 | % 1
  \tempo "" 4=90 | % 1
  c8 ^\markup{ \bold {Allegretto pastorale} } (  a8 g8  f8  g8 a8 )
   | % 2
  c8 (  \acciaccatura { a16  bes16  } a8 g8  f8  g16 a16 g16 a16
  )  | % 3
  \acciaccatura { a16 (  bes16  } c8 ) (  a8 c8 )  d8 (  a8 d8 )
   | % 4
  c8 (  a8 g8  f4 ) r8 | % 5
  c'8 (  a8 g8  f8  g8 a8 )  \break | % 6
  c8 (  \acciaccatura { a16  bes16  } a8 g8  f8  g16 a16 g16 a16
  )  | % 7
  \acciaccatura { a16 (  bes16  } c8 ) (  a8 c8  d8  a8 d8 )  | % 8
  e8 (  cis8 b8  a4. ) | % 9
  e'8 (  cis8 b8  a8  b8 cis8 )  | 
  e8 (  \acciaccatura { cis16  d16  } cis8 b8  a8  b16 cis16 b16
  cis16 )  \break | % 11
  \acciaccatura { cis16 (  d16  } e8 ) (  cis8 e8 )  f8 (  cis8 f8
  )  | % 12
  e8 (  cis8 b8  a4 ) r8 | % 13
  e'8 (  cis8 b8  a8  b8 cis8 )  | % 14
  e8 (  \acciaccatura { cis16  d16  } cis8 b8  a8  b16 cis16 b16
  cis16 )  | % 15
  \acciaccatura { cis16 (  d16  } e8 ) (  cis8 e8  f8  d8 f8 ) 
  \break | % 16
  g8 (  e8 d8  c4. ) | % 17
  g'8 (  e8 d8  c8  d16 e16 d16 e16 )  | % 18
  g8 (  e8 d8  c8  d16 e16 d16 e16 )  | % 19
  g8 (  e8 c8 )  g8  e8 c8  | 
  g''8 (  e8 c8 )  g8  e8 c8  | % 21
  c'8 ( ->  a8 g8  f8  g8 a8 )  \break | % 22
  c8 ( ->  \acciaccatura { a16  bes16  } a8 g8  f8  g16 a16 g16 a16
  )  | % 23
  \acciaccatura { a16 (  bes16  } c8 ) ( ->  a8 c8 )  d8 ( ->  a8
  d8 )  | % 24
  d8 ( ->  bes8 a8  g8 ) r8 r8 | % 25
  d'8 ( ->  bes8 a8  g8  a8 bes8 )  | % 26
  d8 ( ->  \acciaccatura { bes16  c16  } bes8 a8  g8  a16 bes16 a16
  bes16 )  \break | % 27
  \acciaccatura { bes16 (  c16  } d8 ) ( ->  bes8 d8 )  e8 ( -> 
  bes8 e8 )  | % 28
  \acciaccatura { e16 (  f16  } e8 ) ( ->  c8 e8 )  f8 ( ->  c8 f8
  )  | % 29
  des8 (  bes8 as8  ges8  as8 bes8 )  | 
  des8 (  bes8 as8  ges8  as16 bes16 as16 bes16 )  | % 31
  des8 (  bes8 des8  es8  bes8 es8  | % 32
  des2. ~ \break | % 33
  des8 ) r8 r8 r4 r8 | % 34
  R2. | % 35
  c8 (  a8 g8  f8  g8 a8 )  | % 36
  c8 (  a8 g8  f8  g16 a16 g16 a16 )  | % 37
  c8 (  a8 c8  d8  a8 d8  | % 38
  c4. ~ c4 ) r8 | % 39
  c8 ( ->  a8 d8  c4 ) r8 \break | 
  c8 ( ->  a8 d8  c4 ) r8 | % 41
  d2. ( -> | % 42
  a4. d4. ) | % 43
  c8 (  a8 g8  f8  g8 a8 )  | % 44
  \times 2/3  {
	d32 ( \trill  c32 d32 }
	c16 \acciaccatura { a16  bes16  } a8 g8  f8  g16 a16 g16 a16 ) 
	| % 45
	c8 (  a8 c8  d8  b8 d8 )   | % 46
	f32 \trill  e32 f32 e32 f32 e32 f32 e32 f32 e32 f32 e32 f32 e32 ~ e8.
	 d16  e16  | % 47
	f4 r8 des32 \trill ->  ces32 des32 ces32 des32 ces32 bes32 ces32 
	r8 \break | % 48
	des32 \trill ->  ces32 des32 ces32 des32 ces32 bes32 ces32  r8 des32
	\trill ->  ces32 des32 ces32 des32 ces32 bes32 ces32  r8 | % 49
	c8 (  a8 c8  d8  b8 d8 )  | 
	f32 \trill  e32 f32 e32 f32 e32 f32 e32 f32 e32 f32 e32 f32 e32 ~ e8.
	 d16  e16  \break | % 51
	d'32 \trill ->  c32 d32 c32 d32 c32 b32 c32  r8 d32 \trill ->  c32
	d32 c32 d32 c32 b32 c32  r8 | % 52
	f,32 \trill  e32 f32 e32 f32 e32 f32 e32 f32 e32 f32 e32 f32 e32 ~
	e8.  d16  e16  \break | % 53
	d32 \trill ->  c32 d32 c32 d32 c32 ces32 c32  r8 f,32 \trill  e32
	f32 e32 f32 e32 ~ e16  d16  e16  | % 54
	d'32 \trill ->  c32 d32 c32 d32 c32 ces32 c32  r8 f,32 \trill  e32
	f32 e32 f32 e32 ~ e16  d16  e16  | % 55
	R2.*2  | % 57
	c'8 (  a8 g8  f8  g8 a8 )  | % 58
	c8 (  a8 g8  f8  g16 a16 g16 a16 )  | % 59
	R2.*2  | % 61
	c2. | % 62
	d2. ^\markup{ \bold\italic {poco rit.} } | % 63
	f2. ~ | % 64
	f2. ~ | % 65
	f4 r8 r4 r8 \bar "|."
  }

\include "../muziko.ly"
