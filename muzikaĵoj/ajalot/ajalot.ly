\include "../ĉiea.ly"

#(set-global-staff-size 26)

\header {
  titolo-xx     = ""
  titolo-he     = "מה עושות האיילות בלילות?"
  titolo-eo     = "Kion faras la cervoj dumnokte?"
  komponisto-xx = ""
  komponisto-he = "יוני רכטר"
  komponisto-eo = "Joni Reĥter"
  ikono         = "𓃴"
}

\include "../titolo.ly"

melodio = {
  \time 4/4
  %\key b \major
  \repeat volta 2 {
    r8 d b e ~ e4 r |
    r8 c e g\4 b4 b8 a |
    g4\4 d8 b e4 r |
    r8 c dis g\4 d'4 d'8 c' |
    eis4 eis8 cisis' gis4 r |
    r8 cis eis gis bis4 bis8 ais |
    dis4 dis8 bis e4 r |
    r8 c e g\4 b4 b8 a |
    g2\4 r |
  }
  \alternative {
	{ r1 | }
	{
	  \time 7/8
	  e'4 d'8 c'\2 ~ c'4\2 bes8\2 |
	}
  }
  \time 4/4
  a g\4 a4 r a8 a |
  \repeat volta 2 {
    d'4 a8 f a8. d16 d8 d |
    d' d' a f a4 a8 a |
    cisis' eis' cisis' bis a f g\4 d |
    f2 r4 f8 g\4 |
    a a a a g\4 fis e d |
    e2 r |
	\interim
    r8 a fis' b\2 ~ b2\2 |
    r8 g\4 b\2 d'\2 fis'4 fis'8 e' |
    r a fis' b\2 ~ b2\2 |
    r8 g\4 bes\3 d'\2 a'4 a'8 g'\1 |
    c'\3 c'\3 a' dis'\2 ~ dis'2\2 |
    r8 gis\3 bis\2 dis'\2 fisis'4 fisis'8 eis' |
    r ais\3 fisis' b\3 ~ b2\3 |
    r8 g\4 b\2 d'\2 fis'4 fis'8 e' |
    d'2\2 r |
    r1 |
	\deinterim
    \time 7/8
    e'4 d'8 c'\2 ~ c'4\2 bes8\2 |
    \time 4/4
    a g\4 a4 r a8 a |
   
  }
  e2 r |
  r8 d b e ~ e4 r |
  r8 c e g\4 b b b a |
  g4\4 d8 b e4 r |
  r8 c ees g\4 d' d' d' c' |
  eis4 eis8 fisis gis ais ~ ais4 |
  r eis8 fisis gis gis ais gis |
  bis2. bis8 cisis' |
  \repeat volta 2 {
    dis' dis' dis' dis' ~ dis' cisis' bis ais |
    cis' cis' cis' cis' ~ cis' bis ais gis |
    bis bis bis bis b2 |
    \time 2/4
    r4 r8 dis |
    \time 4/4
    dis' dis' dis' dis' ~ dis' cisis' bis ais |
    cis' cis' cis' cis' ~ cis' bis ais gis |
    bis bis bis bis b2 |
    r r4 r8 dis |
	\mark\markup{"ad lib."}
  } 
}

rechterbookmelodio = {
%  d8 b e4. d8 e g |
%  b b b a g2 |
%  d8 b e4. c8 dis g |
%  d' d' d' c' f2 |

  %des'8 bes'8 e'4. des'8 bes' des'
  %a des'4 e'8 aes'8 aes' ges' b b aes' c'
  %aes c' ees' g'8 g' f' ees'2

  \displayLilyMusic \transpose aes, c {
  \time 4/4
  \key ees \major
  \repeat volta 2 {
	r8 bes, g c ~ c4 r4 |
	r8 aes, c ees g4 g8 f |
	ees4 bes,8 g c4 r |
	r8 aes, b, ees bes4 bes8 aes |
	cis4 cis8 ais e4 r |
	r8 a, cis e gis4 gis8 fis |
	b,4 b,8 gis c4 r |
	r8 aes,8 c ees g4 g8 f |
	ees2 r |
  }
  \alternative {
	{ r1 | }
	{ \time 7/8 c'4 bes8 aes ~ aes4 ges8 |}
  }
  \time 4/4
  f8 ees f4 r f8 f |

  \repeat volta 2 {
	bes4 f8 des f8. bes,16 bes,8 bes, |
	bes8 bes f des f4 f8 f |
	ais8 cis' ais gis f des ees bes, |
	des2 r4 des8 ees |
	f8 f f f ees d c bes, |
	c2 r |
	\interim
	r8 f d' g ~ g2 |
	r8 ees g bes d'4 d'8 c' |
	r8 f d' g ~ g2 |
	r8 ees ges bes f'4 f'8 ees' |
	aes8 aes f' b ~ b2 |
	r8 e gis b dis'4 dis'8 cis' |
	r8 fis dis' g ~ g2 |
	r8 ees8 g bes d'4 d'8 c' |
	bes2 r |
	r1 |
	\deinterim
	\time 7/8
	c'4 bes8 aes ~ aes4 ges8 |
	\time 4/4
	f8 ees f4 r f8 f |
  }
  c2 r |
  r8 bes, g c ~ c4 r |
  r8 aes, c ees g g g f |
  ees4 bes,8 g c4 r |
  r8 aes, ces ees bes bes bes aes |
  cis4 cis8 dis e fis ~ fis4 |
  r4 cis8 dis e e fis e |
  gis2. gis8 ais |
  \repeat volta 2 {
	b8 b b b ~ b ais gis fis |
	a8 a a a ~ a gis8 fis e |
	gis8 gis gis gis g2 |
	\time 2/4
	r4 r8 b,8 |
	\time 4/4
	b8 b b b ~ b ais gis fis |
	a8 a a a ~ a gis8 fis e |
	gis8 gis gis gis g2 |
	r2 r4 r8 b,8
  }
}
}

\include "../muziko.ly"
