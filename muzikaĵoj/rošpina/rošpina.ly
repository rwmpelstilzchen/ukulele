\include "../ĉiea.ly"

\header {
  titolo-xx     = ""
  titolo-he     = "ילדת טבע: שקדיה"
  titolo-eo     = "Naturido: Migdalarbo"
  komponisto-xx = ""
  komponisto-he = "עובד אפרת"
  komponisto-eo = "Oved Efrat"
  ikono         = "川"
}

\include "../titolo.ly"


melodio = {
  \key c \major
  \interim 
  e8 e e e ~ e2 |
  f8 f f f ~ f2 |
  ges8 ges ges ges ~ ges2 |
  g8\4 g\4 a b ~ b2 |
  \deinterim
  \break
  r4 c c8 e4. |
  c'8 a ~ a4 a8 a g4\4 |
  f8 e f2. |
  %\break
  r4 c8 c e c' a4 ~ |
  a a8 a g4\4 f8 e |
  f1 |
  \break
  r4 a8 a e d c4 ~ |
  c8 e e f e4 f8 e |
  d1 |
  %\break
  r4 c8 c e c' a4 ~ |
  a a8 a g4\4 f8 e |
  f1 |
  \break
  c8 e e e e4 c |
  c8 f f f f4 c |
  c8 ges ges ges ges4 c |
  c8 g\4 g\4 a b4 g\4 ~ |
  \break
  g8\4 c g\4 e d c4. |
  c8 c c c d e4. |
  e8 e g\4 e d c4. |
  c8 c c c d e4. |
  \break
  g8\4 g\4 g\4 e g\4 a4. |
  a8 a a e a g4.\4 |
  g8\4 g\4 g\4 e g\4 d4. |
  d8 d d c d g4.\4 |
  \break
  R1 |
  r4 c c8 e4. |
  c'8 a ~ a4 a8 a g4\4 |
  f8 e f2. |
  \break
  r4 c8 c e c' a4 ~ |
  \override TextSpanner.bound-details.left.text = #"rit."
  a\startTextSpan \tempo 4 = 100 a8 b \tempo 4 = 80 a \tempo 4 = 60 b \tempo 4 = 40 d' c' ~ |
  c'1\stopTextSpan |
  \bar "|."
}

Amelodio = \displayLilyMusic\transpose a c{
  \key a \major
  \interim
  \transpose a a' {
	cis8 cis cis cis8 ~ cis2 |
	d8 d d d ~ d2 |
	ees8 ees ees ees ~ ees2 |
	e8 e fis aes ~ aes2 |
  }
  \deinterim
  \break
  r4 a4 a8 cis'4. |
  a'8 fis'8 ~ fis'4 fis'8 fis' e'4 |
  d'8 cis'8 d'2. |
  \break
  r4 a8 a8 cis'8 a'8 fis'4 ~ |
  fis'4 fis'8 fis' e'4 d'8 cis'8 |
  d'1 |
  \break
  r4 fis'8 fis' cis'8 b8 a4 ~ |
  a8 cis'8 cis' d' cis'4 d'8 cis' |
  b1 |
  \break
  r4 a8 a8 cis'8 a'8 fis'4 ~ |
  fis'4 fis'8 fis' e'4 d'8 cis'8 |
  d'1 |
  \break
  a8 cis' cis' cis' cis'4 a4 |
  a8 d' d' d' d'4 a4 |
  a8 ees' ees' ees' ees'4 a4 |
  a8 e' e' fis' aes'4 e'4 ~ |
  \break
  e'8 a8 e' cis' b a4. |
  a8 a a a b cis'4. |
  cis'8 cis' e' cis' b a4. |
  a8 a a a b cis'4. |
  \break
  e'8 e' e' cis' e' fis'4. |
  fis'8 fis' fis' cis' fis' e'4. |
  e'8 e' e' cis' e' b4. |
  b8 b b a b e'4. |
  \break
  R1 |
  r4 a4 a8 cis'4. |
  a'8 fis'8 ~ fis'4 fis'8 fis' e'4 |
  d'8 cis'8 d'2. |
  \break
  r4 a8 a8 cis'8 a'8 fis'4 ~ |
  \override TextSpanner.bound-details.left.text = "rit."
  fis'4 \startTextSpan \tempo 4 = 100 fis'8 aes' \tempo 4 = 80 fis' \tempo 4 = 60 aes' \tempo 4 = 40 b' a'8 ~ |
  a'1\stopTextSpan |
  \bar "|."
}

\include "../muziko.ly"
