\include "../ĉiea.ly"

\header {
  titolo-xx     = ""
  titolo-he     = "אגדת רובין הוד"
  titolo-eo     = "La fabulo de Robin Hood"
  komponisto-xx = ""
  komponisto-he = "אוברי הוג׳ז"
  komponisto-eo = "Aubrey Hodges"
  ikono         = "🏹"
}

\include "../titolo.ly"

melodio = {
  \time 4/4
  \key d \minor

  \tempo "Vivacissimo" 4 = 176
  \partial 4*3 d4 a a4 |
  g8\4 f g\4 a bes4 bes4 |
  a8 g\4 a4 f f |
  g8\4 a g\4 f e d e4 |
  \break
  f8 e d4 a a4 |
  g8\4 f g\4 a bes4 bes4 |
  a8 g\4 a8 g\4 f4 g8\4 f |
  e8 cis8 d2. |
  %a8 g\4 a4 f g\4 |
  %e4 d2. |
  \break
  r4 d'2 a4 |
  d' c'8 bes a g\4 a4 | 
  f8 g\4 a4 f4 f4 |
  g8\4 a  bes8 a g\4 f e4 |
  \break
  f8 e d4 a a4 |
  g8\4 f g\4 a bes4 bes4 |
  a8 g\4 a g\4 f a g\4 f |
  e8 f d2. |
  \break

  \tempo "Andante" 4 = 100
  \repeat volta 2 {
	d4 e d c |
	d1 |
	d4 e f g\4 |
	a2 e2 |
	%\break
	d4 e d c |
	d2. a4 |
	bes4 a bes a |
	bes a g\4 f |
	e f g\4 a |
	g\4 f e f |
  }
  \break

  \tempo "Vivacissimo" 4 = 176
  d8 e f g\4 a4 g8\4 f |
  g2.\4 d8 e |
  f4 d d e8 f |
  e8 f e d cis4 a |
  \break
  d8 e f g\4 a4 g8\4 f |
  g2.\4 d8 e |
  f8 e d4 e c |
  d1 |
  \break
  a4 a a g8\4 f |
  g2.\4 d8 e |
  f4 f f2 |
  e1 |
  \break
  a4 a a g8\4 f |
  g2.\4 d8 e |
  f4 e8 f e4 c |
  d1 |
  \bar "|."
}

\include "../muziko.ly"
