\include "../ĉiea.ly"

\header {
  titolo-xx     = ""
  titolo-he     = "פגישה לאין קץ"
  titolo-eo     = "Senfina kunveno"
  komponisto-xx = ""
  komponisto-he = "נעמי שמר"
  komponisto-eo = "Naomi Ŝemer"
  ikono         = "∞"
}

\include "../titolo.ly"

asheva = {
  %r4 d8 d16 c'8 bes16 a4. |
  %d8 d16 bes8 bes16 a16 g4 |
  a,8 a,16 a8 g16 f8 a8 g8 f16 |
}

israeli = {
  \time 3/4
  r4 d8 d c' bes |
  a2 r8 d |
  d8 bes ~ bes4 bes8 a |
  g2. |
  a,4. a,8 a8 g |
  f4. a8 g f |
  f2 r8 d|
  f4 e8 d e4 |
  r4 d8 d c' bes |
  bes8 a ~ a2 |
  d8 d bes4. a8 |
  g2. |
}

tavel = {
% 
  \tuplet 3/2 {d4 d8} \tuplet 3/2 {c'4 bes8}  a2 |
  \tuplet 3/2 {d8 d8 bes8~} \tuplet 3/2{bes8 bes8 a} g2 |
  \tuplet 3/2 {a,4 a,8} \tuplet 3/2 {a4 g8} f4 \tuplet 3/2 {a8 g f} |
  f4 d f8 \tuplet 3/2 {e8 d e8} |
%  r4 d8 d c' bes |
%  bes8 a ~ a2 |
%  d8 d bes4. a8 |
%  g2. |
}

melodio = {
  \time 6/4
  \key g \minor
  g8\4 g\4 f'4. ees'8 d'2. |
  g8\4 g\4 ees'2 ees'8 d' c'2 |
  d4. d8 d'4. c'8 bes4 d' |
  c'4. bes8 bes2. g4\4 bes4 a8 g\4 a2 r |
  \break
  g8\4 g8\4 f'4. ees'8 ees'4 d'2 |
  g8\4 g8\4 ees'4. d'8 c'2. |
  \break
  d4 d4 d'4. c'8 bes4 d' |
  c'4. bes8 bes2. g4\4 |
  bes4 a8 g\4 a2 g2\4 |
  \bar "||"
  \break

  d'4.^"V" c'8\2 d' c'\2 ees'2 d'4 |
  c'4.\2 bes8\2 c'\2 bes\2 d'2 c'4\2 |
  bes4.\2 c'8\2 bes\2 a\2 bes2\2 f'4 |
  ees'8 d' c'4\2 g'4 d'2. |
  \break
  d'4. c'8\2 d' c'\2 ees'2 d'4 |
  c'4.\2 bes8\2 c'\2 bes\2 d'2 c'4 |
  bes4. c'8 bes a bes2 d'4 |
  c'8 bes a4 bes g2.\4 |
  \bar "|."
}

\include "../muziko.ly"
