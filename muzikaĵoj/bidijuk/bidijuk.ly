\include "../ĉiea.ly"

\header {
  titolo-xx     = ""
  titolo-he     = "בדיוק בדיוק כמו שאני ככה זה טוב"
  titolo-eo     = "Percize, percize kiel mi estas"
  komponisto-xx = ""
  komponisto-he = "שי אור"
  komponisto-eo = "Ŝaj Or"
  ikono         = "="
}

\include "../titolo.ly"

melodio = {
  \key c \major
  g8\4 a4 a8 c' a4 a8 |
  a a a a a4 a8 a |
  g4\4 g8\4 a g\4 f d f ~ |
  f1 |
  \break f8 a4 a8 c' a4. |
  g8\4 a a g\4 a2 |
  a8 g4\4 a8 g4\4 f8 d |
  f1 |
  \break f8 g4\4 f8 g4\4 f8 g\4 |
  f g\4 g\4 g\4 g4.\4 g8\4 |
  g\4 g4\4 f g\4 a8 ~ |
  a2 d8 d g\4 g\4 |
  \break g\4 g4.\4 g8\4 g\4 g\4 g\4 |
  g\4 g4.\4 g8\4 g4\4 g8\4 |
  g\4 g\4 f g\4 a2 |
  a8 a a4. g8\4 ~ g4\4 ~ |
  g2\4 f |
  \break \repeat volta 2 {
	f8 f f4 f8 f f4 |
	f e f d ~ |
	d e d e |
	f2 f8 f f f |
	\break f2 e8 e f4 |
	e f d2 |
	d8 d e4. d |
	e4 f4. e |
	f4 d2. |
  } 
}

\include "../muziko.ly"
