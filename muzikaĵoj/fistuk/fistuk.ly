\include "../ĉiea.ly"

\header {
  titolo-xx     = ""
  titolo-he     = "הבית של פיסטוק"
  titolo-eo     = "La domo de Fistuk"
  komponisto-xx = ""
  komponisto-he = "נורית הירש"
  komponisto-eo = "Nurit Hirŝ"
  ikono         = "🏠"
}

\include "../titolo.ly"

melodio = {
  \time 4/4
  \key f \major
  \partial 8 c8 |
  a8 a a f ~ f c4 c8 |
  a8 a a f ~ f4 r8 c8 |
  d8 d d bes ~ bes bes4 bes8 |
  d8 d d bes ~ bes4 r8 bes |
  \break
  d'8 d' d' d' ~ d'4 d' |
  d'8 c' c' c' ~ c'4 r8 a |
  g8\4 g\4 g\4 g\4 ~ g\4 g4\4 g8\4 |
  a8 g\4 a c' ~ c'4 r |
  \break
  c'4 a8 a ~ a a a a |
  a4 f r2 |
  g4\4 g8\4 c' ~ c' c' c' c' |
  a4 f r2 |
  \break
  c'4 a8 a ~ a a a a |
  a4 f r2 |
  \repeat volta 3 { c4 c8 c' ~ c' c' c' c' | }
  % a4 f r r |
  f'4 f' r r |
  \bar "|."
}

\include "../muziko.ly"
