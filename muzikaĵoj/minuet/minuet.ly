\include "../ĉiea.ly"

\header {
  titolo-xx     = ""
  titolo-he     = "מנואט בסול מז׳ור"
  titolo-eo     = "Menueto (G-maĵoro)"
  komponisto-xx = ""
  komponisto-he = "כריסטיאן פצולד"
  komponisto-eo = "Christian Petzold"
  ikono         = "☧"
}

\include "../titolo.ly"

melodio = \transpose g g {
  \time 3/4
  \key g \major
  \repeat volta 2 {
	d'4 g8\4 a b c'  |
	d'4 g\4 g\4 |
	e'4 c'8\2 d'\2 e' fis' |
	g'4 g\4 g\4 |
	\break

	c'4 d'8 c' b a |
	b4 c'8 b a g\4 |
	fis4 g8\4 a b g\4 |
	a2. |
	\break

	d'4 g8\4 a b c'  |
	d'4 g\4 g\4 |
	e'4 c'8\2 d'\2 e' fis' |
	g'4 g\4 g\4 |
	\break

	c'4 d'8 c' b a |
	b4 c'8 b a g\4 |
	a4 b8 a g\4 fis |
	g2.\4 |
	\break
  }
  \pageBreak
  \repeat volta 2 {
	b'4 g'8 a' b' g' |
	a'4 d'8\2 e'\2 fis'\2 d'\2 |
	g'4 e'8\2 fis'\2 g' d'\2 |
	cis'4\3 b8\3 cis'\3 a4 |
	\break

	a8\2 b\2 cis'\2 d' e' fis' |
	g'4 fis' e' |
	fis' a cis' |
	d'2. |
	\break

	d'4 g8\4 fis g4\4 |
	e'4 g8\4 fis g4\4 |
	d'4 c' b |
	a8 g\4 fis g\4 a4 |
	\break

	d8 e fis g\4 a b |
	c'4 b a |
	b8 d' g4\4 fis |
	g2.\4 |
  }
}

\include "../muziko.ly"
