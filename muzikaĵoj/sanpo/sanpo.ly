\include "../ĉiea.ly"

\header {
  titolo-xx     = "となりのトトロ：さんぽ"
  titolo-he     = "טוטורו: סמפו"
  titolo-eo     = "Totoro: Sanpo"
  komponisto-xx = "久石譲"
  komponisto-he = "ג׳ו היסאישי"
  komponisto-eo = "Ĝo Hisaiŝi"
  ikono         = "🐛"
}

\include "../titolo.ly"

melodio = {
  \time 4/4
  \key c \major

  \small
  c'8. g16 \tuplet 3/2 { c'8 g c' } \tuplet 3/2 { d'8 g d' } \tuplet 3/2 { e'8 g e' } | 
  f'8. e'16 d'8. c'16 g8. b16 a8. g16                                                 | 
  c'8. g16 \tuplet 3/2 { c'8 g c' } \tuplet 3/2 { d'8 g d' } \tuplet 3/2 { e'8 g e' } | 
  f'8. e'16 \tuplet 3/2 { c'8 d' b } \tuplet 3/2 { c'8 g c' } c'4                     | 
  d8-> e-> f-> fis-> g-> a-> b-> c'->                                                 | 
  d'4-> \tuplet 3/2 { g'8-> g'-> g'-> } g'4-> r                                       | 
  a' \tuplet 3/2 { g'8 f' e' } d'4\2 a'                                               | 
  g' \tuplet 3/2 { f'8 e' d' } c'4\2 g'                                               | 
  f' \tuplet 3/2 { f'8 e' d' } c'8\2 b\2 a\2 b\2                                      | 
  c'4\2 \tuplet 3/2 { c'8\2 e' g' } c''4 r                                            | 
  \bar "||" \break

  \normalsize
  e4 g4\4 c'2                                     | 
  g4\4 a4 g2\4                                    | 
  r8. c16[ e8. g16\4] c'4 b8. a16                 | 
  g2.\4 r4                                        | 
  a8.[ a16 a8. a16 ~] a8.[ c'16 b8. a16]          | 
  g2.\4 r4                                        | 
  a8.[ g16\4 a8. g16\4] d4 e4                     | 
  c2. r4                                          | 
  aes8.[ aes16 aes8. aes16 ~] aes2                | 
  g8.\4[ g16\4 g8.\4 g16\4 ~] g2\4                | 
  f4 f4 f4 d8. e16 ~                              | 
  e2. r4                                          | 
  c4 c'8. c'16 b4 g8.\4 a16 ~                     | 
  a2 r8. a16[ b8. c'16]                           | 
  d'4 c'4 b4 a4                                   | 
  g2.\4 r4                                        | 
  \repeat volta 2 {
    c'8.[ b16 c'8. g16\4] e8.[ c'16 ~ c'8. b16 ~] | 
    b2. g8.\4 g16\4                               | 
    a4\4 r4 b4 r4                                 | 
  }
  \alternative {
    { c'2. r4                                     | }
    { c'4 \tuplet 3/2 {c'8 c'8 c'8} c'4           | }
  }
  \bar "|."
}

\include "../muziko.ly"
