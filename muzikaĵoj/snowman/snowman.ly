\include "../ĉiea.ly"

\header {
  titolo-xx     = ""
  titolo-he     = "את רוצה לבנות איש שלג?"
  titolo-eo     = "Ĉu vi volas konstrui neĝhomon?"
  komponisto-xx = ""
  komponisto-he = "קריסטן אנדרסון-לופז ורוברט לופז"
  komponisto-eo = "Kristen Anderson-Lopez kaj Robert Lopez"
  ikono         = "☃"
}

\include "../titolo.ly"

melodio = {
  \key f \major
  \time 4/4
  r c8 c f c f a |
  g4\4 a2. |
  r4 c8 c f c f a |
  g1\4 |
  \break
  r4. c8 f c f a |
  bes a f4. c8 bes a |
  f4. f8 f c f a |
  c'1 |
  \break
  r4. c'8 c' bes a bes |
  c' f2 r8 f4 |
  g\4 a8 f ~ f2 |
  a4 g8\4 f g4\4 a |
  d'1 |
  \break
  r4 c8 c f c f a |
  g4\4 a2. |
  r8 c c c f c f a |
  g4\4 f2. |
  \override TextSpanner.bound-details.left.text = "rit."
  r2\startTextSpan f4 e |
  f1\stopTextSpan |
  \bar "|."
}

\include "../muziko.ly"
