\include "../ĉiea.ly"

\header {
  titolo-xx     = ""
  titolo-he     = "הלילה את/ה שלי"
  titolo-eo     = "Ĉi-nokte vi estas mia"
  komponisto-xx = ""
  komponisto-he = "לי דייויד"
  komponisto-eo = "Lee David"
  ikono         = "🎆"
}

\include "../titolo.ly"

melodio = {
  \time 4/4
  \key g \major
  \partial 4 d4 |
  \repeat volta 2 {
	g2.\4 d8 e |
	f8 g\4 a b a4 g\4 |
	e8 g\4 a b a4 g\4 |
	ees8 g\4 a b a4 g8\4 a |
	\break
	b2. g8\4 b |
	a4 d'2 b8 a |
  }
  \alternative {
	{
	  g2.\4 r4 |
	  r2. d4 |
	}
	{
	  g2.\4 r4 |
	  r2. g8\4 g\4 |
	}
  }
  \break
  g2.\4 ees8 f |
  g2.\4 g8\4 g\4 |
  g2.\4 ees8 f |
  g2.\4 ees4 |
  \break
  %d2. b,8 c |
  d2. ~ d8 ~ d |
  d2. e4| 
  cis4 a2. |
  r4 ais8 r b d' b r |
  \break
  g2.\4 d8 e |
  f8 g\4 a b a4 g\4 |
  e8 g\4 a b a4 g\4 |
  ees8 g\4 a b a4 g8\4 a |
  \break
  b2. g8\4 b |
  a4 d'2 b8 a |
  g4\4 r d8 e d e |
  r8 g\4 g2.\4 |
  \bar "|."
}

\include "../muziko.ly"
