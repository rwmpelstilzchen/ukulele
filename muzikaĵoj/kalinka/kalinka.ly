\include "../ĉiea.ly"

\header {
  titolo-xx     = "Калинка"
  titolo-he     = "קלינקה"
  titolo-eo     = "Kalinka"
  komponisto-xx = "Иван Ларионов"
  komponisto-he = "איבן לרינוב"
  komponisto-eo = "Ivan Larinov"
  ikono         = "❄"
}

\include "../titolo.ly"

melodio = {
  \key d \minor
  \time 4/4
  \partial 4
  <a\1>4 |
  \repeat volta 2 {
	<g\4>4 <e\2>8 <f\2>8 <g\4>4 <e\2>8 <f\2>8 |
	<g\4>4 <f\2>8 <e\2>8 <d\3>4 <a\1>8 <a\1>8 |
	<g\4>8. <f\2>16( <e\2>8) <f\2>8 <g\4>4 <e\2>8 <f\2>8 |
  }
  \alternative {
	{
	  <g\4>4 <f\2>8 <e\2>8 <d\3>4 <a\1>4 |
	}
	{
	  <g\4>4 <f\2>8 <e\2>8 <d\3>2 |
	}
  }
  <d'\1>2 <c'\1>2 |
  \break
  \repeat volta 2 {
	<a\1>4 <c'\1>4 <bes\1>4 <a\1>8 <g\4>8 |
	<f\2>2 <c\3>2 |
	<a\1>4 <c'\1>4 <bes\1>4 <a\1>8 <g\4>8 |
	<f\2>2 <c\3>2 |
	<d\3>2 <d\3>4 <e\2>4 |
	<g\4>4 <f\2>4 <e\2>4 <d\3>4 |
  }
  \alternative {
	{
	  <c\3>2 <c'\1>2 |
	  <c'\1>1 |
	}
	{
	  <c'\1>2 <bes\1>2 |
	  <a\1>1 |
	}
  }
  \bar "|."
}

\include "../muziko.ly"
