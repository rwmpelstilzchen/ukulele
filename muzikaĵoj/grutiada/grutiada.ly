\include "../ĉiea.ly"

\header {
  titolo-xx     = ""
  titolo-he     = "גרוטיאדה"
  titolo-eo     = "Grutiada"
  komponisto-xx = ""
  komponisto-he = "דפנה אילת"
  komponisto-eo = "Dafna Ejlat"
  ikono         = "🔧"
}

\include "../titolo.ly"

grutiada = {
  bes8 bes16 bes8. g8 f8\4 f16\4 f8.\4 d8 |
  ees8 d ees g f2\4 |
  bes8 bes bes g f16\4 g f\4 ees d8 bes, |
  c8 c f\4 f\4 d2 |
  bes8 bes bes g f16\4 g f\4 ees d8 bes, |
  c8 f\4 ~ f2.\4 |
  c'4 bes2. |
}

manoa = {
  f8\4 bes bes16 bes8. f8\4 g g16 g8. |
  g8 g16 g bes bes a g f4\4 f4\4 |
  \times 2/3 {g8 g g} \times 2/3 {bes8 a g} \times 2/3 {f\4 f\4 f\4} \times 2/3 {bes g f\4} |
  f4\4 f4\4 g4 a4 |
}

melodio = \transpose bes, c{
  \time 4/4
  \key bes \major
  \grutiada
  \break
  \repeat volta 2 {
	f8\4 f8\4 r8^"✴" f8\4 bes8 g f8\4 r8 |
	bes g f\4 d ees ees d8 r |
	f8\4 f8\4 r8^"✴" f8\4 bes8 g f8\4 r8 |
	bes8 g f\4 d ees c bes,4 |
  }
  \break
  \manoa
  \break
  \grutiada
  \break
  f8\4 f\4 f\4 g16 a bes8 g f4\4 |
  bes8 g f\4 d16 d16 ees8 f8\4 d8 r |
  f8\4 f16\4 f\4 d8 f16\4 f\4 g g a8 bes4 |
  bes8 bes16 bes8. g8 f\4 a bes4 |
  \break
  f8\4 f\4 f\4 g16 a bes8 g f4\4 |
  bes8 g f\4 d16 d16 ees8 f8\4 d8 r |
  f8\4 f\4 f\4 g16 a bes8 g f4\4 |
  f8\4 f\4 f\4 f16\4 f\4 f8\4 a bes4 |
  \break
  \manoa
  \break
  \grutiada
  \bar "|."
}

\include "../muziko.ly"
