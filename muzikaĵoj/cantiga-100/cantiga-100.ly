\include "../ĉiea.ly"

\header {
  titolo-xx     = "Santa Maria Stela do dia"
  titolo-he     = "קנטיגה 100"
  titolo-eo     = "Kantigo 100"
  komponisto-xx = ""
  komponisto-he = "עממי / אלפונזו העשירי"
  komponisto-eo = "popola / Alfonso la 10-a"
  ikono         = "♍"
}

\include "../titolo.ly"

melodio = {
  \key d \dorian
  \time 4/4
  a4 g8\4 f  e4 e8 f |
  g4\4 a  d d |
  a4 g8\4 f  e4 d8 e |
  f8 e d c  d4 d |
  \break
  g4\4 a  b4. a8 |
  c'8 b a b  g4\4 g\4 |
  g4\4 a  b4. a8 |
  c'8 b a g\4  a4 a |
  \break
  b4 c'  d'4. b8 |
  c'8 d' c'16 b a8  b4 b |
  b4 b  b4. a8 |
  c'8 b a b g4\4 g\4 |
  \break
  a4 g8\4 f  e4 e8 f |
  g4\4 a  d d |
  a4 g8\4 f  e4 d8 e |
  f8 e d c  d4 d |
  \bar "|."
}

\include "../muziko.ly"
