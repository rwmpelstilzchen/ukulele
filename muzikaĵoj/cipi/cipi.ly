\include "../ĉiea.ly"

\header {
  titolo-xx     = ""
  titolo-he     = "רגע עם דודלי: ציפי הציפור"
  titolo-eo     = "Rega kun Dodli: Cipi la birdo"
  komponisto-xx = ""
  komponisto-he = "רומן קונסמן"
  komponisto-eo = "Roman Kunsman"
  ikono         = "🐤"
}

\include "../titolo.ly"

melodio = {
  \repeat volta 2 {
    c' g\4 c'8 c' e4 |
    f-. d-. e16( f) g\4 a g4\4 |
    c' g\4 c'16 c' c' c' e4 ~ |
    e8 f4 d8 c2 |
    \break
	c'4 g\4 c'8 c' e4 |
    f-. d-. e16( f) g\4 a g4\4 |
    c' g\4 e'8 e' c'4 ~ |
  }
  \alternative {
	{
	  c'2 e16 d c d e f g\4 b |
	}
	{
	  c'1\repeatTie |
	  c'8 c' c' c' c2 |
	  \bar "|."
	}
  }
}

\include "../muziko.ly"
