\include "../ĉiea.ly"

\header {
  titolo-xx     = ""
  titolo-he     = "הלילות הקסומים"
  titolo-eo     = "La magiaj noktoj"
  komponisto-xx = ""
  komponisto-he = "אילן וירצברג"
  komponisto-eo = "Ilan Vircberg"
  ikono         = "🐸"
}

\include "../titolo.ly"

melodio = {
  \time 2/4
  \key f \major

  \repeat volta 2 {
	\repeat unfold 2 {
	  c4 f8 g\4 ~ |
	  g4\4 f8 g\4 ~ |
	  g4\4 a8 a ~ |
	  a2 |
	}
	\break
	bes4 a8 f ~ |
	f d bes a |
	f2 ~ |
	f |
	bes4 a8 f ~ |
	f4 des8 des ~ |
	des4 c8 c ~ |
	c2 |
  }
  \break
  \repeat volta 2 {
	d4 c8 d |
	f4 d8 c |
	d c d g\4 ~ |
	g4\4 d8 c |
	d4 d8 c |
	d4 d8 c |
	d c d a ~ |
	a4 g8\4 a |
	\break c'4 c'8 c' |
	a4 a8 a |
	g\4 g\4 f d ~ |
	d4 r8 c |
	c'4 bes8 a |
	g4\4 c8 c |

  }
  \alternative {
    {
      c'8 bes a g\4 ~ |
      g2\4 |
    }
    {
      c'8 bes a f ~ |
      f2 |
    }
  }
}

\include "../muziko.ly"
