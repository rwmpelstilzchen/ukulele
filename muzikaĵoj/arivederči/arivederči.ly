\include "../ĉiea.ly"

\header {
  titolo-xx     = ""
  titolo-he     = "הבלדה על ארי ודרצ׳י"
  titolo-eo     = "La balado pri Ari kaj Derĉi"
  komponisto-xx = ""
  komponisto-he = "דני סנדרסון"
  komponisto-eo = "Dani Sanderson"
  ikono         = "👷"
}

\include "../titolo.ly"

melodio = {
  \time 4/4
  \key d \minor
  \set Score.tempoHideNote = ##f

  \tempo 4 = 72
  \repeat volta 2 {
    \repeat unfold 2 {
      d'4.-1 a8 f' e'4 d'8 |
      c'4\2 d'8.\2 c'16\2 c'16\2( bes\2) a8\2 ~ a4\2 |
    }
  }
  \break

  R1^"תנו לי בבקשה מוסיקה של חושך" |
  \tempo 4 = 69
  f16( g\2) a8 bes a16\4( g\4) a8 d' a g\4 |
  f16( g\2) f8 e d16( c) d2 |
  f16( g\2) a8 bes a16\4( g\4) a8 d' a g\4 |
  f16( g\2) f8 e d16( c) d4 ~ d8 a16 bes |
  \break
  c'8. g16\4 c'8. g16\4 e'8 c' c' bes |
  a8 a g\4 g16\4( bes\4) a4 ~ a8 a16 bes |
  c'8. g16\4 c'8. g16\4 e'8 c' c' bes |
  a8 a g\4 g16\4( a\4) f2 |
  \once \override Score.RehearsalMark.break-visibility = #begin-of-line-invisible
  \mark \markup { \musicglyph #"scripts.segno" }
  \bar "||"
  \break

  \tempo 4 = 128
  f8 e d e f e d f |
  a4 a r r8 g\4 ~ |
  g8\4 g\4 a bes a g\4 a f |
  g8\4 g\4 a bes a8. g16\4 f8 f |
  \break
  g8\4 g\4 g\4 g\4 g\4 g\4 a bes |
  a4 f r8 g4\4 g8\4 |
  e8 e e e e e f g\4 |
  f8 d ~ d4 r r8 e |
  \once \override Score.RehearsalMark.break-visibility = #begin-of-line-invisible
  \mark \markup { \musicglyph #"scripts.coda" }
  \break

  \repeat volta 2 {
    f8 e d e f e d f |
    g8\4 g\4 g\4 c g4\4 r8 e |
    f8 e d e f e d f |
    a8 a a c a4 r8 f |
    \break
    g8\4 f g\4 a bes a g\4 f |
    g8\4 a g\4 f f4 r8 f |
    g8\4 d g\4 a bes a g\4 g\4 |
    g8\4 bes bes bes a4\4( g\4)_\markup {\italic "D.S. al Coda"} |
  }
  \break

  \mark \markup { \musicglyph #"scripts.coda" }
  \interim
  d8 d16 f a8 f e f e16 d c8 |
  c8 e16 f c'8 bes16 a g8\4 a bes4 |
  ges8 bes es' des'16 c' bes8 c' des'16 c' bes as |
  des'8 c'16 des' bes8 c'16 des' des' c' bes as bes4 |
  \deinterim
  \break

  \tempo 4 = 132
  \repeat unfold 2 {
    bes4 a bes a8 g\4 ~ |
    g8\4 g4\4 a8 g4\4 f |
  }
  \break
  a4 a c' f8 g\4 ~ |
  g8\4 g4\4 a8 g4\4 f |
  g8\4 g4\4 g8\4 g4\4 e |
  g8\4 f r4 r2 |
  \break
  \repeat unfold 2 {
    bes4 a bes a8 g\4 ~ |
    g8\4 g4\4 a8 g4\4 f |
  }
  \break
  bes4 as ges f8 as ~ |
  as8 as4 as8 f\3( es) des4 | % orig fes
  es8 es4 es8 es4 c8 f ~ |
  f4 r r2 |
  \break

  \tempo 4 = 76
  r2 r4 r8 f |
  bes8 bes bes bes bes a16 a r4 |
  bes8 bes bes bes16 bes ~ bes8 a a f |
  d'8 d'16 d' ~ d'8 d'16 d' ~ d'8 c' r8 c'16 c' |
  bes8 bes bes a bes c' r g\4 |
  \break
  bes8 bes bes bes bes a r f |
  bes8 bes bes bes bes a r f |
  d'8 d' d' d' d' c' r bes |
  bes2 d' |
  bes2\2-1 f'4. e'8 |
  f'16( es') f'8 ~ f'4 bes'4 as' |
  \repeat unfold 3 {
    r2 bes'4 as' |
  }
  \repeat volta 2 {
    d'4.-1 a8 f' e'4 d'8 |
    c'4\2 d'8.\2 c'16\2 c'\2 bes\2 a8\2 ~ a4\2 |
    \once \override Score.RehearsalMark.break-visibility = #begin-of-line-invisible
    \mark "ad lib."
  }
}

emelodio = {
  \time 4/4
  \key e \minor
  \set Score.tempoHideNote = ##f

  \tempo 4 = 72
  \repeat volta 2 {
	\repeat unfold 2 {
	  e'4. b8 g' fis'4 e'8 |
	  d'4 e'8. d'16 d'16( c') b8 ~ b4 |
	}
  }
  \break

  R1^"תנו לי בבקשה מוסיקה של חושך" |
  \tempo 4 = 69
  g16( a) b8 c' b16( a) b8 e' b a |
  g16( a) g8 fis e16( d) e2 |
  g16( a) b8 c' b16( a) b8 e' b a |
  g16( a) g8 fis e16( d) e4 ~ e8 b16 c' |
  \break
  d'8. a16 d'8. a16 fis'8 d' d' c' |
  b8 b a a16( c') b4 ~ b8 b16 c' |
  d'8. a16 d'8. a16 fis'8 d' d' c' |
  b8 b a a16( b) g2 |
  \once \override Score.RehearsalMark.break-visibility = #begin-of-line-invisible
  \mark \markup { \musicglyph #"scripts.segno" }
  \bar "||"
  \break

  \tempo 4 = 128
  g8 fis e fis g fis e g |
  b4 b r r8 a ~ |
  a8 a b c' b a b g |
  a8 a b c' b8. a16 g8 g |
  \break
  a8 a a a a a b c' |
  b4 g r8 a4 a8 |
  fis8 fis fis fis fis fis g a |
  g8 e ~ e4 r r8 fis |
  \once \override Score.RehearsalMark.break-visibility = #begin-of-line-invisible
  \mark \markup { \musicglyph #"scripts.coda" }
  \break

  \repeat volta 2 {
	g8 fis e fis g fis e g |
	a8 a a d a4 r8 fis |
	g8 fis e fis g fis e g |
	b8 b b d b4 r8 g |
	\break
	a8 g a b c' b a g |
	a8 b a g g4 r8 g |
	a8 e a b c' b a a |
	a8 c' c' c' b4( a)_\markup {\italic "D.S. al Coda"} |
  }
  \break

  \mark \markup { \musicglyph #"scripts.coda" }
  \interim
  e8 e16 g b8 g fis g fis16 e d8 |
  d8 fis16 g d'8 c'16 b a8 b c'4 |
  aes8 c' f' ees'16 d' c'8 d' ees'16 d' c' bes |
  ees'8 d'16 ees' c'8 d'16 ees' ees' d' c' bes c'4 |
  \deinterim
  \break

  \tempo 4 = 132
  \repeat unfold 2 {
	c'4 b c' b8 a ~ |
	a8 a4 b8 a4 g |
  }
  \break
  b4 b d' g8 a ~ |
  a8 a4 b8 a4 g |
  a8 a4 a8 a4 fis |
  a8 g r4 r2 |
  \break
  \repeat unfold 2 {
	c'4 b c' b8 a ~ |
	a8 a4 b8 a4 g |
  }
  \break
  c'4 bes aes g8 bes ~ |
  bes8 bes4 bes8 g( f) ees4 | % orig fes
  f8 f4 f8 f4 d8 g ~ |
  g4 r r2 |
  \break

  \tempo 4 = 76
  r2 r4 r8 g |
  c'8 c' c' c' c' b16 b r4 |
  c'8 c' c' c'16 c' ~ c'8 b b g |
  e'8 e'16 e' ~ e'8 e'16 e' ~ e'8 d' r8 d'16 d' |
  c'8 c' c' b c' d' r a |
  c'8 c' c' c' c' b r g |
  c'8 c' c' c' c' b r g |
  e'8 e' e' e' e' d' r c' |
  c'2 e' |
  c'2 g'4. fis'8 |
  g'16( f') g'8 ~ g'4 c''4 bes' |
  \repeat unfold 3 {
	r2 c''4 bes' |
  }
  \repeat volta 2 {
	e'4. b8 g' fis'4 e'8 |
	d'4 e'8. d'16 d' c' b8 ~ b4 |
	\once \override Score.RehearsalMark.break-visibility = #begin-of-line-invisible
	\mark "ad lib."
  }
}

\include "../muziko.ly"
