\include "../ĉiea.ly"

\header {
  titolo-xx     = "El Laberinto del Fauno"
  titolo-he     = "המבוך של פאן"
  titolo-eo     = "La labirinto de la faŭno"
  komponisto-xx = ""
  komponisto-he = "חבייר נברטה"
  komponisto-eo = "Javier Navarrete"
  ikono         = "♑"
}

\include "../titolo.ly"

melodio = {
  \key d \major
  \time 3/4
  fis8 e d e fis g |
  fis2.^\fermata |
  fis8 e d e fis g |
  e2.^\fermata |
  e8 d cis d e fis |
  e2 fis4 |
  e2 fis4 |
  g2\4 a4 |
  fis2. ~ |
  fis ~ |
  fis |
  \break
  fis'8 e' d' e' fis' g' |
  fis'2. ~ |
  fis' |
  fis'8 e' d' e' fis' g' |
  e'2. ~ |
  e' |
  e'8\4 d'\4 cis'\4 d'\4 e'\4 fis'\4 |
  e'2\4 fis'4 |
  e'2\4 fis'4 |
  g'2 a'4 |
  fis'2. ~ |
  fis' ~ |
  fis' |
  \break
  fis ~ |
  fis |
  fis4 g a |
  g2. ~ |
  g |
  g4 a b |
  ais2. ~ |
  ais4 ~ ais g |
  fis2. ~ |
  fis ~ |
  fis |
  \bar "|."
}

\include "../muziko.ly"
