\include "../ĉiea.ly"

\header {
  titolo-xx     = ""
  titolo-he     = \markup{"זרעים של מסטיק" \tiny{"זרעים של מסטיק:"}}
  titolo-eo     = \markup{\tiny{Semoj de maĉgumo:} Semoj de maĉgumo}
  komponisto-xx = ""
  komponisto-he = "נחום נחצ׳ה היימן"
  komponisto-eo = "Naĥum Naĥĉe Hejman"
  ikono         = "🍬"
}

\include "../titolo.ly"

melodio = {
  \key f \major
  \time 3/4
  \partial 4 c4 |
  f4 e f |
  d2 f4 |
  e2 g4\4 |
  f2 c4 |
  c'4 a g\4 |
  f4 bes a |
  g4\2 e f |
  g2\4 g4\4 |
  \break
  f4 e d |
  c4 f g\4 |
  a4 bes c' |
  d'4 bes\fermata a |
  g4\4 c' bes |
  a4 f e |
  d4 g\4 f |
  e4 c c |
  \break
  \repeat volta 2 {
	f4 f f |
	e2 d4 |
	c4 d e |
	f2 g4\4 |
	a4 d e |
	f4 bes a |
	g2.\4 ~ |
	g2\4 bes4 |
	\break
	a4 g\4 a |
	f2 e4 |
	d4 e f |
	g2\4 f4 |
	e4 d c |
	bes4 a g\4 |
  }
  \alternative {
	{
	  f2. ~ |
	  f2 c4 |
	}
	{
	  f2. ~ |
	  f4. r4 r8 |
	}
  }
  \bar "|."
}

\include "../muziko.ly"
