\include "../ĉiea.ly"

#(set-global-staff-size 25)

\header {
  titolo-xx     = ""
  titolo-he     = "כך נולד הצבע"
  titolo-eo     = "Tiamaniere naskiĝis la koloro"
  komponisto-xx = ""
  komponisto-he = "דתיה בן־דור"
  komponisto-eo = "Datja Ben-Dor"
  ikono         = "♻️"
}

\include "../titolo.ly"

melodio =  {
  \key g \major
  \repeat volta 2 {
	fis16 b4 b8. b16 a8. ~ a4 |
	fis16 g8.\4 e16 e8. d'16 b8. ~ b4 |
	fis16 b4 b8. b16 a8. ~ a4 |
	fis16 g8.\4 e16 fis8. d16 e8. ~ e4 |
	\break
	e8 d c' b a b16 a gis4 |
	f8 e d' c' b c'16 b a4 |
	g8\4 fis4 e8 c e fis b |
	fis1 |
	\break
	d8 e d b b b b b |
	d e d a a a a a |
	d e d a a a g\4 fis |
	g\4 a fis b ~ b2 |
	\break
	d8 e d b b b b b |
	b c' d' c' b c' d' e' ~ |
	e'2 e'16 d'8. c'16 b8. |
	a16 e8. g16\4 fis8. a16 g8.\4 ~ g4\4 |
	\bar "||"
	\interim
	e'2 ~ e'16 d'8. c'16 b8. |
	a16 e8. g16\4 fis8. a16 g8.\4 ~ g4\4 ~ |
	g2 r2 |
	\deinterim
  }
}

fmmelodio = \transpose a, b, {
  \key f \major
  e16 a4 a8. a16 g8. ~ g4 |
  e16 f8. d16 d8. c'16 a8. ~ a4 |
  e16 a4 a8. a16 g8. ~ g4 |
  e16 f8. d16 e8. c16 d8. ~ d4  |
  \break
  d8 c bes a g a16 g16 fis4 |
  ees8 d c' bes a bes16 a g4 |
  f8 e4 d8 bes,8 d e a |
  e1 |
  \break
  c8 d c a8 a a a a8 |
  c8 d c g g g g g |
  c8 d c g g g f e |
  f8 g e a8 ~ a2 |
  \break
  c8 d c a8 a a a a8 |
  a8 bes c' bes a bes c' d' ~ |
  d'2 d'16 c'8. bes16 a8. |
  g16 d8. f16 e8. g16 f8. ~ f4 |
  \bar "||"
  %\interim
  d'2 ~ d'16 c'8. bes16 a8. |
  g16 d8. f16 e8. g16 f8. ~ f4 |
  %\deinterim
  \bar "|."
}

\include "../muziko.ly"
