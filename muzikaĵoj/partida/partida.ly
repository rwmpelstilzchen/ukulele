\include "../ĉiea.ly"

\header {
  titolo-xx     = "Partida"
  titolo-he     = "אני אשתגע"
  titolo-eo     = "Disiĝado"
  komponisto-xx = ""
  komponisto-he = ""
  komponisto-eo = ""
  ikono         = "💔"
}

\include "../titolo.ly"

melodio = {
  \tempo "Allegro" 4 = 160
  \key c \minor
  \time 3/4
  \set Timing.beamExceptions = #'()
  \set Timing.beatStructure = #'(1 1 1)
  \repeat volta 2 {
	r8 g\4 g\4 c'4 ees'8 |
	\bar "||"\mark \markup { \musicglyph #"scripts.segno" }
	c'4 g2\4 |
	r8 g\4 g\4 aes4\4 g8\4 |
	f2. |
	\break
	r8 g\4 g\4 g\4 g\4 g\4 |
	g4\4 d' c'8 b ~ |
	b4 g8\4 g4\4 f8 |
	ees2. |
	\break
	r8 c d ees f g\4 |
	bes2 c'4 |
	des'4 c'8 g\4 g\4 bes |
	aes4 g8\4 f ~ f4 |
	\break
	r8 f8 g\4 aes g\4 f |
	g4\4 c'8 g4\4 ees8 |
	d4 f8 ees4 d8 |
	c2. |
  }
  \pageBreak

  \repeat volta 2 {
	r8 g8\4 g\4 g\4 g\4 g\4 |
	aes8\4 g\4 g\4 g\4 b4 |
	g8\4 b b d' d' d' |
	ees'8 g\4 g\4 c' ees'4 |
	\break
	g8\4 c' c' ees' ees' c' |
	d'8 g\4 g\4 b d'4-2 |
	d'8 d' d' f' ees' d' |
	c'8 b c' d' ees' d' |
	\break
	c'8 c' c' c' des' c' |
	b8 c' g\4 bes e g\4 |
	c8 c e g\4 bes des' |
	c'4. aes8 f4 |
	\break
	r8 f g\4 aes g\4 f |
	g4\4 c'8 g4\4 ees8 |
	d4 f8 ees4 d8 |
	c2. |
  }
  R2. |
  \break
  \interim
  \repeat volta 2 {
	f'8\2 f'4\2 aes'8 aes'4 |
	g'8 g'4 f'8\2 f'4\2 |
	ees'8\2 ees'4\2 g'8 g'4 |
	f'8\2 f'4\2 ees'8\2 ees'4\2 |
	\break
	d'8\3 d'4\3 f'8\2 f'4\2 |
	ees'8\2 ees'4\2 d'8\3 d'4\3 |
  }
  \alternative {
	{
	  c'8\3 c'4\3 ees'8\2 ees'4\2 |
	  r8 d'8\3 d'4\3 c'\3 |
	}
	{
	  c'4\3 b8\3 c'\3 ees'\2 g' |
	  c''8 g\4 g\4 c'4 ees'8 |
	  \bar "||"\mark \markup { \musicglyph #"scripts.coda" }
	}
  }
}

%melodio = {%\transpose a c' {
%  \tempo 4 = 160
%  \key a \minor
%  \time 3/4
%  e8 e a8 c'8 a4 |
%  e2 e8 e |
%  f4 e8  d8 ~ d4 |
%  d8 d8 d8 d8 d8 d8 ~ |
%  d8 b4 a8 aes f8 |
%  e8 d8 c2 |
%  a,8 cis e g a bes |
%  bes4 bes8 bes bes bes a a g g4 
%}

\include "../muziko.ly"
