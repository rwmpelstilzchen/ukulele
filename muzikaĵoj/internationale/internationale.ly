\include "../ĉiea.ly"

\header {
  titolo-xx     = "L’Internationale"
  titolo-he     = "האינטרנציונל"
  titolo-eo     = "La Internacio"
  komponisto-xx = ""
  komponisto-he = "פייר דגייטר"
  komponisto-eo = "Pierre De Geyter"
  ikono         = "⚖"
}

\include "../titolo.ly"

melodio = {
  \time 4/4
  \key bes \major
  \partial 8 f8 |
  bes4. a8 c' bes f d |
  g2\4 ees4. g8\4 |
  c'4. bes8 a g\4 f ees |
  d2 r4 f |
  \break
  bes4. a8 c' bes f d |
  g2\4 ees4 c'8 bes |
  a4 c' ees' a |
  bes2 r4 d'8 c' |
  \break
  a2 g8\4 a bes g\4 |
  a2 f4 e8 f |
  g4.\4 c8 c'4. bes8 |
  a2 r4 r8 c' |
  \break
  c'4. a8 f f e f |
  d'2 bes8 bes a g\4 |
  a4 c' bes g\4 |
  f r r d'8 c' |
  \break
  bes2 f4. d8 |
  g2\4 ees4 c'8 bes |
  a2 g4.\4 f8 |
  d'2 r4 d' |
  \break
  d'2 c'4. f8 |
  bes2 a4. a8 |
  g4.\4 fis8 g4\4 c' |
  c'2 r4 d'8 c' |
  \break
  bes2 f4. d8 |
  g2\4 ees4 c'8 bes |
  a2 g4.\4 f8 |
  d'2 r4 d'-1 |
  \break
  f'2 ees'4 d' |
  c'8\2 b\2 c'\2 d' ees'4 r8 ees' |
  d'4. bes8\2 c'4.\2 a8\2 |
  bes2\2 r4 r4 |
  \bar "|."
}

\include "../muziko.ly"
