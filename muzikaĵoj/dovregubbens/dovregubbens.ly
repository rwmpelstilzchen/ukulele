\include "../ĉiea.ly"

#(set-global-staff-size 27)

\header {
  titolo-xx     = \markup{\small "I Dovregubbens hall"}
  titolo-he     = \markup{\small "בהיכלו של מלך־ההר"}
  titolo-eo     = \markup{\small "En la kaverno de l’ montara reĝo"}
  komponisto-xx = ""
  komponisto-he = \markup{\small "אדוורד גריג"}
  komponisto-eo = \markup{\small "Edvard Grieg"}
  ikono         = "👑"
}

\include "../titolo.ly"

melodio = {
  \key c \minor
  g1\4\p\fermata-> |
  c8-.\pp d-._\markup \line { \italic "e" \italic "sempre" \italic "staccato"} ees-. f-. g-.-> ees-. g4-. |
  fis8-.-> d-. fis4-. f8-.-> des-. f4-. |
  c8-. d-. ees-. f-. g-.-> ees-. g-. c'-. |
  bes-. g-. ees-. g-. bes2-> |
  \break
  \set TabStaff.minimumFret = #12
  c'8-. d'-. ees'-. f'-. g'-.-> ees'-. g'4-. |
  fis'8-.-> d'-. fis'4-. f'8-.-> des'-. f'4-. |
  c'8-. d'-. ees'-. f'-. g'-.-> ees'-. g'-. c''-. |
  bes'-. g'-. ees'-. g'-. < bes' >2-> |
  \break
  \set TabStaff.minimumFret = #5
  \repeat unfold 2 {
	g8\4-. a-. b-. c'-. d'-.-> b-. d'4-. |
	ees'8-.-> b-. ees'4-. d'8-.-> b-. d'4-. |
	g8\4-. a-. b-. c'-. d'-.-> b-. d'4-. |
	ees'8-.-> b-. ees'4-. d'2-> |
	\break
  }
  %\break
  %\transpose c' c {
  %  g'8\4-. a'-. b'-. c''-. d''-.-> b'-. d''4-. |
  %  ees''8-.-> b'-. ees''4-. d''8-.-> b'-. d''4-. |
  %  g'8\4-. a'-. b'-. c''-. d''-.-> b'-. d''4-. |
  %  ees''8-.-> b'-. ees''4-. < d'' >2-> |
  %}
  \set TabStaff.minimumFret = #0
  \break
  c8\pp-. d-. ees-. f-. g-.-> ees-. g4-. |
  fis8-.-> d-. fis4-. f8-.-> des-. f4-. |
  c8-. d-. ees-. f-. g-.-> ees-. g-. c'-. |
  bes-. g-. ees-. g-. bes2-> |
  \break
  \set TabStaff.minimumFret = #12
  c'8-. d'-. ees'-. f'-. g'-.-> ees'-. g'4-. |
  fis'8-.-> d'-. fis'4-. f'8-.-> des'-. f'4-. |
  c'8-. d'-. ees'-. f'-. g'-.-> ees'-. g'-. c''-. |
  bes'-. g'-. ees'-. g'-. < bes' >2-> |
  \set TabStaff.minimumFret = #0
  \pageBreak
  %\clef "treble"
  \transpose c'' c' {
	%  c''8-. d''-. < g' ees'' >-. f''-. g''-.-> ees''-. < g' g'' >4-. |
	%  fis''8-.-> d''-. < fis' fis'' >4-. f''8-.-> des''-. < f' f'' >4-. |
	%  c''8-. d''-. < g' ees'' >-. f''-. g''-. ees''-. g''-. c'''-. |
	%  bes''-. g''-. < bes' ees'' >-. g''-. bes''2-> |
	%  \break c'''8-. d'''-. ees'''-. f'''-. g'''-.-> ees'''-. g'''4-. |
	%  fis'''8-.-> d'''-. fis'''4-. f'''8-.-> des'''-. f'''4-. |
	%  c'''8-. d'''-. ees'''-. f'''-. g'''-. ees'''-. g'''-. c''''-. |
	%  bes'''-. g'''-. ees'''-. g'''-. bes'''2-> |
	%  \break g''8-.\< a''-. b''-. c'''-. d'''-.-> b''-. d'''4 |
	%  ees'''8-.-> b''-. ees'''4 d'''8-.-> b''-. d'''4 |
	%  g''8-. a''-. b''-. c'''-. d'''-.-> b''-. d'''4 |
	%  ees'''8-.-> b''-. ees'''4 d'''2-> |
	%  \break g'''8-. a'''-. < g''' b''' >-. c''''-. d''''-.-> b'''-. < g''' b''' d'''' >4-. |
	%  ees''''8-.-> b'''-. < g''' b''' ees'''' >4-. d''''8-.-> b'''-. < g''' b''' d'''' >4-. |
	%  g'''8-. a'''-. < g''' b''' >-. c''''-. d''''-.-> b'''-. < g''' b''' d'''' >4-. |
	%  ees''''8-.-> b'''-. < g''' b''' ees'''' >4-. d''''2 |
	%  \break c''8-.\mf d''-.\< < g' ees'' >-. f''-. g''-.-> ees''-. < g' g'' >4-. |
	%  fis''8-.-> d''-. < g' fis'' >4-. f''8-.-> des''-. < g' fis'' >4-. |
	%  c''8-. d''-. < g' ees'' >-. f''-. g''-. ees''-. < c'' g'' >-. c'''-. |
	%  \break bes''-. g''-. < bes' f'' >-. g''-. bes''2-> |
	%  c'''8-. d'''-. ees'''-. f'''-. g'''-.-> ees'''-. g'''4 |
	%  fis'''8-.-> d'''-. fis'''4-. f'''8-. des'''-. f'''4 |
	%  \break c'''8-. d'''-. ees'''-. f'''-. g'''-. ees'''-. g'''-. c''''-. |
	%  g'''-. ees'''-. g'''-. c''''-. c'''2-> |
	%  \tempo "Più vivo" 4 = 154
	%  \ottava #1 c''''8-.->\ff d''''-. ees''''-. f''''-. g''''-> ees''''-. g''''4-. |
	%  fis''''8-.-> d''''-. fis''''4-. f''''8-.-> des''''-. f''''4-. |
	%  \break c''''8-. d''''-. ees''''-. f''''-. g''''-. ees''''-. g''''-. c'''''-. |
	%  bes''''-. g''''-. ees''''-. g''''-. bes''''2-> |
	%  c''''8-> d''''-. ees''''-. f''''-. g''''-> ees''''-. g''''4-. |
	%  fis''''8-> d''''-. fis''''4-. f''''8-> des''''-. f''''4-. |
	%  \break c''''8-. d''''-. ees''''-. f''''-. g''''-. ees''''-. g''''-. c'''''-. |
	%  bes''''-. g''''-. ees''''-. g''''-. bes''''2-> |
	%  g''''8-._\markup \line { \italic "sempre" \italic "stretto" \italic "sin" \italic "al" \italic "fine"} a''''-. b''''-. c'''''-. d'''''-> b''''-. d'''''4-> |
	%  < ees'''' g'''' ees''''' >8-> b''''-. < ees'''' g'''' ees''''' >4-> |
	%  < d'''' g'''' d''''' >8-> b''''-. < d'''' g'''' d''''' >4-> |
	%  \break g''''8-. < g'''' aes'''' >-. < g'''' b'''' >-. < g'''' c''''' >-. < d'''' g'''' d''''' >-> < d'''' g'''' b'''' >-. < d'''' g'''' d''''' >4-> |
	%  < ees'''' g'''' ees''''' >8-> < ees'''' g'''' b'''' >-. < ees'''' g'''' ees''''' >4-> < d'''' g'''' d''''' >2-> |
	%  g''''8-. a''''-. b''''-. c'''''-. d'''''-> b''''-. d'''''4-> |
	%  < e'''' g'''' e''''' >8 b''''-. < e'''' g'''' e''''' >4-> < d'''' g'''' d''''' >8-> b''''-. < d'''' g'''' d''''' >4-> |
	%  \break g''''8-. < g'''' aes'''' >-. < g'''' b'''' >-. < g'''' c''''' >-. < d'''' g'''' d''''' >-> < d'''' g'''' b'''' >-. < d'''' g'''' d''''' >4-> |
	%  < ees'''' g'''' ees''''' >8-> < ees'''' g'''' b'''' >-. < ees'''' g'''' ees''''' >4-> < d'''' g'''' d''''' >2-> |
	%  c''''8-. d''''-. ees''''-. f''''-. g''''-> ees''''-. g''''4-> |
	%  fis''''8-> d''''-. fis''''4-. f''''8-> des''''-. f''''4-. |
	%  \break c''''8-. d''''-. ees''''-. f''''-. g''''-. c''''-. g''''-. c'''''-. |
	%  bes''''-. g''''-. ees''''-. g''''-. bes''''2-> |
	%  c''''8-. < c'''' d'''' >-. < c'''' ees'''' >-. < c'''' f'''' >-. < c'''' g'''' >-> < c'''' ees'''' >-. < c'''' ees'''' g'''' >4-. |
	%  < c'''' fis'''' >8-> < c'''' d'''' >-. < c'''' d'''' fis'''' >4-. < c'''' f'''' >8-> < c'''' des'''' >-. < c'''' des'''' f'''' >4-. |
	%  \break c''''8-. < c'''' d'''' >-. < c'''' ees'''' >-. < c'''' f'''' >-. < c'''' g'''' >-. < c'''' ees'''' >-. < c'''' g'''' >-. < c'''' c''''' >-. |
	%  < c'''' g'''' >-. < c'''' ees'''' >-. < c'''' g'''' >-. < c'''' c''''' >-. c''''2-> |
	\transpose c'' c {
	  r4 \appoggiatura { a''''16[ bes'''' b''''] } < c''''' >4-.\sfz r2 |
	  r4 \appoggiatura { a''''16[ bes'''' b''''] } < c''''' >4\sfz-. r2 |
	  c'''8-. d'''-. ees'''-. f'''-. g'''-. ees'''-. g'''-. c''''-. |
	  b'''-. g'''-. b'''-. d''''-. c''''2-> |
	  \break
	  r4 \appoggiatura { a''''16[ bes'''' b''''] } < c''''' >4-.\sfz r2 |
	  r4 \appoggiatura { a''''16[ bes'''' b''''] } < c''''' >4\sfz-. r2 |
	  \set TabStaff.minimumFret = #12
	  c''''8-. d''''-. ees''''-. f''''-. g''''-. ees''''-. g''''-. c'''''-. |
	  b''''-. g''''-. b''''-. d'''''-. c'''''2-> |
	  \set TabStaff.minimumFret = #0
	  \break
	  r4 \appoggiatura { a''''16[ bes'''' b''''] } < c''''' >4-.\sfz r2 |
	  r4 \appoggiatura { a''''16[ bes'''' b''''] } < c''''' >4\sfz-. r2 |
	  r4 \appoggiatura { a''''16[ bes'''' b''''] } < c''''' >4\pp \appoggiatura { a''''16[ bes'''' b''''] } < c''''' >4 \appoggiatura { a''''16[ bes'''' b''''] } < c''''' >4 |
	  \appoggiatura { a''''16[ bes'''' b''''] } < c''''' >4\< \appoggiatura { a''''16[ bes'''' b''''] } < c''''' >4 \appoggiatura { a''''16[ bes'''' b''''] } < c''''' >4 \appoggiatura { a''''16[ bes'''' b''''] } < c''''' >4 |
	}
	  %R1*2 |
	  r2 \repeat tremolo 4 {g'16\4 g''} |
	  \repeat tremolo 4 {g'16\4 g''} \repeat tremolo 4 {g'16\4 g''} |
	  \transpose c'' c {
		r4\ff \appoggiatura { a''''16[ bes'''' b''''] } < c''''' >4 r2 \bar "|."
		|
	  }
	}
  }

origmelodio = \transpose b, c'' {\relative c' {
  \key b \minor
  <fis,, fis'>1\p\fermata-> |
  b,,8-.\pp cis-._\markup {\italic {e sempre staccato}} d-. e-. fis-.-> d-. fis4-. |
  eis8-.-> cis-. eis4-. e8-.-> c-. e4-. |
  b8-. cis-. d-. e-. fis-.-> d-. fis-. b-. |
  a-. fis-. d-. fis-. a2-> |
  \break
  b8-. cis-. d-. e-. fis-.-> d-. fis4-. |
  eis8-.-> cis-. eis4-. e8-.-> c-. e4-. |
  b8-. cis-. d-. e-. fis-.-> d-. fis-. b-. |
  a-. fis-. d-. fis-. <a, a'>2-> |
  \break
  fis8-. gis-. ais-. b-. cis-.-> ais-. cis4-. |
  d8-.-> ais-. d4-. cis8-.-> ais-. cis4-. |
  fis,8-. gis-. ais-. b-. cis-.-> ais-. cis4-. |
  d8-.-> ais-. d4-. cis2-> |
  \break
  fis8-. gis-. ais-. b-. cis-.-> ais-. cis4-. |
  d8-.-> ais-. d4-. cis8-.-> ais-. cis4-. |
  fis,8-. gis-. ais-. b-. cis-.-> ais-. cis4-. |
  d8-.-> ais-. d4-. <cis, cis'>2-> |
  \break
  b,8\pp-. cis-. d-. e-. fis-.-> d-. fis4-. |
  eis8-.-> cis-. eis4-. e8-.-> c-. e4-. |
  b8-. cis-. d-. e-. fis-.-> d-. fis-. b-. |
  a-. fis-. d-. fis-. a2-> |
  \break
  b8-. cis-. d-. e-. fis-.-> d-. fis4-. |
  eis8-.-> cis-. eis4-. e8-.-> c-. e4-. |
  b8-. cis-. d-. e-. fis-.-> d-. fis-. b-. |
  a-. fis-. d-. fis-. <a, a'>2-> |
  \break
  \clef treble
  b'8-. cis-. <fis, d'>-. e'-. fis-.-> d-. <fis, fis'>4-. |
  eis'8-.-> cis-. <eis, eis'>4-. e'8-.->  c-. <e, e'>4-. |
  b'8-. cis-. <fis, d'>-. e'-. fis-. d-. fis-. b-. |
  a8-. fis-. <a, d>-. fis'-. a2-> |
  \break
  
  b8-. cis-. d-. e-. fis-.-> d-. fis4-. |
  eis8-.-> cis-. eis4-. e8-.-> c-. e4-. |
  b8-. cis-. d-. e-. fis-. d-. fis-. b-. |
  a-. fis-. d-. fis-. a2-> |
  \break
  fis,8-.\cresc gis-. ais-. b-. cis-.-> ais-. cis4 |
  d8-.-> ais-. d4 cis8-.-> ais-. cis4 |
  fis,8-. gis-. ais-. b-. cis-.-> ais-. cis4 |
  d8-.-> ais-. d4 cis2-> |
  \break
  
  fis8-. gis-. <fis ais>-. b-. cis-.-> ais-. <fis ais cis>4-. |
  d'8-.-> ais-. < fis ais d>4-. cis'8-.-> ais-. <fis ais cis>4-. |
  fis8-. gis-. <fis ais>-. b-. cis-.-> ais-. <fis ais cis>4-. |
  d'8-.-> ais-. <fis ais d>4-.  cis'2 |
  \break
  
  b,,8-.\mf cis-.\cresc <fis, d'>-. e'-. fis-.-> d-. <fis, fis'>4-. |
  eis'8-.-> cis-. <fis, eis'>4-. e'8-.-> c-. <fis, eis'>4-. |
  b8-. cis-. <fis, d'>-. e'-. fis-. d-. <b fis'>-. b'-. |
  \break
  a-. fis-. <a, e'>-. fis'-.  a2-> |
  b8-. cis-. d-. e-. fis-.-> d-. fis4 |
  eis8-.-> cis-. eis4-. e8-. c-. e4 |
  \break
  b8-. cis-. d-. e-. fis-. d-. fis-. b-. |
  fis-. d-. fis-. b-. b,2-> |
  \tempo "Più vivo" 4=154
  \ottava #1
  b'8-.->\ff cis-. d-. e-. fis-> d-. fis4-. |
  eis8-.-> cis-. eis4-. e8-.-> c-. e4-. |
  \break
  b8-. cis-. d-. e-. fis-. d-. fis-. b-. |
  a-. fis-. d-. fis-. a2-> |
  b,8-> cis-. d-. e-. fis-> d-. fis4-. |
  eis8-> cis-. eis4-. e8-> c-. e4-. |
  \break
  b8-. cis-. d-. e-. fis-. d-. fis-. b-. |
  a-. fis-. d-. fis-. a2-> |
  fis8-._\markup {\italic { sempre stretto sin al fine } } gis-. ais-. b-. cis-> ais-. cis4-> |
  <d, fis d'>8-> ais'-. <d, fis d'>4-> |
  
  <cis fis cis'>8-> ais'-. <cis, fis cis'>4-> |
  \break
  
  fis8-. <fis g>-. <fis ais>-. <fis b>-. <cis fis cis'>8-> <cis fis ais>-. <cis fis cis'>4-> |
  <d fis d'>8-> <d fis ais>-. <d fis d'>4-> <cis fis cis'>2-> |
  
  fis8-. gis-. ais-. b-. cis-> ais-. cis4-> |
  
  <dis, fis dis'>8 ais'-. <dis, fis dis'>4-> <cis fis cis'>8-> ais'-. <cis, fis cis'>4-> |
  \break
  fis8-. <fis g>-. <fis ais>-. <fis b>-. <cis fis cis'>8-> <cis fis ais>-. <cis fis cis'>4-> |
  <d fis d'>8-> <d fis ais>-. <d fis d'>4-> <cis fis cis'>2-> |
  
  b8-. cis-. d-. e-. fis-> d-. fis4-> |
  eis8-> cis-. eis4-. e8-> c-. e4-. |
  \break
  b8-. cis-. d-. e-. fis-. b,-. fis'-. b-. |
  a-. fis-. d-. fis-. a2-> |
  
  b,8-. <b cis>-. <b d>-. <b e>-. <b fis'>-> <b d>-. <b d fis>4-. |
  <b eis>8-> <b cis>-. <b cis eis>4-. <b e>8-> <b c>-. <b c e>4-. |
  \break
  b8-. <b cis>-. <b d>-. <b e>-. <b fis'>-. <b d>-.  <b fis'>-. <b b'>-. |
  <b fis'>-. <b d>-.  <b fis'>-. <b b'>-. b2-> |
  r4 \appoggiatura { gis'16[ a ais] } <d, b'>4-.\sfz r2 |
  r4 \appoggiatura { gis16[ a ais] } <d, b'>4\sfz-.\ottava #0 r2 |
  \break
  b,8-. cis-. d-. e-. fis-. d-. fis-. b-. |
  ais-. fis-. ais-. cis-. b2-> |
  r4 \ottava #1 \appoggiatura { gis'16[ a ais] } <d, b'>4-.\sfz r2 |
  r4 \appoggiatura { gis16[ a ais] } <d, b'>4\sfz-. r2 |
  b8-. cis-. d-. e-. fis-. d-. fis-. b-. |
  \break
  ais-. fis-. ais-. cis-. b2-> |
  r4 \appoggiatura { gis16[ a ais] } <d, b'>4-.\sfz r2 |
  r4 \appoggiatura { gis16[ a ais] } <d, b'>4\sfz-. r2 |
  r4 \appoggiatura { gis16[ a ais] } <d, b'>4\pp \appoggiatura { gis16[ a ais] } <d, b'>4 \appoggiatura { gis16[ a ais] } <d, b'>4 |
  \appoggiatura { gis16[ a ais] } <d, b'>4\cresc \appoggiatura { gis16[ a ais] } <d, b'>4 \appoggiatura { gis16[ a ais] } <d, b'>4 \appoggiatura { gis16[ a ais] } <d, b'>4 \ottava #0 |
  r1 |
  r1 |
  r4\ff \ottava #1 \appoggiatura { gis16[ a ais] } <d, b'>4\ottava#0 r2 \bar "|." |
}
}

\include "../muziko.ly"
