\include "../ĉiea.ly"

\header {
  titolo-xx     = ""
  titolo-he     = "קן לציפור"
  titolo-eo     = "Birdonesto"
  komponisto-xx = ""
  komponisto-he = "יצחק אדל"
  komponisto-eo = "Jicĥak Edel"
  ikono         = "🐣"
}

\include "../titolo.ly"

melodio = {
  \tempo 4 = 80
  \time 4/4
  \key f \major
  a8 bes c' bes a4 g\4 |
  g8\4 f g\4 a g2\4 |
  g8\4 a bes g\4 g4\4 f |
  f8 e f g\4 a2 |
  a8 g8\4 a bes8 c'2 |
  bes8 a bes c' d'4. d'8 |
  c'4 bes8 a g4\4 d8 d |
  a8 g\4 f g\4 f2 |
  \bar "|."
}

\include "../muziko.ly"
