\include "../ĉiea.ly"

#(set-global-staff-size 25)

\header {
  titolo-xx     = "Game of Thrones"
  titolo-he     = "משחקי הכס"
  titolo-eo     = "Ludo de Tronoj"
  komponisto-xx = "رامین جوادی"
  komponisto-he = "ראמין ג׳ואדי"
  komponisto-eo = "Ramin Djawadi"
  ikono         = "💺"
}

\include "../titolo.ly"

melodio = {
  \key c \major
  \time 3/4
  \tempo 4 = 100
  \repeat unfold 4 {
	a'8 e'\2 f'16\2 g' a'8 e'\2 f'16\2 g' |
  }
  \break
  \repeat unfold 3 {
	a'8 e'\2 f'16\2 g' a'8 e'\2 f'16\2 g' |
  }
  a'8 e'\2 f'16\2 g' a'4. |
  \break
  e' a |
  c'16 d' e'4 a c'16 d' |
  b2. ~ |
  b |
  d'4. g |
  c'16 b d'4 g4. |
  c'16 b a2 ~ a8 ~ |
  a2. |
  \break
  e'4. a |
  c'16 d' e'4 a c'16 d' |
  b2. ~ |
  b |
  d'4. g |
  b8. c' b g |
  a2. ~ |
  a |
  \break
  a' |
  g' |
  a |
  e' |
  f |
  c'4. d' |
  e'2. ~ |
  e' |
  \break
  a' |
  g' |
  a |
  e' |
  f |
  c'4. b |
  a8 e f16 g a8 e f16 g |
  a8 e f16 g a8 e f16 g |
  a2. \bar "|."
}

\include "../muziko.ly"
