\include "../ĉiea.ly"

#(set-global-staff-size 24)

\header {
  titolo-xx     = ""
  titolo-he     = "ימים לבנים"
  titolo-eo     = "Blankaj tagoj"
  komponisto-xx = ""
  komponisto-he = "שלמה יידוב"
  komponisto-eo = "Ŝlomo Jidov"
  ikono         = " "
}

\include "../titolo.ly"

melodio = {
  \key f \major
  \time 2/2
  \partial 8 c8 |

  f4^\segno f8 g a4. a8 ~ |
  a bes c'4. r8 c' d' |
  \tuplet 3/2 { ees'4 d' c' } \tuplet 3/2 { bes4 a f } g2. r8 f |
  \break
  g4 f8 a ~ a4 g8 f ~ |
  f4 d8 e \tuplet 3/2 { f4 g a } |
  f2. r4 |
  r2 r4 f8 e |
  \break
  d2 f4 c' ~ |
  c' a8 g a4 r8 c' |
  ees'2 d'4 c'8 bes ~ |
  bes2 r4 d'8 e' |
  \break
  \tuplet 3/2 { f'4 ees' des' } \tuplet 3/2 { c'4\2 bes\2 a } |
  c'4 bes8 a \tuplet 3/2 { g4 f d } |
  \mark \markup { \musicglyph #"scripts.coda" }
  f1 ~ |
  f2. r8 c |
  \break

  \repeat volta 2 {
	f2 f4 g8 a ~ |
	a4 a8 bes c'4 r8 d' |
	\tuplet 3/2 { ees'4 d' c' } \tuplet 3/2 { bes4 a f } |
	g2. r4
	\break
	g f8 a ~ a4 g8 f ~ |
	f4 d8 e \tuplet 3/2 { f4 g a } |
	f4. g4 f8 d4 |
	r2 r4 f8 e |
	\break
	d4 f2 c'4 ~ |
	c' a8 g a4 c'8 d' |
	ees'2 d'4 c'8 bes ~ |
	bes2 r4 d'8 e' |
	\break
	\tuplet 3/2 { f'4 ees' des' } \tuplet 3/2 { c'4\2 bes\2 a } |
	c'4 bes8 a \tuplet 3/2 { g4 f d } |
	f1 ~ |
	f |
	\break
	r4 e'8-2 e' \tuplet 3/2 { e'4 f' e' } |
	d'4 a8 a a4 e'8 f' |
	e'4 d'8 e' \tuplet 3/2 { g'4 f' e' } |
	d'2 r4 d'8-2 d' |
	\break
	\tuplet 3/2 { d'4 c' d' } \tuplet 3/2 { ees'4 d' c' } |
	d'4 e'8 f' g'2 ~ |
	\tuplet 3/2 { g'4 f' ees' } \tuplet 3/2 { d'4 c'\2 bes\2 } |
	a1 |
	\break
	r4 d'8-2 d' \tuplet 3/2 { d'4 e' d' } |
	c'4 a8 g a4. a8 |
	d'4 g'8 g' \tuplet 3/2 { f'4 e' d' } |
	c'1\2 |
	\break
	c'4 bes8 d' ~ \tuplet 3/2 { d'4 c' d' } |
	a4. g8 \tuplet 3/2 { f4 d f } |
	g2 d'4 des'8 c' ~ |
	c'2. r8 c_\markup {\italic "D.S. al Coda"} |
  }
  \break

  \mark \markup { \musicglyph #"scripts.coda"}
  f1 ~ |
  f2 d'4 bes |
  c'1 ~ |
  c'2. r4 |
  \bar "|."
}

\include "../muziko.ly"
