\include "../ĉiea.ly"

\header {
  titolo-xx     = ""
  titolo-he     = "מאחורי ההר"
  titolo-eo     = "Malantaŭ la monto"
  komponisto-xx = ""
  komponisto-he = ""
  komponisto-eo = ""
  ikono         = "💬"
}

\include "../titolo.ly"

melodio = {
  \time 4/4
  c4 c c c |
  e4 g\4 e2 |
  g2\4 a2 |
  g1\4 |
  \break
  f4 f f f |
  e4 e e2 |
  d2 g2\4 |
  c1 |
  \break
  c4 c c e |
  g4\4 g\4 g2\4 |
  a2 a |
  g1\4 |
  \break
  f4 f f f |
  e4 e e2 |
  d2 g2\4 |
  c1 |
  \bar "|."
}

\include "../muziko.ly"
