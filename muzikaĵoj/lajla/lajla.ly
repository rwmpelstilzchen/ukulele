\include "../ĉiea.ly"

#(set-global-staff-size 24)

\header {
  titolo-xx     = ""
  titolo-he     = "לילה לילה"
  titolo-eo     = "Nokto, nokto"
  komponisto-xx = ""
  komponisto-he = "מרדכי זעירא"
  komponisto-eo = "Mordeĥaj Zeira"
  ikono         = "🌝"
}

\include "../titolo.ly"

melodio = {
  \time 3/4
  \key f \minor
  c( f\3) ees            | 
  f f c                | 
  g\4 aes\4 c          | 
  f2 f4                | 
  \break
  c( f\3) ees            | 
  f f ees              | 
  aes\4 bes g\4        | 
  aes2\4 bes4          | 
  \break
  c'2 bes4             | 
  c'8( bes) aes4\2 g\4 | 
  bes g\4 f            | 
  aes2.\4              | 
  \break
  c4( des) e           | 
  f g\4 aes\4          | 
  g\4 c' bes           | 
  des'2.               | 
  \break
  c'2 bes4             | 
  aes8\4( g\4) f4 g\4  | 
  c c aes\4            | 
  f2.                  | 
  \bar "||"
  \break

  ees2.%{(%}                      | 
  f%{\3)%}                        | 
  g8\4 aes16\4%{(%} g\4 f4 g\4%{)%} | 
  aes2.\4                   | 
  \break
  g4\4( aes\4) bes          | 
  c' aes\4 f                | 
  e8 f16 e d4 e             | 
  f2.                       | 
  \bar "|."
}

\include "../muziko.ly"
