\include "../ĉiea.ly"

\header {
  titolo-xx     = "Trollpolska"
  titolo-he     = "פולקה של הטרולים"
  titolo-eo     = "Polko de la troloj"
  komponisto-xx = ""
  komponisto-he = ""
  komponisto-eo = ""
  ikono         = "👺"
}

\include "../titolo.ly"

melodio = {
  \time 3/4
  \key d \minor
  \repeat volta 2 {
    a d8. e16 f8. g16\4 |
    a4 cis'\2 d'8.\2 e'16\2 |
    f'8.\2 g'16 f'8.\2 e'16\2 d'4\2 |
    c'8.\3 d'16\2 c'8.\3 bes16\3 a4 |
    bes8. c'16 bes8. a16 g8.\4 bes16 |
    a8. bes16 a8. g16\4 f4 |
    g8.\4 f16 e8. d16 cis e8. |
    d2. |
  }
  \break
  \repeat volta 2 {
    a'8. g'16 f'8. e'16 f'8. a'16 |
    g'8. a'16 g'8. e'16\2 c'4\2 |
    f'8. e'16\2 d'8.\2 cis'16\2 d'8.\2 e'16 |
    f'4 e'8. c'16\2 a4 |
    d'8. c'16 bes8. a16 bes8. d'16 |
    c'8. d'16 c'8. a16 f4 |
    g8.\4 f16 e8. d16 cis e8. |
    d2. |
  }
}

\include "../muziko.ly"
