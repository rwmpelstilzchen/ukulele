\include "../ĉiea.ly"

\header {
  titolo-xx     = "荒城の月"
  titolo-he     = "ירח מעל לטירה החרבה"
  titolo-eo     = "La luno sur la ruina kastelo"
  komponisto-xx = "瀧 廉太郎"
  komponisto-he = "רנטרו טקי"
  komponisto-eo = "Rentarō Taki"
  ikono         = "月"
}

\include "../titolo.ly"

melodio = {
  \key c \phrygianminor
  \time 4/4
  { g\4 g\4 c' d' } |
  { ees' d' c'2 } |
  { aes4 aes g\4 f } |
  { g2.\4 r4 } |
  \break
  { g\4 g\4 c' d' } |
  { ees' d' c'2 } |
  { aes4 f g4.\4 g8\4 } |
  { c2. r4 } |
  \break
  { ees4.\4 ees8\4 d4\3 c\3 } |
  { aes aes g2\4 } |
  { f4 g\4 aes4. aes8 } |
  { g2.\4 r4 } |
  \break
  { g\4 g\4 c' d' } |
  { ees' d' c'2 } |
  { aes4 f g4.\4 g8\4 } |
  { c2. r4 } |
  \bar "|."
}

\include "../muziko.ly"
