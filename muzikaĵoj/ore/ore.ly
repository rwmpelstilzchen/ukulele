\include "../ĉiea.ly"

\header {
  titolo-xx     = ""
  titolo-he     = "מחול השעות"
  titolo-eo     = "Danco de la horoj"
  komponisto-xx = ""
  komponisto-he = "אמילקרה פונקיילי"
  komponisto-eo = "Amilcare Ponchielli"
  ikono         = "⌚"
}

\include "../titolo.ly"

melodio = {
  \key bes \major
  \time 4/4
  \partial 4 { d8 f8 }
  f8 g8 r4 r4 g8 bes8 |
  bes8 a8 r4 r4 f8 g8 |
  g8 a8 r4 r4 a8\1 ees'8\2 |
  ees'8\2 d'8\2 r4 r4 d'8\2 bes'8 |
  bes'8 a'8 r4 r4 a8\1 g'8 |
  g'8 f'8\4 r4 r4 f8\2 d'8 |
  d'8 c'8 c8 a8 a8 g8 g8 g8 |
  g8 ges8 ges8 f8 r4 d8 f8 |
  f8 g8 r4 r4 g8 bes8 |
  \barNumberCheck #10
  bes8 a8 r4 r4 f8 g8 |
  g8 a8 r4 r4 a8\1 ees'8\3 |
  ees'8\3 d'8\3 r4 r4 d'8\3 d''8 |
  d''8 c''8 r4 r4 c''8 a'8 |
  a'8 g'8 r4 r4 g'8 ees'8\2 |
  ees'8\2 d'8\2 d'8\2 bes8\3 bes8\3 g8\4 g8\4 a8 |
  bes8 bes8 r4 r4 bes8\2 c'8\4 |
  \repeat unfold 4 {bes8\2 c'8\4} |
  d'16 ees'16 f'8 r4 r4 g'16 d'16\2 g'16 d'16\2 |
  g'4 f'16 ees'16\4 f'16 ees'16\4 f'4 g'16 ees'16\4 g'16 ees'16\4 |
  \barNumberCheck #20
  g'4 f'16 d'16\2 f'16 d'16\2 f'4 d'8\2 e'8 |
  \repeat unfold 4 {d'8\2 e'8} |
  f'16 g'16 a'8 r4 r4 d''16 a'16\2 d''16 a'16\2 |
  d''4 c''16 g'16\2 c''16 g'16\2 c''4 bes'16\4 g'16\2 bes'16\4 g'16\2 |
  bes'4\4 a'16 f'16\2 a'16 f'16\2 a'4 f'16\2 d'16\3 f'16\2 d'16\3 |
  f'4\2 a8 d'8 g8 bes8 c8 a8 |
  f4\3 f8\3 f'8 f4 d8 f8 |
  f1 |
  \bar "|."
}

\include "../muziko.ly"
