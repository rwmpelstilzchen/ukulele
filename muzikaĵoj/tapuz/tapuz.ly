\include "../ĉiea.ly"

\header {
  titolo-xx     = ""
  titolo-he     = "קלפתי תפוז"
  titolo-eo     = "Mi senŝeligis oranĝon"
  komponisto-xx = ""
  komponisto-he = "יוני רכטר"
  komponisto-eo = "Joni Reĥter"
  ikono         = "👶"
}

\include "../titolo.ly"

melodio = {
  \tempo 4 = 100
  \time 4/4
  \key g \minor
  \partial 16*5 d16 d d f bes ~ |
  \repeat volta 2 {
    bes8 r r4 r8 r16 d d d f g\4 ~ |
    g8\4 r r4 r d'16 d' d' c' ~ |
    c'4 bes16 bes bes a ~ a4 g16\4 g\4 f d ~ |
    d4 r r8 r16 d d d f bes ~ |
	\break
    bes4 r r16 f f f d' c' bes bes ~ |
	bes8. f16 d' c' bes bes ~ bes g8\4 f16 g\4 g\4 a bes ~ |
	bes4 r r8 r16 f g\4 g\4 f g\4 ~ |
  }
  \alternative {
	{
	  g4\4 r r8 r16 d d d f bes\laissezVibrer |
	}
	{
	  g4\4\repeatTie r r8 r16 g\4 g8\4 bes |
	}
  }
  \pageBreak
  \repeat volta 2 {
	c'16 d'8. r4 r8 r16 c' d'8 g16\4 bes ~ |
	bes4 r r8 r16 g\4 g\4 g\4 bes\2-2 c'\2 ~ |
	c'2\2 d'8 f'16 d' ~ d' c'\2 d'8 |
	r2 r8 r16 f d'8 c'16 bes ~ |
	bes4 r r8 a16 a g\4 g\4 f g\4 |
	f4 r r8 r16 d d' d' d' c' ~ |

  }
  \alternative {
	{ 
	  c'4 bes16 bes bes a ~ a4 g16\4 g\4 f d |
	  r2 r8 r16 g\4 g8\4 bes |
	}
	{
	  c'4\repeatTie bes16 bes bes a ~ a4 g16\4 g\4 f g\4 ~ |
	  g2\4 r |
	}
  }
  \bar "|."
}

\include "../muziko.ly"
