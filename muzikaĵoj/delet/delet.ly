\include "../ĉiea.ly"

\header {
  titolo-xx     = ""
  titolo-he     = "דלת הקסמים"
  titolo-eo     = "La magia pordo"
  komponisto-xx = ""
  komponisto-he = "נורית הירש"
  komponisto-eo = "Nurit Hirŝ"
  ikono         = "👁"
}

\include "../titolo.ly"

melodio = {
  \key ees \major
  \time 4/4
  \repeat volta 2 {
    g\4 g8\4 ees bes aes g4\4 |
    f g8\4 c r4 r8 c |
    ees4 ees8 c ees4 ees8 c |
    c' bes bes g\4 bes4 r |
    \break
	g\4 g8\4 g\4 g\4 aes\4 g\4 d |
    f4 ees r c8 ees |
    f4 r ees8 d c d |
    f4 ees r r8 g\4 |
    \break
	bes4 bes8 g\4 bes4 bes8 g\4 |
    c'4 c'8 ees r4 r8 ees |
    aes4\4 g\4 f ees |
    g\4 f8 ees bes4 r |
    \break
	bes bes8 g\4 bes4. g8\4 |
    c'4 c'8 ees ~ ees4 r8 ees |
    aes4 g8\4 g\4 f4 ees8 ees |
  }
  \alternative {
	{
	  f4. ees8 ees4 r |
    }
	{
	  f2. r8 ees |
	  ees4 r r2 |
	}
  } \bar "|."
}

miamelodio = { % משמיעה
  \key f \major
  c' c'8 a c' c' a4 |
  d' d'8 f2 f8 |
  g4\4 g8\4 f g4\4 g8\4 f |
  d' c' c' a c'2 |
  \break
  a4 a8 a a bes a e |
  g4\4 f2 d8 e |
  g2\4 g8\4 g\4 d e g4\4 f2. |
  \break
  a8 c'4 c'8 a c'4. |
  a8 d'4 d'8 f4. f8 |
  bes4 a g\4 f |
  a g8\4 f c'2 |
  \break
  c'4 c'8 a c'4. a8 |
  d'4 d'8 f2 f8 |
  bes4 a8 g\4 g4\4 f |
  g2\4 e4 f |
  \bar "|."
}

\include "../muziko.ly"
