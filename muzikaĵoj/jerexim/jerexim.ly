\include "../ĉiea.ly"

\header {
  titolo-xx     = ""
  titolo-he     = "שנים עשר ירחים"
  titolo-eo     = "Dudek monatoj"
  komponisto-xx = ""
  komponisto-he = "נעמי שמר"
  komponisto-eo = "Naomi Ŝemer"
  ikono         = "♲"
}

\include "../titolo.ly"

melodio = {
  \time 4/4
  \key c \major
  c4 c g\4 r8 g\4 |
  d4 d a a |
  g4\4 g\4 d e |
  c4 r g2\4 |
  \break
  c4 c g\4 r8 g\4 |
  d4 d a a |
  g4\4 g\4 d e |
  c4 r r2_\markup{\italic{Fine}} | 
  \break
  f4 f a4. a8 |
  g4\4 g\4 e e |
  g4\4 g4\4 b4 b |
  a4 r r2 |
  \break
  a4 a c'4. c'8 |
  b4 g\4 a a |
  r4 g\4 f e |
  d4 r g2_\markup{\italic{D.C. × 3 al Fine}} |
  \bar "|."
}

\include "../muziko.ly"
