\include "../ĉiea.ly"

\header {
  titolo-xx     = ""
  titolo-he     = "נדנדה"
  titolo-eo     = "Baskulo"
  komponisto-xx = ""
  komponisto-he = "דניאל סמבורסקי"
  komponisto-eo = "Daniel Samburski"
  ikono         = "⚖"
}

\include "../titolo.ly"

melodio = {
  \key c \major
  \time 4/4
  c4 a f2 |
  c4 a f2 |
  g4. a8 g4 f |
  e4 d e c |
  \break
  c4 a f2 |
  c4 a f2 |
  g4. g8 a4 b |
  c'4 c' c'2 |
  \break
  g4. a8 g4 f |
  e4 d e c |
  g4. a8 g4 f |
  e4 d e c |
  \break
  %c4 a f2 |
  %c4 a f2 |
  %g4. a8 g4 f |
  %e4 d c2 |
  c4 a f2 |
  c4 a f2 |
  g4. g8 a4 b |
  c'4 c' c'2 |
  \bar "|."
}

\include "../muziko.ly"
