\include "../ĉiea.ly"

\header {
  titolo-xx     = ""
  titolo-he     = "פרפר נחמד"
  titolo-eo     = "Argrabla papilio"
  komponisto-xx = ""
  komponisto-he = "נורית הירש"
  komponisto-eo = "Nurit Hirŝ"
  ikono         = "🦋"
}

\include "../titolo.ly"

melodio = {
  \time 4/4
  \key c \major
  e4 g g4. a8 |
  %\tuplet 3/2 {d4 c8} \tuplet 3/2 {d4 a,8} c4 r8 c |
  \tuplet 3/2 {d4 c8} \tuplet 3/2 {d4 c8} c4 r8 c |
  %d4 c8 c4. r8 c8 |
  d4 d d r8 e |
  %\tuplet 3/2 {d4 c8} \tuplet 3/2 {d4 a,8} c4 r8 c |
  \tuplet 3/2 {d4 c8} \tuplet 3/2 {d4 c8} c4 r8 c |
  %d4 c8 c4. r8 c8 |
  e4 g g4. a8 |
  d4 c8 c8~ c4 r8 c8 |
  d4 d d d |
  a4 g a b |
  c'4. c'8 \tuplet 3/2 {b4 b8} \tuplet 3/2 {c'4 b8} |
  a4 r r r8 e |
  a4. a8 \tuplet 3/2 {a4 a8} \tuplet 3/2 {b4 c'8} |
  %b1~ |
  b2. g4 |
  \break
  \repeat volta 2 {
	c'4 c' c' g |
	a c'8 c'~ c'4 r |
	c'4 c' bes g |
	f ees8 c~ c4 r |
	ees4 c ees c |
	g4( f2) ees4 |
  }
  \alternative {
	{
	  c2. r4 |
	  r2 r4 g |
	}
	{
	  c'1~ |
	  c'2. r4 |
	}
  }
  \bar "|."
}

\include "../muziko.ly"
