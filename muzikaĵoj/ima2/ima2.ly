\include "../ĉiea.ly"

\header {
  titolo-xx     = ""
  titolo-he     = "אמא, הו אמא"
  titolo-eo     = "Patrino, ho patrino"
  komponisto-xx = ""
  komponisto-he = "שייקה פייקוב"
  komponisto-eo = "Ŝajke Fajkov"
  ikono         = "🌐"
}

\include "../titolo.ly"

melodio = {
  \time 4/4
  \key f \minor
  \tempo 4 = 100
  \repeat volta 2 {
    c8 des4 c8 aes g4\4 f8 |
    ees ees4 des8 c2 |
    c8 des4 c8 g4.\4 f16 des |
    c1 |
    \break
	c8 des4 c8 c' c'4 bes8 |
    des'4 des'8 c' bes2 |
    aes4 g8\4 aes g4.\4 c16 g\4 |
    f1 |
  }
  \break
  aes8^\segno g4\4 f8 aes g4\4 f8 |
  aes aes4 g8\4 des2 |
  g8\4 g4\4 f8 aes4. g16\4 aes |
  g\4 aes ~ aes4. ~ aes2 |
  \break
  aes8 g4\4 f8 aes g4\4 f8 |
  des' des'4 c'8 bes2 |
  aes8 c4 aes8 g4.\4 c16 g\4 |
  f1_\markup{\italic{"Fine"}} |
  \bar "||"
  \break

  aes8 g4\4 f8 aes g4\4 f8 |
  aes aes4 g8\4 des2 |
  g8\4 g4\4 f8 aes aes4 g8\4 |
  c1 |
  \break
  c8 des des4 c8 c'4 bes8 |
  des' des'4 c'8 bes2 |
  aes8 aes c4 aes8 g4.\4 |
  f8 f4. ~ f2_\markup{\italic{"D.S. al Fine"}} |
  \bar "|."
}


aminormelodio = \displayLilyMusic\transpose e c {
  \time 4/4
  \key a \minor
  \tempo 4 = 100
  \repeat volta 2 {
	e8 f4 e8 c'8 b4 a8 |
	g8 g4 f8 e2 |
	e8 f4 e8 b4. a16 f |
	e1 |
	\break
	e8 f4 e8 e'8 e'4 d'8 |
	f'4 f'8 e' d'2 |
	c'4 b8 c' b4. e16 b |
	a1 |
  }
  \break
  c'8^\segno b4 a8 c' b4 a8 |
  c' c'4 b8 f2 |
  b8 b4 a8 c'4. b16 c' |
  b16 c'16 ~ c'4. ~ c'2 |
  \break
  c'8 b4 a8 c' b4 a8 |
  f'8 f'4 e'8 d'2 |
  c'8 e4 c'8 b4. e16 b |
  a1_\markup{\italic{Fine}} |
  \bar "||"
  \break
  c'8 b4 a8 c' b4 a8 |
  c' c'4 b8 f2 |
  b8 b4 a8 c'8 c'4 b8 |
  e1 |
  \break
  e8 f8 f4 e8 e'4 d'8 |
  f'8 f'4 e'8 d'2 |
  c'8 c' e4 c'8 b4. |
  a8 a4. ~ a2_\markup{\italic{D.S. al Fine}} |
  \bar "|."
}

\include "../muziko.ly"
