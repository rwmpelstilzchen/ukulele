\include "../ĉiea.ly"

#(set-global-staff-size 27)

\header {
  titolo-xx     = "星のカービィ"
  titolo-he     = "קירבי"
  titolo-eo     = "Kirby"
  komponisto-xx = "石川 淳"
  komponisto-he = "ג׳וּן אישיקווה"
  komponisto-eo = "Ĝun Iŝikaŭa"
  ikono         = "🌀"
}

\include "../titolo.ly"

melodio = {
  \key f \major
  \tempo 4 = 160
  \partial 4 c8. c16 |
  \repeat volta 2 {
	f2.\3-2 f8.\3 a16\2 |
	c'8\2 f' e' d' c'4 a8. c'16 |
	bes4\2 g8.\4 g16\4 g4\4 a8. g16\4 |
	f2.\3 c8. c16 |
	\break
	f2.\3 f8.\3 a16\2 |
	c'8\2 f' e' d' c'4 a8. c'16 |
	bes4\2 g8.\4 g16\4 g4\4 a8. g16\4 |
	f1\3 |
	\break
	\interim
	f8. f16 g8\4 a4 f8 g\4 f |
	r c c c c c c c |
	r c c c c c \deinterim f8. g16\4 |
	\break
	\key f \minor
	aes4 g8.\4 aes16 bes4 aes8. bes16 |
	c'4 bes8. c'16 f4 f8. g16\4 |
	aes4 g8.\4 aes16 bes4 aes8. bes16 |
	c'2 f'8 r f8. g16\4 |
	\break
	aes4 g8.\4 aes16 bes4 aes8. bes16 |
	c'4 bes8. c'16 f4 f8. g16\4 |
	aes4 g8.\4 aes16 bes4 aes8. bes16 |
	g2\4 c'4 c8. c16 |
	\mark "ad lib."
  }
}

\include "../muziko.ly"
