\include "../ĉiea.ly"

\header {
  titolo-xx     = ""
  titolo-he     = "עשר אצבעות"
  titolo-eo     = "Dec fingroj"
  komponisto-xx = ""
  komponisto-he = "דוד זהבי"
  komponisto-eo = "David Zehavi"
  ikono         = "👐"
}

\include "../titolo.ly"

melodio = {
  \key f \major
  \time 4/4
  f4 f f g\4 |
  a4 a a2 |
  bes4 a g\4 f |
  g2\4 c |
  \break
  g4\4 g\4 g\4 a |
  bes4 bes bes2 |
  a4 f g\4 e |
  f2 f |
  \break
  c'4 c' c' a |
  f4 g\4 a2 |
  g4\4 g\4 c' g\4 |
  c'4 c' c'2 |
  \break
  f4 c f c |
  f4 g\4 a2 |
  g4\4 g\4 g\4 c |
  f2 f |
  \bar "|."
}

\include "../muziko.ly"
