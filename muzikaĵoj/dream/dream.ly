\include "../ĉiea.ly"

\header {
  titolo-xx     = "Dream a Little Dream of Me"
  titolo-he     = "שמור לך חלום קטן"
  titolo-eo     = "Sonĝetu pri mi"
  komponisto-xx = ""
  komponisto-he = "וילבור שוואנט ופאביאן אנדרה"
  komponisto-eo = "Fabian Andre kaj Wilbur Schwandt"
  ikono         = "💭"
}

\include "../titolo.ly"

nomono = {
  f4 a8 f b4 b8 b |
  aes4 f8 g g < c g > < c g > < c g > |
  f f f f e4 ees |
  d < d c' >8 < d a > ~ ~ < d a >4 d'8 c' |
  < f bes >4 < f bes >8 r < f bes >4 < f bes >8 r |
  < f bes >4 < f bes >8 r f f' f f' |
  < f a >4 < f a > < des aes >8 r < c bes > r |
  < f a > c f r r2 |
  r8. d''16 ~ d''4 \tuplet 3/2 { b'4 d'' b' } |
  d''8. b'16 \tuplet 3/2 { d''8 b' d'' } b'8. a'16 ~ a'4 |
  r d'' \tuplet 3/2 { \acciaccatura { a'8*3/2 } b'4 d'' \acciaccatura { a'8*3/2 } b'4 } |
  a'1 |
  r8. d''16 ~ d''4 \tuplet 3/2 { b'4 d'' b' } |
  \tuplet 3/2 { a'4 fis' a' } b4 cis' |
  r d'' a'8 fis' ~ fis'4 |
  \tuplet 3/2 { f'4 des'' f' } \tuplet 3/2 { e'4 c''2 } |
  r4 f' e'8 f' e' d' |
  f'4. e'8 ~ e'2 |
  r4 f' e'8 f' e' d' |
  \tuplet 3/2 { fis'4 a'4. r8 } \tuplet 3/2 { a'8 fis' ees' } ees'8 d' |
}

melodio = {
  \key g \major
  \time 4/4
  g2\4 fis8 g\4 fis e |
  g4.\4 fis8 ~ fis2 |
  r4 g\4 fis8 g\4 fis e |
  gis b4 gis8 f e ~ e4 |
  \break
  r a g8\4 a g\4 e |
  \tuplet 3/2 { a8 g\4 ees ~ } ees8 g\4 ~ g2\4 |
  d8 e ~ \tuplet 3/2 { e8 g\4 a } bes4 bes8 a ~ |
  a1 |
  \break
  a8\rest g4.\4 fis8 g\4 fis e |
  r g4.\4 fis2 |
  r4 g\4 fis8 g\4 fis e |
  gis b4 gis8 f e ~ e4 |
  \break
  r8 a4. g8\4 a g\4 e |
  \tuplet 3/2 { a4 g\4 ees } g2\4 |
  d8 e ~ \tuplet 3/2 { e8 g\4 a } bes4 a8 g\4 ~ |
  g2\4 r |
  \break
  r8 e' ~ e'4 \tuplet 3/2 { cis'4 e' cis' } |
  \tuplet 3/2 { b4 gis b } cis4 dis |
  r e' \tuplet 3/2 { cis'4 e' cis' } |
  b1 |
  \break
  r8 e' ~ e'4 \tuplet 3/2 { cis'4 e' cis' } |
  \tuplet 3/2 { b4 gis b } cis4 dis |
  r \grace { dis'8( } e'4) b8 gis ~ gis4 |
  dis'2 d' |
  \break
  r8 g4.\4 fis8 g\4 fis e |
  g2\4 fis |
  r8 g4\4 fis8 ~ fis g\4 fis e |
  gis b4 gis8 f e ~ e4 |
  \break
  r8 a4. g8\4 a g\4 e |
  \tuplet 3/2 { a4 g\4 ees } g2\4 |
  d8 e ~ \tuplet 3/2 { e8 g\4 a } bes4 a8 g\4 ~ |
  g2\4 r |
  \break
  r4 r8 a g\4 a g\4 bes |
  a g\4 bes a g2\4 |
  d8 e ~ \tuplet 3/2 { e8 g\4 a } bes4 a |
  g\4 r r2 \bar "|."
}

\include "../muziko.ly"
