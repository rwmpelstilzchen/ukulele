\include "../ĉiea.ly"

\header {
  titolo-xx     = ""
  titolo-he     = "ולס להגנת הצומח"
  titolo-eo     = "Vals por la konservado de la plantaro"
  komponisto-xx = ""
  komponisto-he = "נעמי שמר"
  komponisto-eo = "Naomi Ŝemer"
  ikono         = "¾"
}

\include "../titolo.ly"

melodio = {
  \tempo 4 = 240
  \time 3/4
  \key f \major
  a r c |
  a2. ~ |
  a4 g\4 f |
  a r r |
  r d d |
  a2 bes4 |
  a2. |
  g\4 |
  \break g4\4 r c |
  g2.\4 ~ |
  g4\4 f e |
  g\4 r r |
  r c c |
  g2\4 a4 |
  f r r |
  R2. |
  \break a2 c4 |
  a2. ~ |
  a4 g\4 f |
  a2. ~ |
  a4 a a |
  d'2 a4 |
  a2. |
  g\4 |
  \break g2\4 c4 |
  g2.\4 ~ |
  g4\4 f e |
  g\4 r r |
  r c c |
  g2\4 a4 |
  f r r |
  R2. |
  \break
  f'2.-3 |
  e' |
  d' ~ |
  d'4 e' f' |
  e'2. ~ |
  e'2 b4\2 |
  d'2. |
  c'\2 |
  \break
  ees'-3 |
  d' |
  c' ~ |
  c'4 d' ees' |
  d'2. ~ |
  d'2 c'4 |
  bes\2 r r |
  R2. |
  \break
  bes\2 |
  a |
  g\4 ~ |
  g4\4 a bes\2 |
  c'2.\2 ~ |
  c'2\2 c'4\2 |
  f'2. |
  f\3 |
  \break
  g4\4 r r |
  r r bes |
  a r r |
  r c a |
  g\4 r r |
  r r c' |
  f r r |
  R2._\markup{\italic{"D.C. × 2"}} |
  \bar "||"
  \break
  f'-3 |
  e' |
  d' |
  d'4 c'\2 d' |
  b2.\2 |
  e'2 b4\2 |
  d'2. |
  c'\2 |
  \break
  ees'2-2 ees'4 |
  d'2 d'4 |
  c'2\2 c'4\2 |
  c'2\2 ees'4 |
  d'2 ees'4 |
  d' a\2 c'\2 |
  bes\2 r r |
  R2. |
  \break
  bes\2 |
  a |
  g\4 ~ |
  g4\4 a\2 bes\2 |
  c'2.\2 ~ |
  c'2\2 c'4\2 |
  f'2.\1 |
  f\3 |
  \break
  g4\4 r r |
  r r bes |
  a r r |
  r c a |
  g\4 r r |
  r r c' |
  f r r |
  R2. |
  \bar "|."
}


\include "../muziko.ly"
