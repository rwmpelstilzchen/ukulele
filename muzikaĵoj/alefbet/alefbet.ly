\include "../ĉiea.ly"

\header {
  titolo-xx     = ""
  titolo-he     = "אל״ף־בי״ת"
  titolo-eo     = "Alfabeto"
  komponisto-xx = ""
  komponisto-he = "נעמי שמר"
  komponisto-eo = "Naomi Ŝemer"
  ikono         = "ℵ"
}

\include "../titolo.ly"

sheshsheva = {
  e8 e d d c c d d |
  e8 e c'4 b8 b a4 |
  e8 e d d c c d d |
  e8 e c'4 b8 b a4 |
  g8\4 g\4 g\4 g\4 a a a a |
  g4\4 g\4 fis8 fis e4 |
  g8\4 g\4 g\4 g\4 a b c' a |
  b4 g\4 c' r |
}

melodio = {
  \time 4/4
  \key c \major
  g8\4 a g\4 c d e f d |
  g8\4 a g\4 c f e d4 |
  g8\4 a g\4 c d e f g\4|
  a8 b c' a b b a4 |
  \xNote {b8 b b4 b8 b b b} |
  d'8 d' b d' c' c' a c' |
  b8 b g8.\4 e16 g8\4 g\4 a4 |
  d'8 d' d' d' c' c' c' c' |
  b8 a g\4 f e4 d |
  << {\xNote{b4 b b8 b b4}} \\ {c4 s s2} >> |
  \break
  \sheshsheva
  \break
  \xNote {b8 b b4 b8 b b b} |
  \xNote {b4 b b8 b b4} |
  g8\4 a g\4 c d e f d |
  g8\4 a g\4 c f e d4 |
  g8\4 a g\4 c d e f g\4 |
  a8 b c' a b b a4 |
  \xNote {b4 b8 b b4 b} |
  d'8 d' b d' c' c' a c' |
  b8 b g8.\4 e16 g8\4 g\4 a4 |
  d'8 d' d' d' c' c' c' c' |
  b8 a g\4 f e4 d |
  << {\xNote{b8 b b b b b b b}} \\ {c4 s4 s2} >> |
  \xNote{b8 b b b b b b b} |
  g4\4 g\4 g\4 g\4 |
  \break
  \sheshsheva
  \break
  \xNote{b8 b b b b b b4} |
  \xNote{b8 b b8. b16 b8 b b4} |
  g8\4 a g\4 c d e f g\4 |
  a8 b c' a b b a4 |
  d'8 d' b d' c' c' a c' |
  b8 b g8.\4 e16 g8\4 g\4 a4 |
  d'8 d' b d' c' c' a c' |
  b8 a g\4 f e4 d |
  << {\xNote{b8 b b b b4 b8 b8}} \\ {c4 s4 s2} >>  |
  \xNote{b4 b b b} |
  \break
  e8 e d d c c d d |
  e8 e c'4 b8 b a4 |
  e8 e d d c c d d |
  e8 e c'4 b8 b a4 |
  g8\4 g\4 g\4 g\4 a a a a |
  g4\4 g\4 fis8 fis e4 |
  g8\4 g\4 g\4 g\4 a b c' a |
  b2. g4\4 |
  c'1~ |
  c'4 r4 r2 |
  \bar "|."
}

\include "../muziko.ly"
