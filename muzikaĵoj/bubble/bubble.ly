\include "../ĉiea.ly"

\header {
  titolo-xx     = "バブルボブル"
  titolo-he     = "באבל בובל"
  titolo-eo     = "Bubble Bobble"
  komponisto-xx = "君島正"
  komponisto-he = "טדשי קימיג׳ימה"
  komponisto-eo = "Tadaŝi Kimiĝima"
  ikono         = "Ⓔ"
}

\include "../titolo.ly"

melodio = {
  \key f \major
  \time 4/4
  \interim
  bes4. bes8 ~ bes a g4 |
  a8 bes4 c'8 f2 |
  g4. d8 ~ d d a4 |
  bes-3\3 c'4\2 d'\2 e'4 |
  \deinterim
  \repeat volta 2 {
	f'8-2 e' d'8.\2 c'16\2 e'8 d'\2 c'\2 bes\3 |
	d'8\2 c'\2 bes16\3 a8\3 c'16\2 ~ c'4. a16 g\3 |
	f8\3 g\3 a bes-3\3 g\3 a16\3 bes8.\3 c'8\2 |
  }
  \alternative {
	{
	  c'8\2 d'\2 e'16 d'8.\2 c'8\2 c'\2 d'\2 e' |
	}
	{
	  c'8\2 d'\2 e'16 c'8.\2 f'8 c'8\2 d'\2 dis'\4 |
	}
  }
  \repeat volta 2 {
	e'8 c d dis e c'\2 d'\2 e' |
	f'8 c d e f c'\2 d'\2 e' |
	g'8 c d e g c'\2 d'\2 e'\2 |
	a'8 c d e a f'\2 g' a' |
	bes'4. bes'4 a'8 g'4 |
	a'2. a'4 |
  }
  \alternative {
	{
	  g'4.  d'8\2 ~ d'4 a' |
	  g'2 ~ g'8 c'8\2 d'\2 dis'\4 |
	}
	{
	  g'4.  c'8\3 ~ c' a' c'\3 a' |
	  f'2\2 ~ f'8\3 r4. |
	}
  }
  \bar "|."
}

\include "../muziko.ly"
