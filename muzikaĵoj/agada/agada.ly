\include "../ĉiea.ly"

\header {
  titolo-xx     = ""
  titolo-he     = "אגדה"
  titolo-eo     = "Fabelo"
  komponisto-xx = ""
  komponisto-he = "חנינא קַרְצֶ׳בְסְקִי"
  komponisto-eo = "Ĥanina Karĉevski"
  ikono         = "🌴"
}

\include "../titolo.ly"

melodio = {
  \key f \minor
  <c'\1>4 <f\2>4 <c'\1>4 <f\2>8 <c'\1>8 |
  <bes\1>2 <bes\1>2 |
  <bes\1>4 <f\2>4 <bes\1>4 <aes\4>8 <f\2>8 |
  <aes\4>4 <bes\1>4 <c'\1>2 |
  <c'\1>4 <f\2>4 <c'\1>4 <f\2>8 <c'\1>8 |
  <bes\1>2 <bes\1>2 |
  <aes\4>4 <aes\4>4 <g\4>4 <ees\3>8 <c\3>8 |
  <f\2>2 <f\2>2_\markup{\italic{Fine}} |
  \break

  \repeat volta 2 {
	<ees'\1>4 <c'\4>4 <ees'\1>4 <c'\4>8 <ees'\1>8 |
	<f'\1>2 <f'\1>4. <f'\1>8 |
	<ees'\1>4 <c'\4>8 <ees'\1>8 <des'\4>4 <bes\2>8 <des'\4>8 |
  }
  \alternative {
	{ <c'\2>2 <c'\2>4 <c'\2>4 | }
	{ <c'\2>2 <c'\2>2_\markup{\italic{D.C. al Fine}} | }
  }
  \bar "|."
}

\include "../muziko.ly"
