\include "../ĉiea.ly"

\header {
  titolo-xx     = ""
  titolo-he     = ""
  titolo-eo     = ""
  komponisto-xx = ""
  komponisto-he = ""
  komponisto-eo = ""
  ikono         = ""
}

\include "../titolo.ly"

melodio = {
  \key bes \major
  \time 4/4
  d'\mp c' bes4. g16 a |
  bes8 a g f2 c'16 d' |
  ees'8 d' bes d' c'4 d'8 bes |
  ges4 ees ges8 f ees d |
  d bes ~ bes2. ~ |
  bes r8 f\p |
  \break
  d'4 d'8 c' bes4 bes8 a |
  g a8. bes8 c'16 f4. f8 |
  g a bes g g16 f8 bes8. d'8 |
  ees' d' bes d' c'4. d'8 |
  f'4. g'8 f'16 ees'8 ees'8. d'8 |
  d' c'8. c'8 d'16 ees'4. d'16 c' |
  bes4. f8 g16 bes ~ bes4. |
  c'4 d' ees'8 c'4. ~ |
  c'2 ~ c'4. f8\mp |
  d'4 d'8 c' bes4 bes8 a |
  g a bes c' f4. f8 |
  g a bes g g16 f8 bes8. d'8 |
  ees' d'8. bes8 d'16 c'4. d'8 |
  f' f' f'8. g'16 f' ees'8. ees'8 d' |
  d' c'8. c'8 d'16 ees'4. d'16 c' |
  bes4. f8 g16 bes ~ bes4 c'8 |
  d'16 c'8 bes16 ~ bes2. |
  r2 ees'8 d' c' bes |
  a16 bes c'8 ~ c'2. |
  r4. d'8 f' ees' d' c' |
  b c'16 d' ~ d'2. |
  ees'4 d' c'2 |
  r8 d'\< ees' d'16 c'8. bes8 a bes |
  d'2. f'8 ees' |
  c'2 ~ c'4. f8\!\mf |
  bes4 c'8 d' a2 |
  a8 bes a f g2 |
  ees'8 d' d' c' c' bes4 g8 |
  a bes16 c' ~ c'2. |
  bes4 c'8 d' f'4 ~ \tuplet 3/2 { f'8 d' a } |
  g1 |
  ees'8 d' d' c' c'2 |
  f'8 ees'16 ees'8. d'16 d'8. c'8 ~ \tuplet 3/2 { c'8 d' ees' } |
  f'1 |
  r4 r8 d'\mp ees' d' ~ \tuplet 3/2 { d'8 a c' } |
  c'8 bes ~ bes2. |
  c'2 c'8 bes16 a8. g8 |
  f4. d8 f d c' bes ~ |
  bes4 a g f |
  d'4. f'8 bes d' c' c'16 bes |
  a4. a16 bes c'8 bes a g |
  f2 r |
  R1 |
  r16 d\mf ees f g8 f g bes bes4\> |
  ees8 bes a4. g8 f16 ees d8 |
  c bes, f4.\! r8 r4 |
  r2 r4 r8 f\mp |
  d'4 d'8 c' bes4 bes8 a |
  g a bes c' f2 |
  g8 a bes g g16 f8 bes8. d'8 |
  ees' d'8. bes8 d'16 c'4. d'8 |
  f'4. g'8 f'8. ees' d'8 |
  d' c'8. c'8 d'16 ees'4. d'16 c' |
  bes4. f8 bes4 bes16 c' d'8 |
  c' bes2. r16 r |
  r2 ees'8 d' c' bes |
  a16 bes c'8 ~ c'2. |
  r4. d'8 f' ees' d' c' |
  b c'16 d' ~ d'2. |
  ees'4 d' c'2 |
  r8 d' ees' d'16 c'8. bes8 a bes |
  d'2.\< f'8 ees' |
  c'2 ~ c'4. f8\!\mf |
  bes4 c'8 d' a2 |
  a8 bes a f g2 |
  ees'8 d' d' c' c' bes4 g8 |
  a bes16 c' ~ c'2. |
  bes4 c'8 d' f'4 d'8 a |
  g1 |
  ees'8 d' d' c' c'2 |
  f'8 ees'16 ees'8. d'16 d'8. c'8 ~ \tuplet 3/2 { c'8 d' ees' } |
  f'1 |
  r4 r8 d' ees' d' ~ \tuplet 3/2 { d'8 a c' } |
  c'8 bes ~ bes2 f8\f c |
  f c d a, d a, ees f |
  g ees ees c < ees ees ees > c d bes, |
  d bes, f c a f f f16 g |
  a f g a d8 bes, d bes, f a, |
  f a, g f g bes f g, |
  ees g, c d, c d, c ees,16 f, |
  g, f, g, bes, ees,8 d, ees ees, g g, |
  bes bes, r ees16 c d ees f d ees f g a |
  bes c' d' ees' f'4. \breathe r8 r4 |
  r4. r4 f8\mp bes c' |
  d' f' c'2 c'8 bes16 a ~ |
  a8 g f4. d8 f d |
  c' bes ~ bes4 a g |
  f g2 ees'4^\markup \line { \italic "rit."} |
  d' c' bes a |
  f2 bes, ~ |
  bes,1 \bar "|."
 
}


oldmelodio = \transpose c bes, \relative e' {
  %\tempo 4 = 70
    \clef "treble" \key c \major \numericTimeSignature\time 4/4 | % 1
    e4 ^\markup{ \bold {Adagio} } \mp d4 c4. a16  b16  | % 2
    c8  b8 a8  g2 d'16  e16  | % 3
    f8  e8 c8 e8  d4 e8  c8  | % 4
    as4 f4 as8  g8 f8 e8   | % 5
    e8  c'8 ~  c2. ~ | % 6
    c2. ^\markup{ \bold {Adagio} } r8 g8 \p | % 7
    e'4 e8  d8  c4 c8  b8  | % 8
    a8  b8. c8 d16  g,4. g8  | % 9
    a8  b8 c8 a8  a16  g8 c8. e8  | 
    f8  e8 c8 e8  d4. e8 | % 11
    g4. a8 g16  f8 f8. e8  | % 12
    e8  d8. d8 e16  f4. e16  d16   | % 13
    c4. g8 a16  c16 ~  c4. | % 14
    d4 e4 f8 d4. ~ | % 15
    d2 ~ d4. g,8 \mp | % 16
    e'4 e8  d8  c4 c8  b8  | % 17
    a8  b8 c8 d8  g,4. g8  | % 18
    a8  b8 c8 a8  a16  g8 c8. e8  | % 19
    f8  e8. c8 e16  d4. e8 | 
    g8  g8  g8.  a16  g16  f8.  f8  e8  | % 21
    e8  d8. d8 e16  f4. e16  d16   | % 22
    c4. g8 a16  c16 ~  c4 d8 | % 23
    e16  d8 c16 ~  c2. | % 24
    r2 f8  e8 d8 c8  | % 25
    b16  c16 d8 ~  d2.  | % 26
    r4. e8 g8  f8 e8 d8  | % 27
    cis8  d16 e16 ~  e2. | % 28
    f4 e4 d2 | % 29
    r8 e8 \< f8  e16 d8. c8 b8 c8   | 
    e2. g8  f8  | % 31
    d2 ~ d4. g,8 \! \mf | % 32
    c4 d8  e8  b2 | % 33
    b8  c8 b8 g8  a2  | % 34
    f'8  e8 e8 d8  d8 c4 a8 | % 35
    b8  c16 d16 ~  d2. | % 36
    c4 d8  e8  g4 ~ 
    \times 2/3  {
        g8  e8 b8  }
    | % 37
    a1 | % 38
    f'8  e8 e8 d8  d2  | % 39
    g8  f16 f8. e16 e8. d8 ~  
    \times 2/3  {
        d8  e8 f8  }
    | 
    g1 | % 41
    r4 r8 e8 \mp f8  e8 ~  
    \times 2/3  {
        e8  b8 d8  }
    | % 42
    d8  c8 ~  c2.  | % 43
    d2 d8  c16 b8. a8  | % 44
    g4. e8 g8  e8 d'8 c8 ~  | % 45
    c4 b4 a4 g4 | % 46
    e'4. g8 c,8  e8  d8  d16 c16   | % 47
    b4. b16  c16  d8  c8 b8 a8  | % 48
    g2 r2 | % 49
    R1 | 
    r16 e16 \mf  f16 g16  a8  g8  a8  c8  c4 \> | % 51
    f,8  c'8  b4. a8 g16  f16 e8   | % 52
    d8  c8  g'4. \! r8 r4 | % 53
    r2 r4 r8 g8 \mp | % 54
    e'4 e8  d8  c4 c8  b8  | % 55
    a8  b8 c8 d8  g,2  | % 56
    a8  b8 c8 a8  a16  g8 c8. e8  | % 57
    f8  e8. c8 e16  d4. e8 | % 58
    g4. a8 g8.  f8. e8  | % 59
    e8  d8. d8 e16  f4. e16  d16   | 
    c4. g8 c4 c16  d16 e8  | % 61
    d8 c2. r16 r16 | % 62
    r2 f8  e8 d8 c8  | % 63
    b16  c16 d8 ~  d2.  | % 64
    r4. e8 g8  f8 e8 d8  | % 65
    cis8  d16 e16 ~  e2. | % 66
    f4 e4 d2  | % 67
    r8 e8 f8  e16 d8. c8 b8 c8  | % 68
    e2. \< g8  f8  | % 69
    d2 ~ d4. g,8 \! \mf | 
    c4 d8  e8  b2  | % 71
    b8  c8 b8 g8  a2 | % 72
    f'8  e8 e8 d8  d8 c4 a8 | % 73
    b8  c16 d16 ~  d2. | % 74
    c4 d8  e8  g4 e8  b8   | % 75
    a1 | % 76
    f'8  e8 e8 d8  d2 | % 77
    g8  f16 f8. e16 e8. d8 ~  
    \times 2/3  {
        d8  e8 f8  }
    | % 78
    g1  | % 79
    r4 r8 e8 f8  e8 ~  
    \times 2/3  {
        e8  b8 d8  }
    | 
    d8  c8 ~  c2 g8 \f  d8  | % 81
    g8  d8 e8 b8  e8  b8 f'8 g8  | % 82
    a8  f8 f8 d8  <f f f>8  d8 e8 c8  | % 83
    e8  c8 g'8 d8  b'8  g8  g8  g16 a16   | % 84
    b16  g16 a16 b16  e,8  c8  e8  c8 g'8 b,8  | % 85
    g'8  b,8 a'8 g8  a8  c8 g8 a,8  | % 86
    f'8  a,8 d8 e,8  d'8  e,8  d'8  f,16 g16  | % 87
    a16  g16 a16 c16  f,8  e8  f'8  f,8 a'8 a,8   | % 88
    c'8  c,8  r8 f16  d16  e16  f16 g16 e16  f16  g16 a16 b16  | % 89
    c16  d16 e16 f16  g4. \breathe r8 r4 | 
    r4. r4 g,8 \mp  c8 d8  | % 91
    e8  g8  d2 d8  c16 b16 ~   | % 92
    b8  a8  g4. e8  g8 e8  | % 93
    d'8  c8 ~  c4 b4 a4 | % 94
    g4 a2 f'4 ^\markup{ \italic {rit.} } | % 95
    e4 d4 c4 b4 | % 96
    g2 c,2 ~ | % 97
    c1 \bar "|."
    }
}

\include "../muziko.ly"
