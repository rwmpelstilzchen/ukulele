## Ukulele Facilele<br />Ukulele for beginners<br />אוּקַלֵּיקַלּוּת

Facilaj [entabulaturigitaj](https://eo.wikipedia.org/wiki/Tabulaturo) muzikaĵoj por la [ukulelo](https://eo.wikipedia.org/wiki/Ukulelo), konvenaj por komencantoj.

Easy [ukulele](https://en.wikipedia.org/wiki/Ukulele) pieces in [tablature](https://en.wikipedia.org/wiki/Tablature), suitable for absolute beginners.

נעימות פשוטות ל[אוקוללה](https://he.wikipedia.org/wiki/%D7%99%D7%95%D7%A7%D7%95%D7%9C%D7%99%D7%9C%D7%99), ב[טבלטורה](https://he.wikipedia.org/wiki/%D7%98%D7%91%D7%9C%D7%98%D7%95%D7%A8%D7%94_%D7%9C%D7%9B%D7%9C%D7%99%D7%9D_%D7%A9%D7%95%D7%A0%D7%99%D7%9D). מתאימות למתחילים.
