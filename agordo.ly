akordoj = \chordmode {g4 c e a}
muziko = {g'4\4 c' e' a'}
<<
  \new ChordNames {\akordoj}
  \new Staff \with {\omit StringNumber \hide Staff.TimeSignature}
  \muziko
  \new TabStaff \with {stringTunings = \stringTuning <g' c' e' a'>}
  \muziko
>>
